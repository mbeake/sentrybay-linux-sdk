#include <dlfcn.h>
#include <stdio.h>
#include <string.h>

/* create pointers for real glibc functions */
void * __real_dlsym(void *handle, const char *symbol);
ssize_t __real_write(int fd, const void *buf, size_t count);
int __real_puts(const char* str);

void *__wrap_dlsym(void *handle, const char *symbol)
{
  printf("dlsym() called: symbol=%s\n", symbol);

  return __real_dlsym(handle, symbol);
}

ssize_t __wrap_write (int fd, const void *buf, size_t count)
{
    /* printing out the number of characters */
    printf("write:chars#:%lu\n", count);

    /* call the real glibc function and return the result */
    ssize_t result = __real_write(fd, buf, count);
    return result;
}

int __wrap_puts (const char* str)
{
    /* printing out the number of characters */
    printf("puts:chars#:%lu\n", strlen(str));

    /* call the real glibc function and return the result */
    int result = __real_puts(str);
    return result;
}
