#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>

static void eraseImage(XImage *image)
{
  if (image)
  {
    for (int i = 0; i < image->width; i++)
    {
      for (int j = 0; j < image->height; j++)
      {
        XPutPixel(image, i, j, 0);
      }
    }
  }
}

static Bool imageCaptureEnabled(
  register Display *display,
  Drawable d
)
{
  Bool enabled = False;

  Atom actualTypeReturned = XA_INTEGER;
  int actualFormatReturned = 0;
  unsigned long numberItemsReturned = 0;
  unsigned long numberBytesUnread = 0;
  unsigned char *propertyDataReturned = 0;

//  Atom property = XInternAtom(display, "_SB_IMAGE_CAPTURE_ENABLED", True);
  Atom property = XInternAtom(display, "_NET_WM_PID", True);

  int status = XGetWindowProperty(display, d, property, 0L, sizeof(int), False, XA_INTEGER,
    &actualTypeReturned, &actualFormatReturned, &numberItemsReturned, &numberBytesUnread, &propertyDataReturned);

  printf("status=%d actualFormatReturned=%d numberItemsReturned=%lu numberBytesUnread=%lu\n",
		  status, actualFormatReturned, numberItemsReturned, numberBytesUnread);

  if (status && numberItemsReturned == 1 && numberBytesUnread == 0 && propertyDataReturned)
  {
    enabled = True;
  }

  return enabled;
}

static XImage *(*realXGetImage) (
  register Display *display,
  Drawable d,
  int x,
  int y,
  unsigned int width,
  unsigned int height,
  unsigned long plane_mask,
  int format);

Bool (*realXShmGetImage) (
  register Display *display,
  Drawable d,
  XImage *image,
  int x,
  int y,
  unsigned long plane_mask);

XImage *XGetImage (
  register Display *display,
  Drawable d,
  int x,
  int y,
  unsigned int width,
  unsigned int height,
  unsigned long plane_mask,
  int format) /* either XYPixmap or ZPixmap */
{
  printf("XGetImage called: x=%d, y=%d, width=%d, height=%d, plane_mask=%lx\n", x, y, width, height, plane_mask);

  XImage *image = realXGetImage(display, d, x, y, width, height, plane_mask, format);

  if (imageCaptureEnabled(display, d) == False) eraseImage(image);

  return image;
}

Bool XShmGetImage(
  register Display *display,
  Drawable d,
  XImage *image,
  int x, int y,
  unsigned long plane_mask)
{
  printf("XShmGetImage called: x=%d, y=%d, plane_mask=%lx\n", x, y, plane_mask);

  Bool status = False;

  if (realXShmGetImage) status = realXShmGetImage(display, d, image, x, y, plane_mask);

  if (imageCaptureEnabled(display, d) == False) eraseImage(image);

  return status;
}

void _init(void)
{
  printf("Loading X11 overrides.\n");
  realXGetImage = dlsym(RTLD_NEXT, "XGetImage");
  realXShmGetImage = dlsym(RTLD_NEXT, "XShmGetImage");
}
