#include <X11/Xlib.h>
#include <X11/X.h>

void main()
{
    Display *display = XOpenDisplay(NULL);
    Window root = DefaultRootWindow(display);

    XWindowAttributes attributes;
    XGetWindowAttributes(display, root, &attributes);

    XImage *ximage = XGetImage(display, root, 0, 0, attributes.width, attributes.height, AllPlanes, ZPixmap);

    XCloseDisplay(display);
}
