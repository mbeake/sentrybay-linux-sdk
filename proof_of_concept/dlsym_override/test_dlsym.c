#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>

static void * _real_dlsym = NULL;

extern void *_dl_sym(void *, const char *, void *);
extern void *__libc_dlsym (void *, const char *);

int main()
{
    printf("dlsym is %p\n", dlsym);
    printf("_dl_sym is %p\n", _dl_sym);
    printf("__libc_dlsym is %p\n", __libc_dlsym);

    _real_dlsym = dlsym(((void *) 0l), "dlsym");
    printf("dlsym(((void *) 0l), \"dlsym\") returned %p\n", _real_dlsym);

    _real_dlsym = dlvsym(((void *) 0l), "dlsym", "GLIBC_2.0");
    printf("dlvsym(((void *) 0l), \"dlsym\", \"GLIBC_2.0\") returned %p\n", _real_dlsym);

    _real_dlsym = dlsym(((void *) -1l), "dlsym");
    printf("dlsym(((void *) -1l), \"dlsym\") returned %p\n", _real_dlsym);

    _real_dlsym = dlvsym(((void *) -1l), "dlsym", "GLIBC_2.0");
    printf("dlvsym(((void *) -1l), \"dlsym\", \"GLIBC_2.0\") returned %p\n", _real_dlsym);

    return 0;
}
