#include <dlfcn.h> // for RTLD_NEXT
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <pthread.h>

#include <xcb/xcb.h>
#include <xcb/xinput.h>

#include <X11/Xlib.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>

#include "dlfcn.h" // for real_dlsym
#include "SentryBayKeys.h"
#include "SentryBayLib.h"
#include "trace.h"
#include "utils.h"

// Checking whether protection is on requires a message to the service, so we tend to check this fairly late
// (only after confirming the event is keystroke event)

#ifdef __cplusplus
extern "C" {
#endif

static const char *fake_extension = "SENTRYBAY";

typedef xcb_generic_event_t*(*FUNCPTR_XCBWAITFOREVENT) (xcb_connection_t*);
typedef xcb_generic_event_t*(*FUNCPTR_XCBPOLLFOREVENT) (xcb_connection_t*);
typedef xcb_generic_event_t*(*FUNCPTR_XCBPOLLFORQUEUEDEVENT) (xcb_connection_t*);
typedef xcb_query_extension_cookie_t(*FUNCPTR_XCBQUERYEXTENSION) (xcb_connection_t*, uint16_t, const char*);
typedef xcb_query_extension_cookie_t(*FUNCPTR_XCBQUERYEXTENSIONUNCHECKED) (xcb_connection_t*, uint16_t, const char*);

/*xcb_poll_for_special_event, xcb_wait_for_special_event: this seems to be specific to Present extension, I don't think it's
 * relevant to key processing.*/

static FUNCPTR_XCBWAITFOREVENT real_xcb_wait_for_event = NULL;
static FUNCPTR_XCBPOLLFOREVENT real_xcb_poll_for_event = NULL;
static FUNCPTR_XCBPOLLFORQUEUEDEVENT real_xcb_poll_for_queued_event = NULL;
static FUNCPTR_XCBQUERYEXTENSION real_xcb_query_extension = NULL;
static FUNCPTR_XCBQUERYEXTENSIONUNCHECKED real_xcb_query_extension_unchecked = NULL;

extern void sendFocusInMessage( pid_t pid, Window wid );
extern void sendFocusOutMessage( pid_t pid, Window wid );

pthread_mutex_t get_active_window_lock;

#define XCB_FOCUS_CHECK 0

#if XCB_FOCUS_CHECK
static xcb_window_t xcb_get_Active_Window(xcb_window_t event_window)
{
    pthread_mutex_lock(&get_active_window_lock);
    xcb_connection_t* conn = xcb_connect(NULL, NULL);
    xcb_get_input_focus_reply_t* focus_reply = NULL;
    xcb_query_tree_cookie_t focus_tree_cookie;
    xcb_query_tree_reply_t* focus_tree_reply= NULL;
    xcb_window_t focus_window = XCB_INPUT_FOCUS_NONE;
    int cnt = 0;

    if(xcb_connection_has_error(conn)){
        tracef( TRACE_ERROR, SUBSYSTEM_NAMES::XCBKEYS, "xcb_connection_has_error");
        pthread_mutex_unlock(&get_active_window_lock);
        xcb_disconnect(conn);
        return focus_window;
    }

    focus_reply = xcb_get_input_focus_reply(conn, xcb_get_input_focus(conn), NULL);
    if (!focus_reply) {
        tracef( TRACE_ERROR, SUBSYSTEM_NAMES::XCBKEYS, "get active window pid=%d  focus reply null", getpid());
        xcb_disconnect(conn);
        pthread_mutex_unlock(&get_active_window_lock);
        return focus_window;
    }
    focus_window = focus_reply->focus;
    if(event_window == focus_window || focus_window == XCB_INPUT_FOCUS_NONE){
        xcb_disconnect(conn);
        free(focus_reply);
        pthread_mutex_unlock(&get_active_window_lock);
        return focus_window;
    }
    while (cnt <= 1) {
        if(event_window == focus_window) {
            break;
        }

        focus_tree_cookie = xcb_query_tree(conn, focus_window);
        focus_tree_reply = xcb_query_tree_reply(conn, focus_tree_cookie, NULL);
        if (!focus_tree_reply) {
            focus_window = XCB_INPUT_FOCUS_NONE;
            break;
        }
        /* 
         * This is mainly handle the case of the focus window being a subwindow, so we try to find its parent and check if it matches with
         * the window id we got in the event. we also do some checks to stop the iteration.
         */
        if (focus_window == focus_tree_reply->root || focus_tree_reply->parent == focus_tree_reply->root) {
            break;
        } else {
            focus_window = focus_tree_reply->parent;
        }
        free(focus_tree_reply);
        focus_tree_reply = NULL;
        cnt++;
    }

    free(focus_tree_reply);
    free(focus_reply);
    xcb_disconnect(conn);
    pthread_mutex_unlock(&get_active_window_lock);
    return focus_window;
}
#endif

static void handle_XI_keyboard_events_xcb( xcb_ge_generic_event_t* ge, xcb_connection_t* conn )
{
    static bool white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::XCBKEYS);
    if( white_listed ) return;
    switch( ge->event_type )
    {
        case XI_RawKeyPress:
        {
            xcb_input_raw_key_press_event_t* re = (xcb_input_raw_key_press_event_t*)ge;
            if( isModifiableKeyCode(NULL, conn, re->detail) &&
                isSBkeyProtectionOn() )
            {
                trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Modifying xcb xi_rawkey press" );
                re->detail = (re->time % 50) + 10;
            }
        }
        break;
        
        case XI_RawKeyRelease:
        {
            xcb_input_raw_key_release_event_t* re = (xcb_input_raw_key_release_event_t*)ge;
            if( isModifiableKeyCode(NULL, conn, re->detail) &&
                isSBkeyProtectionOn() )
            {
                trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Modifying xcb xi_rawkey release" );
                re->detail = (re->time % 50) + 10;
            }
        }
        break;

        case XI_KeyPress:
        {
            xcb_input_device_key_press_event_t* de = (xcb_input_device_key_press_event_t*)ge;
            if( isModifiableKeyCode(NULL, conn, de->detail) &&
                isSBkeyProtectionOn() )
            {
                trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Modifying xcb xi_key press" );
                de->detail = (de->time % 50) + 10;
            }
        }
        break;

        case XI_KeyRelease:
        {
            xcb_input_device_key_press_event_t* de = (xcb_input_device_key_press_event_t*)ge;
            if( isModifiableKeyCode(NULL, conn, de->detail) &&
                isSBkeyProtectionOn() )
            {
                trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Modifying xcb xi_key release" );
                de->detail = (de->time % 50) + 10;
            }
        }
        break;

        case XI_FocusIn:
        {
            xcb_input_focus_in_event_t *fie = (xcb_input_focus_in_event_t*)ge;
            tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "XI_FocusIn event: pid=%d winid=0x%x", getpid(), fie->child);
            sendFocusInMessage( getpid(), fie->event );
        }
        break;

        case XI_FocusOut:
        {
            xcb_input_focus_out_event_t *foe = (xcb_input_focus_out_event_t*)ge;
            tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "XI_FocusOut event: pid=%d winid=0x%x", getpid(), foe->child);
            sendFocusOutMessage( getpid(), foe->event );
        }
        break;
    }
}

static void handle_keyboard_events_xcb( xcb_connection_t* c, xcb_generic_event_t* event_return )
{
    static bool white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::XCBKEYS);
    if( white_listed ) return;
    // Every event contains an 8-bit type code; the most  significant bit in this code is set if the event was generated from
    // SendEvent requests, so if we mask out that bit then we ignore artificially generated events.
    if( (event_return->response_type & ~0x80) == XCB_GE_GENERIC )
    {
        xcb_ge_generic_event_t* ge = (xcb_ge_generic_event_t*)event_return;
        if( ge->extension == sb_xi_opcode )
        {
            handle_XI_keyboard_events_xcb( ge, c );
        }
    }
    else
    {
        tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Response type is:%i", event_return->response_type & ~0x80 );

        switch( event_return->response_type & ~0x80 )
        {
            case XCB_KEY_PRESS:
            {
                xcb_key_press_event_t* kpe = (xcb_key_press_event_t*)event_return;
                if( isModifiableKeyCode(NULL, c, kpe->detail) &&
                    isSBkeyProtectionOn() )
                {
                    trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Changing XCB_KEY_PRESS" );
                    kpe->detail = (kpe->time % 50) + 10;
                }
            }
            break;

            case XCB_KEY_RELEASE:
            {
                xcb_key_release_event_t* kre = (xcb_key_release_event_t*)event_return;
                if( isModifiableKeyCode(NULL, c, kre->detail) &&
                    isSBkeyProtectionOn() )
                {
                    trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Changing XCB_KEY_RELEASE" );
                    kre->detail = (kre->time % 50) + 10;
                }
            }
            break;

            case XCB_FOCUS_IN:
            {
                xcb_focus_in_event_t *fie = (xcb_focus_in_event_t*)event_return;
                tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "XCB_FOCUS_IN event: pid=%d winid=0x%x", getpid(), fie->event);
#if XCB_FOCUS_CHECK
                xcb_window_t focus_window = xcb_get_Active_Window(fie->event);

                // Sanity check: do not send focus in unless the X server agrees the event window is currently in focus.
                if(focus_window == XCB_INPUT_FOCUS_POINTER_ROOT  || focus_window == XCB_INPUT_FOCUS_NONE || focus_window == fie->event)
                {
                    sendFocusInMessage( getpid(), fie->event); 
                } else {
                    tracef(TRACE_INFO, SUBSYSTEM_NAMES::XCBKEYS, "focus in event, do not send focus in message. event: pid=%d winid=0x%x", getpid(), fie->event);
                }
#else
                sendFocusInMessage( getpid(), fie->event );
#endif
            }
            break;

            case XCB_FOCUS_OUT:
            {
                xcb_focus_out_event_t *foe = (xcb_focus_out_event_t*)event_return;
                tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "XCB_FOCUS_OUT event: pid=%d winid=0x%x", getpid(), foe->event);
#if XCB_FOCUS_CHECK
                xcb_window_t focus_window = xcb_get_Active_Window(foe->event);

                // Sanity check: do not send focus out unless the X server agrees the event window is currently not in focus.
                if(focus_window == XCB_INPUT_FOCUS_POINTER_ROOT || focus_window == XCB_INPUT_FOCUS_NONE || focus_window != foe->event)
                {
                    sendFocusOutMessage( getpid(), foe->event);
                } else {
                    tracef(TRACE_INFO, SUBSYSTEM_NAMES::XCBKEYS, "focus out event, do not send focus out message. event: pid=%d winid=0x%x", getpid(), foe->event);
                }
#else
                sendFocusOutMessage( getpid(), foe->event );
#endif
            }
            break;

            default:
                if (xi_key_press_type == INVALID_EVENT_TYPE || xi_key_release_type == INVALID_EVENT_TYPE)
                    read_xinput_event_ids();

                if( (event_return->response_type & ~0x80) == xi_key_press_type )
                {
                    xcb_input_device_key_press_event_t* dkpe = (xcb_input_device_key_press_event_t*)event_return;
                    if( isModifiableKeyCode(NULL, c, dkpe->detail) &&
                        isSBkeyProtectionOn() )
                    {
                        trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Changing xcb xi_key_press_type" );
                        dkpe->detail = (dkpe->time % 50) + 10;
                    }
                }
                else if( (event_return->response_type & ~0x80) == xi_key_release_type )
                {
                    xcb_input_device_key_release_event_t* dkre = (xcb_input_device_key_release_event_t*)event_return;
                    if( isModifiableKeyCode(NULL, c, dkre->detail) &&
                        isSBkeyProtectionOn() )
                    {
                        trace( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "Changing xcb xi_key_release_type" );
                        dkre->detail = (dkre->time % 50) + 10;
                    }
                }
            break;
        }
    }
}

__attribute__((visibility("default"))) xcb_generic_event_t* xcb_wait_for_event( xcb_connection_t* c )
{
    if( real_xcb_wait_for_event == NULL )
    {
        // Lookup the symbol using the "real" dlsym().
        real_xcb_wait_for_event = (FUNCPTR_XCBWAITFOREVENT)real_dlsym(RTLD_NEXT, "xcb_wait_for_event");

        if( real_xcb_wait_for_event == NULL ) return 0;
    }
    
    xcb_generic_event_t* event_return = real_xcb_wait_for_event(c);
    
    if( event_return )
    {
        tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "xcb_wait_for_event called. Type is:%i",
                event_return->response_type & ~0x80 );
    }

    if( event_return )
    {
        handle_keyboard_events_xcb( c, event_return );
    }
    return event_return;
}

__attribute__((visibility("default"))) xcb_generic_event_t* xcb_poll_for_event( xcb_connection_t* c )
{
    if( real_xcb_poll_for_event == NULL )
    {
        // Lookup the symbol using the "real" dlsym().
        real_xcb_poll_for_event = (FUNCPTR_XCBPOLLFOREVENT)real_dlsym(RTLD_NEXT, "xcb_poll_for_event");

        if( real_xcb_poll_for_event == NULL ) return 0;
    }

    xcb_generic_event_t* event_return = real_xcb_poll_for_event(c);

    if( event_return )
    {
        tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "xcb_poll_for_event called. Type is:%i",
                event_return->response_type & ~0x80 );
    }

    if( event_return )
    {
        handle_keyboard_events_xcb( c, event_return );
    }
    return event_return;
}

__attribute__((visibility("default"))) xcb_generic_event_t* xcb_poll_for_queued_event( xcb_connection_t* c )
{
    if( real_xcb_poll_for_queued_event == NULL )
    {
        // Lookup the symbol using the "real" dlsym().
        real_xcb_poll_for_queued_event = (FUNCPTR_XCBPOLLFORQUEUEDEVENT)real_dlsym(RTLD_NEXT, "xcb_poll_for_queued_event");

        if( real_xcb_poll_for_queued_event == NULL ) return 0;
    }
    
    xcb_generic_event_t* event_return = real_xcb_poll_for_queued_event(c);

    if( event_return )
    {
        tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::XCBKEYS, "xcb_poll_for_queued_event called. Type is:%i",
                event_return->response_type & ~0x80 );
    }

    if( event_return )
    {
        handle_keyboard_events_xcb( c, event_return );
    }
    return event_return;
}

__attribute__((visibility("default"))) xcb_query_extension_cookie_t xcb_query_extension( xcb_connection_t* c, uint16_t name_len, const char* name )
{
    if( real_xcb_query_extension == NULL )
    {
        // Lookup the symbol using the "real" dlsym().
        real_xcb_query_extension = (FUNCPTR_XCBQUERYEXTENSION)real_dlsym(RTLD_NEXT, "xcb_query_extension");

        if( real_xcb_query_extension == NULL )
        {
            trace(TRACE_FATAL, SUBSYSTEM_NAMES::XCB, "xcb_query_extension(): real_xcb_query_extension == NULL Shouldn't happen");
            exit(-1); // Shouldn't happen
        }
    }

    if( strcmp(name, "RECORD") == 0 )
    {
        return real_xcb_query_extension( c, strlen( fake_extension ), fake_extension );
    }
    else
    {
        return real_xcb_query_extension( c, name_len, name );
    }
}

__attribute__((visibility("default"))) xcb_query_extension_cookie_t xcb_query_extension_unchecked( xcb_connection_t* c, uint16_t name_len, const char* name)
{
    if( real_xcb_query_extension_unchecked == NULL )
    {
        // Lookup the symbol using the "real" dlsym().
        real_xcb_query_extension_unchecked = (FUNCPTR_XCBQUERYEXTENSIONUNCHECKED)real_dlsym(RTLD_NEXT, "xcb_query_extension_unchecked");

        if( real_xcb_query_extension_unchecked == NULL )
        {
            trace(TRACE_FATAL, SUBSYSTEM_NAMES::XCB, "xcb_query_extension_unchecked(): real_xcb_query_extension_unchecked == NULL Shouldn't happen");
            exit(-1); // Shouldn't happen
        }
    }

    if( strcmp(name, "RECORD") == 0 )
    {
        return real_xcb_query_extension_unchecked( c, strlen( fake_extension ), fake_extension );
    }
    else
    {
        return real_xcb_query_extension_unchecked( c, name_len, name );
    }
}

static const INTERCEPT intercepts[] =
{
    { "xcb_wait_for_event", (void *)xcb_wait_for_event, RTLD_NEXT },
    { "xcb_poll_for_event", (void *)xcb_poll_for_event, RTLD_NEXT },
    { "xcb_poll_for_queued_event", (void *)xcb_poll_for_queued_event, RTLD_NEXT },
    { "xcb_query_extension", (void *)xcb_query_extension, RTLD_NEXT },
    { "xcb_query_extension_unchecked", (void *)xcb_query_extension_unchecked, RTLD_NEXT }
};

void _init_xcb_keys(void)
{
    //////
    //
    // SEC-9 - Do not call trace here as it can lead to performance issues
    //
    //////

    registerIntercepts(intercepts, sizeof(intercepts)/sizeof(intercepts[0]));
}

#ifdef __cplusplus
}
#endif
