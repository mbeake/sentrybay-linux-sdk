#ifndef X_ERROR_TRAP_H_
#define X_ERROR_TRAP_H_

#include <X11/Xlib.h>

class XErrorTrap
{
public:
  explicit XErrorTrap(Display* display);
  ~XErrorTrap();

  void setErrorCode(int errorCode) { m_errorCode = errorCode; }
  int errorCode() const { return m_errorCode; }

private:
  int m_errorCode;
  XErrorHandler m_originalErrorHandler;

  XErrorTrap(const XErrorTrap&) = delete;
  XErrorTrap& operator=(const XErrorTrap&) = delete;
};

#endif // X_ERROR_TRAP_H_
