#ifndef __WINDOWANTIKEYLOGGING__H
#define __WINDOWANTIKEYLOGGING__H

#include "X11.h"

#include "sentryclient.h"

void SendRegisterMessage( Window wid )
{
    printf( "Would send register message for wid:%lu to service\n", wid );
    fflush( stdout );
    SentryClient client;
    client.RegisterWindow( getpid(), wid );
}

void SendDeregisterMessage( Window wid )
{
    printf( "Would send deregister message for wid:%lu to service\n", wid );
    fflush(stdout);
    SentryClient client;
    client.UnRegisterWindow( getpid(), wid );
}

void RegisterWindowForAntiKeylogging( Display* display, Window wid )
{
    // 1. Register this wid with the service to let it know that it's a protected window
    // 2. Make sure we're receiving focus change events on this window
    // The actual processing of focus change events is done in the preload library.
    if( wid == None )
    {
        return;
    }
    SendRegisterMessage( wid );
    XSelectInput( display, wid, FocusChangeMask );
}

void DeregisterWindowForAntiKeylogging( Window wid )
{
    if( wid == None )
    {
        return;
    }
    SendDeregisterMessage( wid );
}

Window CreateAndRegisterWindow( Display* display, Window parent, int x, int y,
                                unsigned int width, unsigned int height,
                                unsigned int border_width, int depth,
                                unsigned int window_class, Visual* visual,
                                unsigned long valuemask, XSetWindowAttributes* attributes )
{
    Window wid = XCreateWindow( display, parent, x, y,
                                width, height,
                                border_width, depth,
                                window_class, visual,
                                valuemask, attributes );
    if( wid != None )
    {
        RegisterWindowForAntiKeylogging( display, wid );
    }
    return wid;
}

Window CreateAndRegisterSimpleWindow( Display* display, Window parent, int x, int y,
                                      unsigned int width, unsigned int height,
                                      unsigned int border_width,
                                      unsigned long border, unsigned long background )
{
    Window wid = XCreateSimpleWindow( display, parent, x, y,
                                      width, height,
                                      border_width,
                                      border, background );
    if( wid != None )
    {
        RegisterWindowForAntiKeylogging( display, wid );
    }
    return wid;
}

void DeregisterAndDestroyWindow( Display* display, Window wid )
{
    if( wid != None )
    {
        DeregisterWindowForAntiKeylogging( wid );
    }
    XDestroyWindow( display, wid );
}

void EnableKeystrokeProtection()
{
    printf( "Sending keystroke protection message to service\n" );
    fflush(stdout);
    SentryClient client;
    client.SetKLockState(true);
}

void DisableKeystrokeProtection()
{
    printf( "Sending keystroke protection disable message to service\n" );
    fflush(stdout);
    SentryClient client;
    client.SetKLockState(false);
}

#endif
