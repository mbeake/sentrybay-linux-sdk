#ifdef __cplusplus
extern "C" {
#endif

void _init_dlfcn(void);

extern void *real_dlsym(void *, const char *);

#ifdef __cplusplus
}
#endif
