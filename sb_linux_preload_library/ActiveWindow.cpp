#include <unordered_set>
#include <unordered_map>
#include <iostream>

#include <X11/Xlib.h>

#include "X11.h"

extern "C"
{
    void *__dso_handle = (void*) &__dso_handle;
}

namespace
{
    static std::unordered_map< pid_t, std::unordered_set<Window> > pid_to_windows;
}

Bool pidHasActiveWindow( pid_t pid )
{
    std::unordered_map< pid_t, std::unordered_set<Window> > pid_to_windows;
    Window active_window = getActiveWindow();
    Bool rv = False;
    if( active_window != None )
    {
        const auto found_pid_windows = pid_to_windows.find( pid );
        if( found_pid_windows != pid_to_windows.end() )
        {
            const auto found_active_window = found_pid_windows->second.find( active_window );
            if( found_active_window != found_pid_windows->second.end() )
            {
                rv = True;    
            }
        }
    }
    return rv;
}

Window getActiveWindow()
{
    Display *dpy = NULL;
    Window root = None;
    Atom activewindow_atom;
    
    // Function return value
    Window active_window = None;

    // Unused XGetWindowProperty returned values
    int actual_format;
    unsigned long extra, nitems;
    Atom actual_atom;
 
    // The window id we're hoping to get back
    unsigned char *data;
    
    // try to open default display, i.e. $DISPLAY
    if( !(dpy = XOpenDisplay(NULL)) )
    {
        // Error, no active display
        return active_window;    
    }

    // get default window
    root = XDefaultRootWindow(dpy);
    
    // get Atom for _NET_ACTIVE_WINDOW
    activewindow_atom = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", False);

    // get _NET_ACTIVE_WINDOW property from the root window, if available.
    // Not all X Window Managers support it, although nearly all modern ones should.
    if( (XGetWindowProperty(dpy, root, activewindow_atom, 0, ~0, False,
                           AnyPropertyType, &actual_atom, &actual_format, &nitems, &extra,
                           &data) != Success) ||
        (data == NULL) )
    {
        XCloseDisplay(dpy);
        return active_window;    
    }

    active_window = *(Window *) data;
    XFree (data);

#ifdef DEBUG
    std::cout << "NET_ACTIVE_WINDOW is:" << active_window << std::endl;
#endif

    if( active_window == None )
    {
        // As a fallback, try finding the window with input focus.
        int revert_to; // ignored
        XGetInputFocus( dpy,
                        &active_window,
                        &revert_to
                        );
#ifdef DEBUG
        std::cout << "input focus window is:" << active_window << std::endl;
#endif
    }
    if( active_window == PointerRoot )
    {
        // log error or warning? Can we handle this? Should SDK detect and refuse to run?
        active_window = None;
    }
    XCloseDisplay(dpy);
    return active_window;
}
