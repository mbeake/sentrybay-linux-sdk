#include <iostream>

#include <stdlib.h>

#include <xcb/xcb.h>
#include <xcb/xinput.h>

#include <X11/Xlib-xcb.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>

int main( int argc, char** argv )
{
    Display* display = XOpenDisplay(NULL);
    
    xcb_screen_t* screen;
    
    //xcb_connection_t* c = xcb_connect( NULL, NULL );
    xcb_connection_t* c = XGetXCBConnection(display);

    screen = xcb_setup_roots_iterator (xcb_get_setup(c)).data;
    
    uint32_t mask = XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE;
    uint32_t values[2];
    
    xcb_window_t wid2 = xcb_generate_id(c);
    mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    values[0] = screen->white_pixel;
    values[1] = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_BUTTON_PRESS |
                XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_ENTER_WINDOW |
                XCB_EVENT_MASK_LEAVE_WINDOW |
                XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE;
                
    xcb_create_window (c,
                       0,
                       wid2,
                       screen->root,
                       0, 0,
                       150, 150,
                       10,
                       XCB_WINDOW_CLASS_INPUT_OUTPUT,
                       screen->root_visual,
                       mask, values);
    xcb_map_window( c, wid2 );
    
    // xinput. xcb-input is considered experimental, so do this the Xlib way.
    /*
    struct
    {
        xcb_input_event_mask_t head;
        xcb_input_xi_event_mask_t mask;
    } xcb_test_mask;
    xcb_test_mask.head.deviceid = XCB_INPUT_DEVICE_ALL;
    xcb_test_mask.head.mask_len = sizeof(xcb_test_mask.mask) / sizeof(uint32_t);
    xcb_test_mask.mask = xcb_input_xi_event_mask_t(
                         XCB_INPUT_XI_EVENT_MASK_KEY_PRESS | XCB_INPUT_XI_EVENT_MASK_KEY_RELEASE |
                         XCB_INPUT_XI_EVENT_MASK_RAW_KEY_PRESS | XCB_INPUT_XI_EVENT_MASK_RAW_KEY_RELEASE);
    xcb_input_xi_select_events( c, screen->root, 1, &xcb_test_mask.head );*/

    XIEventMask eventmask;
    unsigned char xi_key_events_mask[2] = { 0, 0 };

    eventmask.deviceid = XIAllDevices;
    eventmask.mask_len = sizeof(xi_key_events_mask);
    eventmask.mask = xi_key_events_mask;
    XISetMask( xi_key_events_mask, XI_KeyPress );
    XISetMask( xi_key_events_mask, XI_KeyRelease );
    XISetMask( xi_key_events_mask, XI_RawKeyPress );
    XISetMask( xi_key_events_mask, XI_RawKeyRelease );

    XISelectEvents(display, screen->root, &eventmask, 1 );

    xcb_flush(c);

    xcb_generic_event_t* event;

    while( ( event = xcb_wait_for_event(c) ) )
    {
        std::cout << "Event received" << std::endl;
        if( event )
        {
            switch( event->response_type & ~ 0x80 )
            {
                case XCB_KEY_PRESS:
                {
                    xcb_key_press_event_t* ke = (xcb_key_press_event_t*)event;
                    std::cout << "Key_press code is:" << (int)ke->detail << std::endl;
                }
                break;

                case XCB_KEY_RELEASE:
                {
                    xcb_key_release_event_t* ke = (xcb_key_release_event_t*)event;
                    std::cout << "Key_release code is:" << (int)ke->detail << std::endl;
                }
                break;

                case XCB_GE_GENERIC:
                {
                    std::cout << "Received generic event" << std::endl;

                    switch( ((xcb_ge_event_t*)event)->event_type )
                    {
                        case XI_RawKeyPress:
                        {
                            std::cout << "XI_RawKeyPress detail is:"
                                      << ((xcb_input_raw_key_press_event_t*)event)->detail << std::endl;
                        }
                        break;

                        case XI_RawKeyRelease:
                        {
                            std::cout << "XI_RawKeyRelease detail is:"
                                      << ((xcb_input_raw_key_release_event_t*)event)->detail << std::endl;
                        }
                        break;

                        case XI_KeyPress:
                        {
                            std::cout << "XI_KeyPress detail is:"
                                      << ((xcb_input_device_key_press_event_t*)event)->detail << std::endl;
                        }
                        break;

                        case XI_KeyRelease:
                        {
                            std::cout << "XI_KeyRelease detail is:"
                                      << ((xcb_input_device_key_release_event_t*)event)->detail << std::endl;
                        }
                        break;
                    }
                }
            }
        }
    }

    return 0;
}
