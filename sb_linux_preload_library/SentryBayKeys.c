#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#include <X11/keysym.h>
#include <X11/X.h>
#include <X11/Xlib.h>

#include <X11/extensions/XInput.h>

#include <xcb/xcbext.h>
#include <xcb/xcb.h>
#include <xcb/xinput.h>

#include "SentryBayKeys.h"

#include "trace.h"
#include "SentryClient.h"

#ifdef __cplusplus
extern "C" {
#endif

int xi_key_press_type = INVALID_EVENT_TYPE;
int xi_key_release_type = INVALID_EVENT_TYPE;

int sb_xi_opcode;

CHECKED_INPUT_EXTENSION sb_checked_input_extension = NOT_CHECKED;
CHECKED_INPUT_EXTENSION sb_checked_xcb_input_extension = NOT_CHECKED;

static bool cachedIsSBkeyProtectionOn = false;

static pthread_t isSBkeyProtectionOnThread;
static pthread_mutex_t isSBkeyProtectionOnThreadStartedMutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_cond_t isSBkeyProtectionOnThreadWaitCondition = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t isSBkeyProtectionOnThreadWaitConditionMutex = PTHREAD_MUTEX_INITIALIZER;

static void *isSBkeyProtectionOnThreadFunction(void *)
{
    tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "entered isSBkeyProtectionOnThreadFunction(): pid: %i", getpid() );

    sb_sdk_client::SentryClient client;

    do
    {
        client.IsKeyScrambled( getpid(), cachedIsSBkeyProtectionOn );
        tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "isSBkeyProtectionOnThreadFunction(): pid: %i cachedIsSBkeyProtectionOn: %d", getpid(), cachedIsSBkeyProtectionOn);
        pthread_cond_wait(&isSBkeyProtectionOnThreadWaitCondition, &isSBkeyProtectionOnThreadWaitConditionMutex);
    }
    while(1);

    return NULL;
}

int isSBkeyProtectionOn()
{
    tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "isSBkeyProtectionOn(): pid: %i", getpid() );
    static bool white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::SBKEYS);
    if( white_listed )
    {
        return 0;
    }

    static int isSBkeyProtectionOnThreadStarted = 0;

    // Check whether the thread has already been started.
    if (isSBkeyProtectionOnThreadStarted == 0) // quick check to optimise out unnecessary mutex calls.
    {
        // Start the thread here at the point of need. Use a mutex to avoid a race condition.
        pthread_mutex_lock(&isSBkeyProtectionOnThreadStartedMutex);
        if (isSBkeyProtectionOnThreadStarted == 0)
        {
            tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "creating isSBkeyProtectionOnThread: pid: %i", getpid() );
            pthread_create(&isSBkeyProtectionOnThread, NULL, isSBkeyProtectionOnThreadFunction, NULL);
            isSBkeyProtectionOnThreadStarted = 1;
        }
        pthread_mutex_unlock(&isSBkeyProtectionOnThreadStartedMutex);
    }

    pthread_cond_signal(&isSBkeyProtectionOnThreadWaitCondition);

    return cachedIsSBkeyProtectionOn;
}

void setCachedValue( bool value )
{
    tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "setCachedValue():%i", value );
    cachedIsSBkeyProtectionOn = value;
}

int register_keyboard_events( Display* display )
{
    // For xinput key events
    int                 number_events = 0;
    XEventClass         event_class;
    int                 i;
    int                 num_devices;
    XDevice             *device;
    XInputClassInfo     *ip;
    XDeviceInfo         *devices;
    int                 loop;

    devices = XListInputDevices(display, &num_devices);

    for( loop = 0; loop < num_devices; ++loop )
    {
        if( devices[loop].use == IsXExtensionKeyboard )
        {
            device = XOpenDevice( display, devices[loop].id );

            if( !device )
            {
                tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "unable to open device id:%li", devices[loop].id );
                return 0;
            }

            if( device->num_classes > 0 )
            {
                for( ip = device->classes, i=0; i < devices[loop].num_classes; ip++, i++)
                {
                    switch( ip->input_class )
                    {
                    case KeyClass:
                        {
                        int tmp_key = INVALID_EVENT_TYPE;
                        DeviceKeyPress(device, tmp_key, event_class);
                        ++number_events;
                        if( xi_key_press_type != INVALID_EVENT_TYPE &&
                            xi_key_press_type != tmp_key )
                        {
                            tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS,
                                    "Error: mismatched xi_key_press types:%i vs. %i",
                                    xi_key_press_type, tmp_key );
                        }
                        else
                        {
                            xi_key_press_type = tmp_key;
                            tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "Registered DeviceKeyPress:%i for device id:%li",
                                    xi_key_press_type, devices[loop].id );
                        }

                        tmp_key = INVALID_EVENT_TYPE;
                        DeviceKeyRelease(device, tmp_key, event_class);
                        ++number_events;
                        if( xi_key_release_type != INVALID_EVENT_TYPE &&
                            xi_key_release_type != tmp_key )
                        {
                            tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "Error: mismatched xi_key_release types:%i vs. %i",
                                    xi_key_release_type, tmp_key );
                        }
                        else
                        {
                            xi_key_release_type = tmp_key;
                            tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "Registered DeviceKeyRelease:%i for device id:%li",
                                    xi_key_release_type, devices[loop].id );
                        }

                        }
                        break;
                    }
                }
            }
        }
    }
    // Compiler complains if we don't use event_class
    if( event_class == 0 )
    {
    }

    return number_events;
}

#if 0
int register_keyboard_events_xcb( xcb_connection_t* c )
{
    int number_events = 0;

    xcb_input_list_input_devices_cookie_t cookie = xcb_input_list_input_devices(c);
    xcb_input_list_input_devices_reply_t *input_devices_reply = xcb_input_list_input_devices_reply(c, cookie, NULL);

    for (xcb_input_device_info_iterator_t device_info_iterator = xcb_input_list_input_devices_devices_iterator(input_devices_reply);
         device_info_iterator.rem;
         xcb_input_device_info_next(&device_info_iterator))
    {
        xcb_input_device_info_t *device_info = device_info_iterator.data;

        if (device_info->device_use == IsXExtensionKeyboard)
        {
            xcb_input_open_device_cookie_t cookie = xcb_input_open_device(c, device_info->device_id);
            xcb_input_open_device_reply_t *open_device_reply = xcb_input_open_device_reply(c, cookie, NULL);

            if (!open_device_reply)
            {
                tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "unable to open device id:%li", device_info->device_id );
            }
            else
            {
                for (xcb_input_input_class_info_iterator_t input_class_info_iterator = xcb_input_open_device_class_info_iterator(open_device_reply);
                     input_class_info_iterator.rem;
                     xcb_input_input_class_info_next(&input_class_info_iterator))
                {
                    switch(input_class_info_iterator.data->class_id)
                    {
                    case KeyClass:

                        xcb_input_input_class_info_iterator_t ip = xcb_input_open_device_class_info_iterator(open_device_reply);

                        for (int i = 0; i < open_device_reply->num_classes; i++)
                        {
                            xcb_input_input_class_info_t *input_class_info = ip.data;

                            int tmp_key = (input_class_info->class_id == 0) ? input_class_info->event_type_base + 0 : 0; // DeviceKeyPress

                            ++number_events;
                            if( xi_key_press_type != INVALID_EVENT_TYPE && xi_key_press_type != tmp_key )
                            {
                                tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "Error: mismatched xi_key_press types:%i vs. %i",
                                        xi_key_press_type, tmp_key );
                            }
                            else
                            {
                                xi_key_press_type = tmp_key;
                                tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "Registered DeviceKeyPress:%i for device id:%li",
                                        xi_key_press_type, device_info->device_id );
                            }

                            tmp_key = (input_class_info->class_id == 0) ? input_class_info->event_type_base + 1 : 0; // DeviceKeyRelease

                            ++number_events;
                            if( xi_key_release_type != INVALID_EVENT_TYPE && xi_key_release_type != tmp_key )
                            {
                                tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "Error: mismatched xi_key_release types:%i vs. %i",
                                        xi_key_release_type, tmp_key );
                            }
                            else
                            {
                                xi_key_release_type = tmp_key;
                                tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "Registered DeviceKeyRelease:%i for device id:%li",
                                        xi_key_release_type, device_info->device_id );
                            }

                            xcb_input_input_class_info_next(&ip);
                        }
                    }
                }

                free(open_device_reply);
            }
        }
    }

    free(input_devices_reply);

    return number_events;
}
#endif

const char *XInputEventIDsFilePath = "/usr/local/lib/sentrybay/.XInputEventIDs";

void write_xinput_event_ids()
{
    FILE *file = fopen( XInputEventIDsFilePath, "w" );
    if ( file )
    {
        fprintf( file, "xi_key_press_type=%d\n", xi_key_press_type );
        fprintf( file, "xi_key_release_type=%d\n", xi_key_release_type );
        fclose( file );
    }
    else
    {
        tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "write_xinput_event_ids(): Failed to open XInput Events IDs file, errno=%s.", strerror( errno ) );
    }
}

void read_xinput_event_ids()
{
    xi_key_press_type = INVALID_EVENT_TYPE;
    xi_key_release_type = INVALID_EVENT_TYPE;

    FILE *file = fopen( XInputEventIDsFilePath, "r" );
    if ( file )
    {
        int matched_items = 0;

        matched_items += fscanf( file, "xi_key_press_type=%d\n", &xi_key_press_type );
        matched_items += fscanf( file, "xi_key_release_type=%d\n", &xi_key_release_type );

        tracef( TRACE_INFO, SUBSYSTEM_NAMES::SBKEYS, "read_xinput_event_ids(): read %d entries, xi_key_press_type=%d, xi_key_release_type=%d.",
                matched_items, xi_key_press_type, xi_key_release_type );

        fclose( file );
    }
    else
    {
        tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "read_xinput_event_ids(): Failed to open XInput Events IDs file, errno=%s.", strerror( errno ) );
    }
}

bool isModifiableKeyCode( Display* display, xcb_connection_t* xcb_conn, unsigned int kc )
{
    static KeyCode lalt_key_code = 0;
    static KeyCode lsuper_key_code = 0;
    static KeyCode tab_key_code = 0;
    static KeyCode f1_key_code = 0;
    static KeyCode f2_key_code = 0;
    static KeyCode escape_key_code = 0;
    static KeyCode lctrl_key_code = 0;
    static KeyCode page_up_key_code = 0;
    static KeyCode page_down_key_code = 0;
    static KeyCode left_key_code = 0;
    static KeyCode right_key_code = 0;
    static KeyCode down_key_code = 0;
    static KeyCode up_key_code = 0;
    static KeyCode del_key_code = 0;

    static bool looked_up_codes = false;
    bool rv = true;
    char curr_keys_map[32];
    memset( curr_keys_map, 0, sizeof curr_keys_map );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "isModifiableKeyCode" );

    if( !looked_up_codes  )
    {
        if( display == NULL )
        {
            // Presumably we are calling this from within xcb code. We can't use xcb calls because we're already within
            // the xcb event loop, and if you try to make a new call to the server from within the event loop it just hangs.
            // It _might_ be ok to do an XOpenDisplay within the xcb event loop, but I have seen quite bad behaviour.
            // We just hardcode the three most important values, which are very unlikely to change.
            lalt_key_code = 64;
            lsuper_key_code = 133;
            tab_key_code = 23;
        }
        else
        {
            lalt_key_code = XKeysymToKeycode( display, XK_Alt_L );
            lsuper_key_code = XKeysymToKeycode( display, XK_Super_L );
            tab_key_code = XKeysymToKeycode( display, XK_Tab );
            f1_key_code = XKeysymToKeycode( display, XK_F1 );
            f2_key_code = XKeysymToKeycode( display, XK_F2 );
            escape_key_code = XKeysymToKeycode( display, XK_Escape );
            lctrl_key_code = XKeysymToKeycode( display, XK_Control_L );
            page_up_key_code = XKeysymToKeycode( display, XK_Page_Up );
            page_down_key_code = XKeysymToKeycode( display, XK_Page_Down );
            up_key_code = XKeysymToKeycode( display, XK_Up );
            down_key_code = XKeysymToKeycode( display, XK_Down );
            left_key_code = XKeysymToKeycode( display, XK_Left );
            right_key_code = XKeysymToKeycode( display, XK_Right );
            del_key_code = XKeysymToKeycode( display, XK_Delete );
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "(%i) X lakc/lskc %u/%u", getpid(), lalt_key_code, lsuper_key_code );
            XQueryKeymap(display, curr_keys_map);
            looked_up_codes = true;
        }
    }

    if( lsuper_key_code/8 > sizeof curr_keys_map )
    {
        tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "skc is out of range" );
        lsuper_key_code = 0;
    }
    if( lalt_key_code/8 > sizeof curr_keys_map )
    {
        tracef( TRACE_ERROR, SUBSYSTEM_NAMES::SBKEYS, "akc is out of range" );
        lalt_key_code = 0;
    }

    if( kc == lalt_key_code ||
        kc == lsuper_key_code ||
        kc == tab_key_code ||
        kc == f1_key_code ||
        kc == f2_key_code ||
        kc == escape_key_code ||
        kc == lctrl_key_code ||
        kc == page_up_key_code ||
        kc == page_down_key_code ||
        kc == left_key_code ||
        kc == right_key_code ||
        kc == down_key_code ||
        kc == up_key_code ||
        kc == del_key_code )
    {
        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "not modifiable:%u", kc );
        rv = false;
    }
    // Is the "key_pressed" bit set for the relevant keycodes in the curr_keys_map?
    // Note the "relevant" keycodes are usually not the same as the kc we are processing.
    else if( curr_keys_map[lsuper_key_code/8] & (0x1<<(lsuper_key_code%8)) ||
             curr_keys_map[lalt_key_code/8]   & (0x1<<(lalt_key_code%8)) )
    {
        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SBKEYS, "xlib: not modifiable:%u", kc );
        rv = false;
    }

    return rv;
}

#ifdef __cplusplus
}
#endif
