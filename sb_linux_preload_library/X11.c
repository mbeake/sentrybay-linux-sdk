#include "SentryBayLib.h"

#ifndef __cplusplus
#define _GNU_SOURCE // for RTLD_NEXT
#endif
#include <dlfcn.h> // for RTLD_NEXT

#include <X11/X.h>
#include <X11/Xutil.h> // for XPutPixel()
#include <X11/Xlibint.h> // for _XReply() - used specifically for blocking of "recordmydesktop" application only
#include <X11/extensions/shmproto.h> // for xShmGetImageReq

#include <stdlib.h> // for exit()
#include <stdio.h> // for printf()

#include "dlfcn.h" // for real_dlsym()
#include "X11.h"
#include "XErrorTrap.h"
#include "X11ShmSupervisor.h"
#include "trace.h"
#include "SentryClient.h"

#ifdef __cplusplus
extern "C" {
#endif

static void eraseImage(XImage *image)
{
  if (image)
  {
    for (int i = 0; i < image->width; i++)
    {
      for (int j = 0; j < image->height; j++)
      {
        XPutPixel(image, i, j, 0);
      }
    }
  }
}

static bool isAWindow(Display *display, Drawable d)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11, "isAWindow() called display=%p, drawable=0x%lx", display, d);

  XWindowAttributes attributes;

  XErrorTrap errorTrap(display); // Trap BadWindow in the following call:

  return XGetWindowAttributes(display, d, &attributes) != 0 && errorTrap.errorCode() == 0;
}

typedef XImage *(*FUNCPTR_XGETIMAGE) (Display *, Drawable, int, int, unsigned int, unsigned int, unsigned long, int);
typedef Bool (*FUNCPTR_XSHMGETIMAGE) (Display *, Drawable, XImage *, int, int, unsigned long);
typedef Status (*FUNCPTR__XREPLY) (Display *, xReply *, int, Bool); // Used specifically for blocking of "recordmydesktop" application only
typedef int (*FUNCPTR_XCOPYAREA) (Display *, Drawable, Drawable, GC, int, int, unsigned int, unsigned int, int, int);

static FUNCPTR_XGETIMAGE real_XGetImage = NULL;
static FUNCPTR_XSHMGETIMAGE real_XShmGetImage = NULL;
static FUNCPTR__XREPLY real__XReply = NULL; // Used specifically for blocking of "recordmydesktop" application only
static FUNCPTR_XCOPYAREA real_XCopyArea = NULL;

__attribute__((visibility("default"))) XImage *XGetImage (
  Display *display,
  Drawable d,
  int x,
  int y,
  unsigned int width,
  unsigned int height,
  unsigned long plane_mask,
  int format) /* either XYPixmap or ZPixmap */
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11, "XGetImage() called: x=%d y=%d width=%d height=%d", x, y, width, height);

  if (real_XGetImage == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real_XGetImage = (FUNCPTR_XGETIMAGE)real_dlsym(RTLD_NEXT, "XGetImage");

    if (real_XGetImage == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::X11, "XGetImage(): real_XGetImage == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  XImage *image = real_XGetImage(display, d, x, y, width, height, plane_mask, format);

  if (screenCaptureEnabled() == false && isAWindow(display, d))
    eraseImage(image);

  return image;
}

__attribute__((visibility("default"))) Bool XShmGetImage (
  Display *display,
  Drawable d,
  XImage *image,
  int x, int y,
  unsigned long plane_mask)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11, "XShmGetImage() called: x=%d y=%d", x, y);

  if (real_XShmGetImage == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real_XShmGetImage = (FUNCPTR_XSHMGETIMAGE)real_dlsym(RTLD_NEXT, "XShmGetImage");

    if (real_XShmGetImage == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::X11, "XShmGetImage(): real_XShmGetImage == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  Bool status = real_XShmGetImage(display, d, image, x, y, plane_mask);

  if (screenCaptureEnabled() == false && isAWindow(display, d))
    eraseImage(image);

  return status;
}

// Used specifically for blocking of "recordmydesktop" application only
__attribute__((visibility("default"))) Status _XReply (
  Display *display,
  xReply *reply,
  int extra,
  Bool discard)
{
  if (real__XReply == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real__XReply = (FUNCPTR__XREPLY)real_dlsym(RTLD_NEXT, "_XReply");

    if (real__XReply == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::X11, "_XReply(): real__XReply == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  // Must do this *before* calling real_XReply()
  void *request = display->last_req;

  extern char *__progname;

  if (screenCaptureEnabled() == false && strcmp(__progname, "recordmydesktop") == 0)
  {
    if (((xGetImageReq *)request)->reqType == X_GetImage)
    {
      tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11, "_XReply(): Processing xGetImageReq, request=%lu, x=%d, y=%d, width=%d, height=%d",
        display->request,
        ((xGetImageReq *)request)->x,
        ((xGetImageReq *)request)->y,
        ((xGetImageReq *)request)->width,
        ((xGetImageReq *)request)->height);

      ((xGetImageReq *)request)->width = 0;
      ((xGetImageReq *)request)->height = 0;
    }
    else if (((xShmGetImageReq *)request)->reqType == X11ShmSupervisor_shm_opcode(display) &&
             ((xShmGetImageReq *)request)->shmReqType == X_ShmGetImage)
    {
      tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11, "_XReply(): Processing xShmGetImageReq, request=%lu, x=%d, y=%d, width=%d, height=%d",
        display->request,
        ((xShmGetImageReq *)request)->x,
        ((xShmGetImageReq *)request)->y,
        ((xShmGetImageReq *)request)->width,
        ((xShmGetImageReq *)request)->height);

      ((xShmGetImageReq *)request)->width = 0;
      ((xShmGetImageReq *)request)->height = 0;
    }
  }

  return real__XReply(display, reply, extra, discard);
}

extern char *__progname; // For interim fix for ACL-180 Disappearing CWA toolbar icons.

__attribute__((visibility("default"))) int XCopyArea (
  Display *display,
  Drawable src,
  Drawable dest,
  GC gc,
  int src_x,
  int src_y,
  unsigned int width,
  unsigned int height,
  int dest_x,
  int dest_y
)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11, "XCopyArea() called: src=0x%lx, dest=0x%lx, src_x=%d, src_y=%d, width=%d, height=%d, dest_x=%d dest_y=%d", src, dest, src_x, src_y, width, height, dest_x, dest_y);

  if (real_XCopyArea == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real_XCopyArea = (FUNCPTR_XCOPYAREA)real_dlsym(RTLD_NEXT, "XCopyArea");

    if (real_XCopyArea == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::X11, "XCopyArea(): real_XCopyArea == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  if (src != dest &&
      strcmp(__progname, "wfica") != 0 && // Interim fix for ACL-180 Disappearing CWA toolbar icons.
      screenCaptureEnabled() == false && isAWindow(display, src))
  {
    return XFillRectangle(display, dest, gc, dest_x, dest_y, width, height);
  }

  return real_XCopyArea(display, src, dest, gc, src_x, src_y, width, height, dest_x, dest_y);
}

static const INTERCEPT intercepts[] =
{
  { "XGetImage", (void *)XGetImage, RTLD_NEXT },
  { "XShmGetImage", (void *)XShmGetImage, RTLD_NEXT },
  { "XCopyArea", (void *)XCopyArea, RTLD_NEXT }
};

void _init_X11(void)
{
  //////
  //
  // SEC-9 - Do not call trace here as it can lead to performance issues
  //
  //////

  registerIntercepts(intercepts, sizeof(intercepts)/sizeof(intercepts[0]));
}

#ifdef __cplusplus
}
#endif
