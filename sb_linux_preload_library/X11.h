#ifndef __X11__H
#define __X11__H
#include <X11/X.h>
#include <X11/Xlib.h>

#ifdef __cplusplus
extern "C" {
#endif

void _init_X11(void);

#ifdef __cplusplus
}
#endif

#endif
