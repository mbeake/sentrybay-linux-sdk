#include "XErrorTrap.h"

#include <mutex>

static std::mutex g_mutex;
static XErrorTrap *g_trapOwner = NULL;

static int XServerErrorHandler(Display *display, XErrorEvent *error_event)
{
  g_trapOwner->setErrorCode(error_event->error_code);
  return 0;
}

XErrorTrap::XErrorTrap(Display *display)
{
  g_mutex.lock();

  g_trapOwner = this;
  m_errorCode = 0;

  m_originalErrorHandler = XSetErrorHandler(XServerErrorHandler);
}

XErrorTrap::~XErrorTrap()
{
  XSetErrorHandler(m_originalErrorHandler);

  g_trapOwner = NULL;

  g_mutex.unlock();
}
