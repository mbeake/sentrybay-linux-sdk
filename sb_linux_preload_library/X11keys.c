#ifndef __cplusplus
#define _GNU_SOURCE // for RTLD_NEXT
#endif

#include <dlfcn.h> // for RTLD_NEXT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>

#include "X11ShmSupervisor.h"
#include "trace.h"
#include "SentryClient.h"
#include "SentryBayKeys.h"
#include "SentryBayLib.h"
#include "X11.h"
#include "dlfcn.h" // for real_dlsym

// Checking whether protection is on requires a message to the service, so we tend to check this fairly late
// (only after confirming the event is keystroke event)

#ifdef __cplusplus
extern "C" {
#endif

typedef int(*FUNCPTR_XNEXTEVENT) (Display *, XEvent *);
typedef int (*FUNCPTR_XPEEKEVENT) (Display *, XEvent *);
typedef int (*FUNCPTR_XWINDOWEVENT) (Display *, Window, long, XEvent *);
typedef Bool (*FUNCPTR_XCHECKWINDOWEVENT) (Display *, Window, long, XEvent *);
typedef int (*FUNCPTR_XMASKEVENT) (Display *, long, XEvent *);
typedef Bool (*FUNCPTR_XCHECKMASKEVENT) (Display *, long, XEvent *);
typedef Bool (*FUNCPTR_XCHECKTYPEDEVENT) (Display *, int, XEvent *);
typedef Bool (*FUNCPTR_XCHECKTYPEDWINDOWEVENT) (Display *, Window, int, XEvent *);
typedef Bool (*FUNCPTR_XQUERYEXTENSION) (Display *, _Xconst char *, int *, int *, int *);
typedef Bool (*FUNCPTR_XGETEVENTDATA)(Display *, XGenericEventCookie *);

static FUNCPTR_XNEXTEVENT realXNextEvent = NULL;
static FUNCPTR_XPEEKEVENT realXPeekEvent = NULL;
static FUNCPTR_XWINDOWEVENT realXWindowEvent = NULL;
static FUNCPTR_XCHECKWINDOWEVENT realXCheckWindowEvent = NULL;
static FUNCPTR_XMASKEVENT realXMaskEvent = NULL;
static FUNCPTR_XCHECKMASKEVENT realXCheckMaskEvent = NULL;
static FUNCPTR_XCHECKTYPEDEVENT realXCheckTypedEvent = NULL;
static FUNCPTR_XCHECKTYPEDWINDOWEVENT realXCheckTypedWindowEvent = NULL;
static FUNCPTR_XQUERYEXTENSION real_XQueryExtension = NULL;
static FUNCPTR_XGETEVENTDATA realXGetEventData = NULL;

void sendFocusInMessage( pid_t pid, Window wid )
{
    // It's essentially the pid which is protected (we don't necessarily have access to a helpful window id in
    // keyboard events), however sending Window as well protects us from any problems with asynchronous processing
    // of focus messages when switching between Windows in the same process.
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "Sending focus in message with pid:%i wid:0x%lx", pid, wid );

    sb_sdk_client::SentryClient client;
    client.FocusInWindow( pid, wid );
}

void sendFocusOutMessage( pid_t pid, Window wid )
{
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "Sending focus out message with pid:%i wid:0x%lx", pid, wid );

    sb_sdk_client::SentryClient client;
    client.FocusOutWindow( pid, wid );
}

static void handleFocusEvents( XFocusChangeEvent* focus_event )
{
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "FocusChange: serial:%ul mode:%i detail:%i", focus_event->serial, focus_event->mode, focus_event->detail);
    int revert_to = RevertToNone;
    Window focus_window = None;
    if( focus_event->display ) XGetInputFocus(focus_event->display, &focus_window, &revert_to);
    if( focus_event->type == FocusIn )
    {
        // Sanity check: do not send focus in unless the X server agrees the event window is currently in focus.
        if( focus_window == PointerRoot || focus_window == None || focus_window == focus_event->window )
        {
            sendFocusInMessage( getpid(), focus_event->window );
            // There are no circumstances in which an in-focus window should be receiving
            // scrambled keys, so we can update the cached value rather than waiting for
            // a response from the service.
            setCachedValue( false );
        }
        else
        {
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::X11KEYS, "Do not send focus_in because window does not have input focus. focus_window:0x%lx  pid:%i wid:0x%lx", focus_window, getpid(), focus_event->window );
        }
    }
    else if( focus_event->type == FocusOut )
    {
        // Sanity check: do not send focus out unless the X server agrees the event window is currently not in focus.
        if( focus_window == PointerRoot || focus_window == None || focus_window != focus_event->window )
        {
            sendFocusOutMessage( getpid(), focus_event->window );
            // We could call setCachedValue( true ) here, but that might scramble keys when it should not
        }
        else
        {
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::X11KEYS, "Do not send focus_out because window does have input focus. pid:%i event wid:0x%lx focus wid:0x%lx", getpid(), focus_event->window, focus_window );
        }
    }
}

static void handleDestroyWindowEvents( XDestroyWindowEvent* destroy_event )
{
    // The service will ignore the pid if we send zero, which is what we want
    // (the window should always be considered not in focus if it has been destroyed)
    sendFocusOutMessage( (pid_t)0, destroy_event->window );
}

static void handleKeyboardEvents( Display* display, XEvent *event_return )
{
    if (xi_key_press_type == INVALID_EVENT_TYPE || xi_key_release_type == INVALID_EVENT_TYPE)
        read_xinput_event_ids();

    // substitute random keycode in sensible range using event time as seed value
    if( (event_return->type == KeyPress || event_return->type == KeyRelease) )
    {
        XKeyEvent* ke = (XKeyEvent*)event_return;
        if( ke->send_event == False && // ignore artificially generated key events
            isModifiableKeyCode(display, NULL, ke->keycode) &&
            isSBkeyProtectionOn()  )
        {
            ke->keycode = (ke->time % 50) + 10;
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "type is X11 key event:%u", ke->keycode );
        }
    }
    else if( (event_return->type == xi_key_press_type || event_return->type == xi_key_release_type) )
    {
        XDeviceKeyEvent* dke = (XDeviceKeyEvent*)event_return;
        if( dke->send_event == False && // ignore artificially generated key events
            isModifiableKeyCode(display, NULL, dke->keycode) &&
            isSBkeyProtectionOn() )
        {
            dke->keycode = (dke->time % 50) + 10;
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "type is Xinput 1 key event:%u", dke->keycode );
        }
    }
}

static void handleEvents( Display* display, XEvent* event_return )
{
    static bool white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::X11KEYS);
    if( white_listed ) return;
    if( event_return->type == FocusIn ||
        event_return->type == FocusOut )
    {
        XFocusChangeEvent* fcev = (XFocusChangeEvent*)event_return;
        if( fcev->send_event == False ) // Ignore artificially generated focus events
        {
            handleFocusEvents( fcev );
        }
    }
    else if( event_return->type == DestroyNotify )
    {
        XDestroyWindowEvent* dwev = (XDestroyWindowEvent*)event_return;
        if( dwev->send_event == False ) // Ignore artificially generated destroy events
        {
            handleDestroyWindowEvents( dwev );
        }
    }
    else
    {
        handleKeyboardEvents( display, event_return );
    }
}

__attribute__((visibility("default"))) int XNextEvent(Display *display, XEvent *event_return)
{
    if (realXNextEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXNextEvent = (FUNCPTR_XNEXTEVENT)real_dlsym(RTLD_NEXT, "XNextEvent");

        if (realXNextEvent == NULL) return 0;
    }

    int rc = realXNextEvent( display, event_return );
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XNextEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) int XPeekEvent(Display *display, XEvent *event_return)
{
    if (realXPeekEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXPeekEvent = (FUNCPTR_XPEEKEVENT)real_dlsym(RTLD_NEXT, "XPeekEvent");
        if( realXPeekEvent == NULL)
        {
            return 0;
        }
    }
    int rc = realXPeekEvent( display, event_return );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XPeekEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) int XWindowEvent(Display *display, Window w, long event_mask, XEvent *event_return)
{
    if (realXWindowEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXWindowEvent = (FUNCPTR_XWINDOWEVENT)real_dlsym(RTLD_NEXT, "XWindowEvent");
        if( realXWindowEvent == NULL)
        {
            return 0;
        }
    }
    int rc = realXWindowEvent( display, w, event_mask, event_return );
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XWindowEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) Bool XCheckWindowEvent(Display *display, Window w, long event_mask, XEvent *event_return)
{
    if (realXCheckWindowEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXCheckWindowEvent = (FUNCPTR_XCHECKWINDOWEVENT)real_dlsym(RTLD_NEXT, "XCheckWindowEvent");
        if( realXCheckWindowEvent == NULL)
        {
            return 0;
        }
    }
    Bool rc = realXCheckWindowEvent( display, w, event_mask, event_return );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XCheckWindowEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) int XMaskEvent(Display *display, long event_mask, XEvent *event_return)
{
    if (realXMaskEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXMaskEvent = (FUNCPTR_XMASKEVENT)real_dlsym(RTLD_NEXT, "XMaskEvent");
        if( realXMaskEvent == NULL)
        {
            return 0;
        }
    }
    int rc = realXMaskEvent( display, event_mask, event_return );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XMaskEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) Bool XCheckMaskEvent(Display *display, long event_mask, XEvent *event_return)
{
    if (realXCheckMaskEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXCheckMaskEvent = (FUNCPTR_XCHECKMASKEVENT)real_dlsym(RTLD_NEXT, "XCheckMaskEvent");
        if( realXCheckMaskEvent == NULL)
        {
            return 0;
        }
    }
    Bool rc = realXCheckMaskEvent( display, event_mask, event_return );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XCheckMaskEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) Bool XCheckTypedEvent(Display *display, int event_type, XEvent *event_return)
{
    if (realXCheckTypedEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXCheckTypedEvent = (FUNCPTR_XCHECKTYPEDEVENT)real_dlsym(RTLD_NEXT, "XCheckTypedEvent");
        if( realXCheckTypedEvent == NULL)
        {
            return 0;
        }
    }
    Bool rc = realXCheckTypedEvent( display, event_type, event_return );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XCheckTypedEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

__attribute__((visibility("default"))) Bool XCheckTypedWindowEvent(Display *display, Window w, int event_type, XEvent *event_return)
{
    if (realXCheckTypedWindowEvent == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXCheckTypedWindowEvent = (FUNCPTR_XCHECKTYPEDWINDOWEVENT)real_dlsym(RTLD_NEXT, "XCheckTypedWindowEvent");
        if( realXCheckTypedWindowEvent == NULL)
        {
            return 0;
        }
    }
    Bool rc = realXCheckTypedWindowEvent( display, w, event_type, event_return );

    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XCheckTypedWindowEvent called. Type is:%i", event_return->type );

    handleEvents( display, event_return );

    return rc;
}

// For xinput2, the data is kept in the cookie rather than the event, so we need to
// intercept XGetEventData rather than XNextEvent
__attribute__((visibility("default"))) Bool XGetEventData(Display *display, XGenericEventCookie *cookie)
{
    if (realXGetEventData == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        realXGetEventData = (FUNCPTR_XGETEVENTDATA)real_dlsym(RTLD_NEXT, "XGetEventData");
        if( realXGetEventData == NULL)
        {
            return 0;
        }
    }
    Bool rv = realXGetEventData( display, cookie );

    static bool is_white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::X11KEYS);
    if (is_white_listed) return rv;

    if( sb_checked_input_extension == NOT_CHECKED )
    {
        int first_event_return, first_error_return;
        if( !XQueryExtension(display, "XInputExtension", &sb_xi_opcode, &first_event_return, &first_error_return) )
        {
            sb_checked_input_extension = NOT_PRESENT;
        }
        else
        {
            sb_checked_input_extension = PRESENT;
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "sb_xi_opcode is:%i", sb_xi_opcode );
        }
    }

    if( rv && sb_checked_input_extension == PRESENT &&
        cookie->type == GenericEvent && cookie->extension == sb_xi_opcode )
    {
        switch( cookie->evtype )
        {
            case XI_RawKeyPress:
            case XI_RawKeyRelease:
            {
                XIRawEvent* re = (XIRawEvent*)cookie->data;
                if( re->send_event == False &&
                    isModifiableKeyCode(display, NULL, re->detail) &&
                    isSBkeyProtectionOn() )
                {
                    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "Modifying xi2_rawkey" );
                    re->detail = (re->time % 50) + 10;
                }
            }
            break;

            case XI_KeyPress:
            case XI_KeyRelease:
            {
                XIDeviceEvent* de = (XIDeviceEvent*)cookie->data;

                if( de->send_event == False &&
                    isModifiableKeyCode(display, NULL, de->detail) &&
                    isSBkeyProtectionOn() )
                {
                    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "Modifying xi2_key" );
                    de->detail = (de->time % 50) + 10;
                }
            }
            break;
        }
    }
    return rv;
}

// XRecordQueryVersion is overridden to stop malware using the XRecord extension. Note there is no way
// to switch this off, all software will think the XRecord extension is not available for as long as the preload
// is in use (otherwise protection would be bypassed if malware started before protection was switched on).
// Display: Specifies the connection to the X server.
// cmajor_return: Returns the extension protocol major version in use.
// cminor_return: Returns the extension protocol minor version in use.
__attribute__((visibility("default"))) Status XRecordQueryVersion (Display *display, int *cmajor_return, int *cminor_return)
{
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XRecordQueryVersion called : %i , %i", *cmajor_return, *cminor_return);

    if( cmajor_return )
    {
        *cmajor_return = 0;
    }
    if( cminor_return )
    {
        *cminor_return = 0;
    }
    return 0; // Zero means XRecord extension not available
}

__attribute__((visibility("default"))) Bool XQueryExtension (
    Display *display,
    _Xconst char *name,
    int *major_opcode_return,
    int *first_event_return,
    int *first_error_return
)
{
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "XQueryExtension() called: name=%s", name);

    // XRecordQueryVersion and XQueryExtension are overridden to stop malware using the XRecord extension. Note there is no way
    // to switch this off, all software will think the XRecord extension is not available for as long as the preload
    // is in use (otherwise protection would be bypassed if malware started before protection was switched on).
    // Display: Specifies the connection to the X server.
    // cmajor_return: Returns the extension protocol major version in use.
    // cminor_return: Returns the extension protocol minor version in use.
    if( strcmp(name, "RECORD") == 0 )
    {
        if( major_opcode_return ) *major_opcode_return = 0;
        if( first_event_return ) *first_event_return = 0;
        if( first_error_return ) *first_error_return = 0;

        return 0; // Zero means extension not available
    }

    if (real_XQueryExtension == NULL)
    {
        // Lookup the symbol using the "real" dlsym().
        real_XQueryExtension = (FUNCPTR_XQUERYEXTENSION)real_dlsym(RTLD_NEXT, "XQueryExtension");

        if (real_XQueryExtension == NULL)
        {
            trace(TRACE_FATAL, SUBSYSTEM_NAMES::X11KEYS, "XQueryExtension(): real_XQueryExtension == NULL Shouldn't happen");
            exit(-1); // Shouldn't happen
        }
    }

    Bool status = real_XQueryExtension(display, name, major_opcode_return, first_event_return, first_error_return);

    static bool is_white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::X11KEYS);
    if (is_white_listed) return status;

    if (status == True && strcmp(name, "MIT-SHM") == 0)
    {
        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::X11KEYS, "X11 MIT-SHM Extension: shm_opcode=%d", *major_opcode_return);

        X11ShmSupervisor_notify_shm_opcode(display, *major_opcode_return);
    }

    return status;
}

static const INTERCEPT intercepts[] =
{
    { "XNextEvent", (void *)XNextEvent, RTLD_NEXT },
    { "XPeekEvent", (void *)XPeekEvent, RTLD_NEXT },
    { "XWindowEvent", (void *)XWindowEvent, RTLD_NEXT },
    { "XCheckWindowEvent", (void *)XCheckWindowEvent, RTLD_NEXT },
    { "XMaskEvent", (void *)XMaskEvent, RTLD_NEXT },
    { "XCheckMaskEvent", (void *)XCheckMaskEvent, RTLD_NEXT },
    { "XCheckTypedEvent", (void *)XCheckTypedEvent, RTLD_NEXT },
    { "XCheckTypedWindowEvent", (void *)XCheckTypedWindowEvent, RTLD_NEXT },
    { "XQueryExtension", (void *)XQueryExtension, RTLD_NEXT },
    { "XGetEventData", (void *)XGetEventData, RTLD_NEXT }
};

void _init_X11keys(void)
{
    //////
    //
    // SEC-9 - Do not call trace here as it can lead to performance issues
    //
    //////

    registerIntercepts(intercepts, sizeof(intercepts)/sizeof(intercepts[0]));
}

#ifdef __cplusplus
}
#endif
