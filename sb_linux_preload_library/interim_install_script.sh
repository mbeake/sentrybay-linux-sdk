INSTALLDIR=/usr/local/lib/sentrybay
PRELOAD_LIBRARY=libsentrybay
USER=test

echo "Un-installing ${PRELOAD_LIBRARY}.so from /etc/ld.so.preload"
rm -f /etc/ld.so.preload

echo "Installing ${PRELOAD_LIBRARY}.so into ${INSTALLDIR}"
mkdir -p ${INSTALLDIR}
cp -p ${PRELOAD_LIBRARY}.so ${INSTALLDIR}/${PRELOAD_LIBRARY}.so.1.0
ln -s -f ${INSTALLDIR}/${PRELOAD_LIBRARY}.so.1.0 ${INSTALLDIR}/${PRELOAD_LIBRARY}.so

echo "Installing service"
mkdir -p /usr/local/bin/sentrybay/logs
cp ../sentry_service/service/executable/sentryservice /usr/local/bin/sentrybay/
chmod +x /usr/local/bin/sentrybay/sentryservice
#todo Chris needs to advise what permissions need to be set up

echo "Blocking D-Bus screen capture interfaces..." 
echo "org.gnome.Shell.Screenshot" 
cp ../proof_of_concept/D-Bus/org.gnome.Shell.Screenshot.conf /etc/dbus-1/session.d/
echo "org.gnome.Shell.Screencast" 
cp ../proof_of_concept/D-Bus/org.gnome.Shell.Screencast.conf /etc/dbus-1/session.d/

echo "Enabling ${PRELOAD_LIBRARY}.so at boot-time"
cp ld.so.preload /etc/
