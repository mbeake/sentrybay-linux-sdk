#include <map>
#include <mutex>

#include "X11ShmSupervisor.h"

// The following is to eliminate "relocation R_X86_64_PC32 against undefined hidden symbol `__dso_handle' can not be used when making a shared object" linker error.
extern "C"
{
  void *__dso_handle = (void*) &__dso_handle;
}

static std::map<Display *, int> g_shm_codes;
static std::mutex g_shm_codes_mutex;

#ifdef __cplusplus
extern "C" {
#endif

int X11ShmSupervisor_shm_opcode(Display *display)
{
  std::lock_guard<std::mutex> lock(g_shm_codes_mutex);

  auto iterator = g_shm_codes.find(display);

  return iterator == g_shm_codes.end() ? -1 : iterator->second;
}

void X11ShmSupervisor_notify_shm_opcode(Display *display, int shm_opcode)
{
  std::lock_guard<std::mutex> lock(g_shm_codes_mutex);

  g_shm_codes[display] = shm_opcode;
}

#ifdef __cplusplus
}
#endif
