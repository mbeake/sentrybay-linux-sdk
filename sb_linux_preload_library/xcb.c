#include "SentryBayLib.h"

#ifndef __cplusplus
#define _GNU_SOURCE // for RTLD_NEXT
#endif
#include <dlfcn.h> // for RTLD_NEXT

#include <xcb/xcb.h>
#include <xcb/shm.h>

#include <stdio.h> // for printf()
#include <stdlib.h> // for exit()
#include <string.h> // for memset()

#include "dlfcn.h" // for real_dlsym()
#include "xcb.h"
#include "trace.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef xcb_get_image_reply_t *(*FUNCPTR_XCB_GET_IMAGE_REPLY) (
  xcb_connection_t *, xcb_get_image_cookie_t, xcb_generic_error_t **);
typedef xcb_shm_get_image_cookie_t (*FUNCPTR_XCB_SHM_GET_IMAGE) (
  xcb_connection_t *, xcb_drawable_t, int16_t, int16_t, uint16_t, uint16_t, uint32_t, uint8_t, xcb_shm_seg_t, uint32_t);
typedef xcb_shm_get_image_cookie_t (*FUNCPTR_XCB_SHM_GET_IMAGE_UNCHECKED) (
  xcb_connection_t *, xcb_drawable_t, int16_t, int16_t, uint16_t, uint16_t, uint32_t, uint8_t, xcb_shm_seg_t, uint32_t);

static FUNCPTR_XCB_GET_IMAGE_REPLY real_xcb_get_image_reply = NULL;
static FUNCPTR_XCB_SHM_GET_IMAGE real_xcb_shm_get_image = NULL;
static FUNCPTR_XCB_SHM_GET_IMAGE_UNCHECKED real_xcb_shm_get_image_unchecked = NULL;

__attribute__((visibility("default"))) xcb_get_image_reply_t *xcb_get_image_reply (
  xcb_connection_t *connection,
  xcb_get_image_cookie_t cookie,
  xcb_generic_error_t **e)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::XCB, "xcb_get_image_reply() called: sequence=%d", cookie.sequence);

  if (real_xcb_get_image_reply == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real_xcb_get_image_reply = (FUNCPTR_XCB_GET_IMAGE_REPLY)real_dlsym(RTLD_NEXT, "xcb_get_image_reply");

    if (real_xcb_get_image_reply == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::XCB, "xcb_get_image_reply(): real_xcb_get_image_reply == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  xcb_get_image_reply_t *image_reply = real_xcb_get_image_reply(connection, cookie, e);

  if (image_reply && screenCaptureEnabled() == false)
  {
    int bytes = xcb_get_image_data_length(image_reply);
    uint8_t *data = xcb_get_image_data(image_reply);

    memset(data, 0, bytes);
  }

  return image_reply;
}

__attribute__((visibility("default"))) xcb_shm_get_image_cookie_t xcb_shm_get_image (
  xcb_connection_t *connection,
  xcb_drawable_t drawable,
  int16_t x,
  int16_t y,
  uint16_t width,
  uint16_t height,
  uint32_t plane_mask,
  uint8_t format,
  xcb_shm_seg_t shmseg,
  uint32_t offset
)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::XCB, "xcb_shm_get_image() called: x=%d y=%d width=%d height=%d shmseg=%d", x, y, width, height, shmseg);

  if (real_xcb_shm_get_image == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real_xcb_shm_get_image = (FUNCPTR_XCB_SHM_GET_IMAGE)real_dlsym(RTLD_NEXT, "xcb_shm_get_image");

    if (real_xcb_shm_get_image == NULL)
    {
      // Lookup the symbol using the "real" dlsym() directly from it's shared library.
      void *handle = dlopen("libxcb-shm.so", RTLD_NOW | RTLD_GLOBAL);
      if (handle)
      {
        real_xcb_shm_get_image = (FUNCPTR_XCB_SHM_GET_IMAGE)real_dlsym(handle, "xcb_shm_get_image");
      }
    }

    if (real_xcb_shm_get_image == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::XCB, "xcb_shm_get_image(): real_xcb_shm_get_image == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  if (screenCaptureEnabled() == false)
  {
    width = 0;
    height = 0;
  }

  return real_xcb_shm_get_image(connection, drawable, x, y, width, height, plane_mask, format, shmseg, offset);
}

__attribute__((visibility("default"))) xcb_shm_get_image_cookie_t xcb_shm_get_image_unchecked (
  xcb_connection_t *connection,
  xcb_drawable_t drawable,
  int16_t x,
  int16_t y,
  uint16_t width,
  uint16_t height,
  uint32_t plane_mask,
  uint8_t format,
  xcb_shm_seg_t shmseg,
  uint32_t offset
)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::XCB, "xcb_shm_get_image_unchecked() called: x=%d y=%d width=%d height=%d shmseg=%d", x, y, width, height, shmseg);

  if (real_xcb_shm_get_image_unchecked == NULL)
  {
    // Lookup the symbol using the "real" dlsym().
    real_xcb_shm_get_image_unchecked = (FUNCPTR_XCB_SHM_GET_IMAGE_UNCHECKED)real_dlsym(RTLD_NEXT, "xcb_shm_get_image_unchecked");

    if (real_xcb_shm_get_image_unchecked == NULL)
    {
      // Lookup the symbol using the "real" dlsym() directly from it's shared library.
      void *handle = dlopen("libxcb-shm.so", RTLD_NOW | RTLD_GLOBAL);
      if (handle)
      {
        real_xcb_shm_get_image_unchecked = (FUNCPTR_XCB_SHM_GET_IMAGE_UNCHECKED)real_dlsym(handle, "xcb_shm_get_image_unchecked");
      }
    }

    if (real_xcb_shm_get_image_unchecked == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::XCB, "xcb_shm_get_image_unchecked(): real_shm_get_image_unchecked == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  if (screenCaptureEnabled() == false)
  {
    width = 0;
    height = 0;
  }

  return real_xcb_shm_get_image_unchecked(connection, drawable, x, y, width, height, plane_mask, format, shmseg, offset);
}

static const INTERCEPT intercepts[] =
{
  { "xcb_get_image_reply", (void *)xcb_get_image_reply, RTLD_NEXT },
  { "xcb_shm_get_image", (void *)xcb_shm_get_image, RTLD_NEXT },
  { "xcb_shm_get_image_unchecked", (void *)xcb_shm_get_image_unchecked, RTLD_NEXT }
};

void _init_xcb(void)
{
  //////
  //
  // SEC-9 - Do not call trace here as it can lead to performance issues
  //
  //////

  registerIntercepts(intercepts, sizeof(intercepts)/sizeof(intercepts[0]));
}

#ifdef __cplusplus
}
#endif
