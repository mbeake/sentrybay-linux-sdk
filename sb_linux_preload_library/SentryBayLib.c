#include "SentryBayLib.h"
#include "SentryBayKeys.h"
#include "dlfcn.h"
#include "X11.h"
#include "xcb.h"
#include "SentryClient.h"
#include "utils.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((visibility("default"))) void _init(void)
{
  //////
  //
  // SEC-9 - Do not call trace here as it can lead to performance issues
  //
  //////

  // Don't dymaically intercept call for whitelisted process
  if(utils::isWhitelisted(SUBSYSTEM_NAMES::LIB_INIT)) return;

  _init_dlfcn();
  _init_X11();
  _init_xcb();
  _init_X11keys();
  _init_xcb_keys();
}

#define MAX_INTERCEPTS 100

static unsigned _interceptCount = 0;

static INTERCEPT _intercepts[MAX_INTERCEPTS];

bool screenCaptureEnabled()
{
  static bool is_white_listed = utils::isWhitelisted(SUBSYSTEM_NAMES::ILOCK);
  if (is_white_listed) return true;

  tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::ILOCK, "screenCaptureEnabled(): pid: %i", getpid() );

  bool ilockstate = false;

  sb_sdk_client::SentryClient client;

  if (client.GetILockState(ilockstate) != 0) return true;

  return ilockstate == false;
}

void registerIntercepts(const INTERCEPT intercepts[], unsigned count)
{
  //////
  //
  // SEC-9 - Do not call trace here as it can lead to performance issues
  //
  //////

  for (unsigned i = 0; i < count; i++)
  {
    if (_interceptCount < MAX_INTERCEPTS)
    {
      _intercepts[_interceptCount++] = intercepts[i];
    }
  }
}

void *lookupIntercept(const char *symbol, void **handle)
{
  for (unsigned i = 0; i < _interceptCount; i++)
  {
    if (strcmp(symbol, _intercepts[i].symbol) == 0)
    {
      if (handle) *handle = _intercepts[i].handle;
      return _intercepts[i].function;
    }
  }

  return NULL;
}

void setInterceptHandle(const char *symbol, void *handle)
{
  for (unsigned i = 0; i < _interceptCount; i++)
  {
    if (strcmp(symbol, _intercepts[i].symbol) == 0)
      _intercepts[i].handle = handle;
  }
}

#ifdef __cplusplus
}
#endif
