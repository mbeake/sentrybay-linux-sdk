#include <iostream>

#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/extensions/XI2.h>

#include "WindowAntiKeylogging.h"

int main( int argc, char** argv )
{

    Window wid = atol(argv[1]);
    Window wid2 = atol(argv[2]);
    
    Display* display = XOpenDisplay(NULL);
    
    EnableKeystrokeProtection();

    std::cout << "Registering window id:" << argv[1] << " and:" << argv[2] << " for protection" << std::endl;

    RegisterWindowForAntiKeylogging( display, wid );
    RegisterWindowForAntiKeylogging( display, wid2 );

    Window focus_return;
    int revert_to_return;

    XGetInputFocus( display, &focus_return, &revert_to_return );

    std::cout << "Window with input focus is:0x" << std::hex << focus_return << std::endl;

    XEvent ev;
    
    while(1)
    {
        XNextEvent(display, &ev);
        
        //std::cout << "Process event:" << ev.type << std::endl;
        //std::string focus_in = "In";
        
        switch( ev.type )
        {
            case FocusIn:
            case FocusOut:
            {
                std::string focus_type = "In";
                if( ev.type == FocusOut )
                {
                    focus_type = "Out";    
                }
                XFocusChangeEvent* fcev = (XFocusChangeEvent*)(&ev);

                const char *detail = "unknown";

                switch (fcev->detail)
                {
                    case XINotifyAncestor: detail = "NotifyAncestor"; break;
                    case XINotifyVirtual: detail = "NotifyVirtual"; break;
                    case XINotifyInferior: detail = "NotifyInferior"; break;
                    case XINotifyNonlinear: detail = "NotifyNonlinear"; break;
                    case XINotifyNonlinearVirtual: detail = "NotifyNonlinearVirtual"; break;
                    case XINotifyPointer: detail = "NotifyPointer"; break;
                    case XINotifyPointerRoot: detail = "NotifyPointerRoot"; break;
                    case XINotifyDetailNone: detail = "NotifyDetailNone"; break;
                }

                std::cout << "Test program: Focus" << focus_type << " detail:" << detail << " for window:" << fcev->window << std::endl;

                XGetInputFocus( display, &focus_return, &revert_to_return );

                std::cout << "Window with input focus is:" << focus_return << std::endl;

            }
            break;
        }
    }
    
    return 0;
}
