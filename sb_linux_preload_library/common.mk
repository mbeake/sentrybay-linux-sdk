include ../global_common.mk

INCS := -I../trace -I../include
DEPLIBS := ../trace/$(LIBS)/libtrace.a
FLAGS := -fvisibility=hidden -Wall -fPIC -DPIC -Werror -O3 $(INCS)
CFLAGS := $(FLAGS)
CXXFLAGS := $(FLAGS) -std=c++11
X11LIBS := -lX11 -lXext -lxcb -lX11-xcb -lXi

INSTALLDIR := /usr/local/lib/sentrybay

LIBSENTRYBAYFILES := dlfcn.c X11.c SentryBayKeys.c X11keys.c xcb_keys.c XErrorTrap.c xcb.c SentryBayLib.c X11ShmSupervisor.cpp

all: $(LIBS)/libsentrybay.so

$(LIBS)/libsentrybay.so: $(LIBSENTRYBAYFILES) $(DEPLIBS)
	@mkdir -p $(LIBS)
	@$(CXX) $(CXXFLAGS) $^ -pthread -shared -nostdlib -lgcc -lpthread -ldl -lc $(X11LIBS) -lstdc++ -o $@

.SILENT: $(DEPLIBS)

$(LIBS)/libsentrybay_bad.so: SentryBayLib_bad.c
	@mkdir -p $(LIBS)
	@$(CC) -shared -nostdlib -lgcc -o $@ $^

$(EXES)/TestXcb.exe: TestXcb.c
	@mkdir -p $(EXES)
	@$(CC) $(CXXFLAGS) $^ $(X11LIBS) -o $@

$(EXES)/GetXInputEventIDs: GetXInputEventIDs.c SentryBayKeys.c $(DEPLIBS)
	@mkdir -p $(EXES)
	@$(CC) -pthread $(CXXFLAGS) $^ $(X11LIBS) -o $@ 

install:
	@mkdir -p $(INSTALLDIR)
	@cp -p $(LIBS)/libsentrybay.so $(INSTALLDIR)/libsentrybay.so.1.0
	@ln -s -f $(INSTALLDIR)/libsentrybay.so.1.0 $(INSTALLDIR)/libsentrybay.so
	@cp -p $(LIBS)/libsentrybay_bad.so $(INSTALLDIR)/libsentrybay_bad.so.1.0
	@ln -s -f $(INSTALLDIR)/libsentrybay_bad.so.1.0 $(INSTALLDIR)/libsentrybay_bad.so

clean:
	@rm -rf $(OBJS) $(LIBS) $(EXES)
