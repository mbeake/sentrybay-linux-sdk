#include "SentryBayLib.h"

#ifndef __cplusplus
#define _GNU_SOURCE // for RTLD_NEXT
#endif
#include <dlfcn.h> // for RTLD_NEXT

#include <string.h>
#include <stdlib.h> // for exit()
#include <stdio.h> // for printf()

#include "dlfcn.h" // for real_dlsym()
#include "trace.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void *_dl_sym(void *, const char *, void *);

typedef void *(*FUNCPTR_DLSYM) (void *, const char *);

static FUNCPTR_DLSYM _real_dlsym = NULL;

void *real_dlsym(void *handle, const char *symbol)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::DLFCN, "real_dlsym() called: symbol=%s handle=%p - intercept not found, so using real_dlsym", symbol, handle);

  if (_real_dlsym == NULL)
  {
    // Lookup the symbol using an internal version of the "real" dlsym().
    _real_dlsym = (FUNCPTR_DLSYM)_dl_sym(RTLD_NEXT, "dlsym", (void *)real_dlsym);

    if (_real_dlsym == NULL)
    {
      trace(TRACE_FATAL, SUBSYSTEM_NAMES::DLFCN, "real_dlsym(): _real_dlsym == NULL Shouldn't happen");
      exit(-1); // Shouldn't happen
    }
  }

  void *intercept_handle = handle;
  lookupIntercept(symbol, &intercept_handle);

  return _real_dlsym(intercept_handle, symbol);
}

__attribute__((visibility("default"))) void *dlsym(void *handle, const char *symbol)
{
  tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::DLFCN, "dlsym() called: symbol=%s handle=%p", symbol, handle);

  // Interim workaround for ACL-178 Cannot launch Chromium
  if (handle == RTLD_NEXT &&
      (strcmp(symbol, "localtime") == 0 || strcmp(symbol, "localtime_r") == 0 ||
       strcmp(symbol, "localtime64") == 0 || strcmp(symbol, "localtime64_r") == 0))
  {
    void *handle = dlopen("libc.so.6", RTLD_LAZY);
    if (handle)
    {
      void *addr = real_dlsym(handle, symbol);
      dlclose(handle);
      return addr;
    }
    else trace(TRACE_ERROR, SUBSYSTEM_NAMES::DLFCN, "dlsym(): dlopen(\"libc.so.6\", RTLD_LAZY) returned NULL");
  }

  void *intercept_handle = RTLD_NEXT;
  void *intercept_function = lookupIntercept(symbol, &intercept_handle);

  if (intercept_function)
  {
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::DLFCN, "dlsym() called: symbol=%s handle=%p - intercept found", symbol, handle);
    // Needed when an application has dynamically loaded a shared library and performed a symbol lookup.
    // If this symbol is overridden by the SentryBay pre-load library, we need to make sure that the
    // SentryBay override looks up the symbol in the library dynamically loaded by the application.
    // NOTE: Don't do this for dlsym() !!
    if (strcmp(symbol, "dlsym") != 0) 
    {
        setInterceptHandle(symbol, handle);
    }
    else
    {
        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::DLFCN, "dlsym() not setting intercept handle for symbol=%s", symbol);
    }
  }

  return intercept_function ? intercept_function : real_dlsym(handle, symbol);
}

static const INTERCEPT intercepts[] =
{
  { "dlsym", (void *)dlsym, RTLD_NEXT }
};

void _init_dlfcn(void)
{
  //////
  //
  // SEC-9 - Do not call trace here as it can lead to performance issues
  //
  //////

  registerIntercepts(intercepts, sizeof(intercepts)/sizeof(intercepts[0]));
}

#ifdef __cplusplus
}
#endif

