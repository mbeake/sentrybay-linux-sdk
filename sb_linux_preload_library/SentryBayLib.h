#ifndef _SENTRYBAYLIB_H_
#define _SENTRYBAYLIB_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  const char *symbol;
  void *function;
  void *handle;
} INTERCEPT;

bool screenCaptureEnabled();

void registerIntercepts(const INTERCEPT [], unsigned count);
void *lookupIntercept(const char *symbol, void **handle);

// Needed when an application has dynamically loaded a shared library and performed a symbol lookup.
void setInterceptHandle(const char *symbol, void *handle);

#ifdef __cplusplus
}
#endif

#endif
