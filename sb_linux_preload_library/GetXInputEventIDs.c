#include "SentryBayKeys.h"
#include "trace.h"

int main()
{
    Display *display = XOpenDisplay( NULL );
    if ( display )
    {
        register_keyboard_events( display );
        XCloseDisplay( display );
        
        write_xinput_event_ids();
    }
    else
    {
    	trace( TRACE_ERROR, SUBSYSTEM_NAMES::GETIDS, "Failed to open connection to X11 server, XOpenDisplay() returned NULL." );
    }
}
