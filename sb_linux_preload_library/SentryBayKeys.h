#ifndef _SENTRYBAYKEYS_H
#define _SENTRYBAYKEYS_H

#include <xcb/xcb.h>
#include <X11/Xlib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define INVALID_EVENT_TYPE      -1

enum CHECKED_INPUT_EXTENSION { NOT_CHECKED, NOT_PRESENT, PRESENT };

extern int xi_key_press_type;
extern int xi_key_release_type;
extern int sb_xi_opcode;
extern CHECKED_INPUT_EXTENSION sb_checked_input_extension;

Bool checkPid();
int isSBkeyProtectionOn();
// Note, this will be overridden as soon as the thread fires again, however it can
// be helpful if we don't want to wait for the thread.
void setCachedValue( bool value );
int register_keyboard_events( Display* display );
int register_keyboard_events_xcb( xcb_connection_t* c );
bool isModifiableKeyCode( Display* display, xcb_connection_t* conn, unsigned int kc );

void write_xinput_event_ids();
void read_xinput_event_ids();

void _init_X11keys(void);
void _init_xcb_keys(void);

#ifdef __cplusplus
}
#endif

#endif



