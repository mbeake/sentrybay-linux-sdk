#include <X11/Xlib.h>
#include <X11/extensions/XShm.h>

#ifdef __cplusplus
extern "C" {
#endif

int X11ShmSupervisor_shm_opcode(Display *display);

void X11ShmSupervisor_notify_shm_opcode(Display *display, int shm_opcode);

#ifdef __cplusplus
}
#endif
