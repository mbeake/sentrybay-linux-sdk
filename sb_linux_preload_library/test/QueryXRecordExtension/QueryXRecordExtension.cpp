//============================================================================
// Name        : QueryXRecordExtension.cpp
// Author      : Paul Saint
// Version     : 1.0
// Copyright   :
// Description : Test program for querying the XRecord X11 extension.
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/extensions/record.h>

#include <xcb/xcb.h>
#include <xcb/record.h>

int main(void)
{
  const char * display_name = NULL; // ie use DISPLAY environment variable

  Display *display = XOpenDisplay(display_name);

  int default_screen = 0;
  xcb_connection_t *c = xcb_connect(display_name, &default_screen);

  if (display == NULL || c == NULL)
  {
    puts("QueryXRecordExtension: failed to open connection to X.Org server");

    return EXIT_FAILURE;
  }

  const char *extension_name = "RECORD";

  int major_opcode = 0;
  int first_event = 0;
  int first_error = 0;

  // Try XQueryExtension() API

  Bool present = XQueryExtension(display, extension_name, &major_opcode, &first_event, &first_error);

  if (present)
  {
    printf("QueryXRecordExtension: XQueryExtension() reports extension \"%s\" is present: major_opcode=%d first_event=%d first_error=%d\n",
           extension_name, major_opcode, first_event, first_error);
  }
  else
  {
    printf("QueryXRecordExtension: XQueryExtension() reports extension \"%s\" is NOT present\n", extension_name);
  }

  // Try xcb_query_extension() API

  xcb_generic_error_t *error;
  xcb_query_extension_cookie_t cookie = xcb_query_extension(c, strlen(extension_name), extension_name);
  const xcb_query_extension_reply_t *reply = xcb_query_extension_reply(c, cookie, &error);

  if (reply == NULL)
  {
    printf("QueryXRecordExtension: xcb_query_extension_reply() returned NULL\n");
  }
  else if (reply->present)
  {
    printf("QueryXRecordExtension: xcb_query_extension() reports extension \"%s\" is present: major_opcode=%d first_event=%d first_error=%d\n",
           extension_name, reply->major_opcode, reply->first_event, reply->first_error);
  }
  else
  {
    printf("QueryXRecordExtension: xcb_query_extension() reports extension \"%s\" is NOT present\n", extension_name);
  }

  // Try xcb_query_extension_unchecked() API

  cookie = xcb_query_extension_unchecked(c, strlen(extension_name), extension_name);
  reply = xcb_query_extension_reply(c, cookie, &error);

  if (reply == NULL)
  {
    printf("QueryXRecordExtension: xcb_query_extension_reply() returned NULL\n");
  }
  else if (reply->present)
  {
    printf("QueryXRecordExtension: xcb_query_extension_unchecked() reports extension \"%s\" is present: major_opcode=%d first_event=%d first_error=%d\n",
           extension_name, reply->major_opcode, reply->first_event, reply->first_error);
  }
  else
  {
    printf("QueryXRecordExtension: xcb_query_extension_unchecked() reports extension \"%s\" is NOT present\n", extension_name);
  }

  // Try xcb_get_extension_data() API

  reply = xcb_get_extension_data(c, &xcb_record_id);

  if (reply == NULL)
  {
    printf("QueryXRecordExtension: xcb_get_extension_data() returned NULL\n");
  }
  else if (reply->present)
  {
    printf("QueryXRecordExtension: xcb_get_extension_data() reports extension \"%s\" is present: major_opcode=%d first_event=%d first_error=%d\n",
         extension_name, reply->major_opcode, reply->first_event, reply->first_error);
  }
  else
  {
    printf("QueryXRecordExtension: xcb_get_extension_data() reports extension \"%s\" is NOT present\n", extension_name);
  }

  // Test XRecordQueryVersion() API

  int major_verson = 0;
  int minor_verson = 0;

  Status status = XRecordQueryVersion(display, &major_verson, &minor_verson);

  if (status != 0)
  {
    printf("QueryXRecordExtension: XRecordQueryVersion() reports extension \"%s\" version: major_verson=%d minor_verson=%d\n",
         extension_name, major_verson, minor_verson);
  }
  else
  {
    printf("QueryXRecordExtension: XRecordQueryVersion() reports extension \"%s\" is NOT present\n", extension_name);
  }

  // Test xcb_record_query_version() API

  xcb_record_query_version_cookie_t record_query_version_cookie =
    xcb_record_query_version(c, XCB_RECORD_MAJOR_VERSION, XCB_RECORD_MINOR_VERSION);

  xcb_record_query_version_reply_t *record_query_version_reply = xcb_record_query_version_reply(c, record_query_version_cookie, NULL);

  if (record_query_version_reply == NULL)
  {
    printf("QueryXRecordExtension: xcb_record_query_version() returned NULL\n");
  }
  else
  {
    printf("QueryXRecordExtension: xcb_record_query_version() reports extension \"%s\" version: major_version=%d minor_version=%d\n",
         extension_name, record_query_version_reply->major_version, record_query_version_reply->minor_version);
  }

  XCloseDisplay(display);
  xcb_disconnect(c);

  return EXIT_SUCCESS;
}
