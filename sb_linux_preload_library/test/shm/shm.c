#include <sys/shm.h>

#include <X11/Xlib.h>
#include <X11/Xlibint.h>
#include <X11/extensions/shmproto.h>
#include <X11/extensions/XShm.h>

int main(int argc, char *argv[])
{
  int x = 0;
  int y = 0;

  int width = 100;
  int height = 200;

  Display *dpy = XOpenDisplay(NULL);;

  // Shared memory setup

  int shm_opcode, shm_event_base, shm_error_base;

  XQueryExtension(dpy, "MIT-SHM", &shm_opcode, &shm_event_base, &shm_error_base);

  int screen = DefaultScreen(dpy);
  Visual *visual = DefaultVisual(dpy, screen);
  int depth = DefaultDepth(dpy, screen);
  Window root = RootWindow(dpy, screen);

  XShmSegmentInfo shminfo;

  XImage *image = XShmCreateImage(dpy, visual, depth, ZPixmap, NULL, &shminfo, width, height);

  shminfo.shmid = shmget(IPC_PRIVATE, image->bytes_per_line * image->height, IPC_CREAT|0777);
  shminfo.shmaddr = image->data = shmat(shminfo.shmid, NULL, 0);
  shminfo.readOnly = False;

  XShmAttach(dpy, &shminfo);

  // Get initial screen shot image using XShmGetImage()
  XShmGetImage(dpy, root, image, x, y, AllPlanes);

  // At this point in the code image->data should contain the image pixel data.

  // Get next screen shot image using _XGetRequest(), _XReply() and _XReadPad()
  LockDisplay(dpy);

  xShmGetImageReq *request = (xShmGetImageReq *)_XGetRequest(dpy, X_ShmGetImage, sizeof(xShmGetImageReq));
  request->reqType = shm_opcode;
  request->shmReqType = X_ShmGetImage;
  request->shmseg = shminfo.shmseg;

  request->drawable = root;
  request->x = x;
  request->y = y;
  request->width = width;
  request->height = height;
  request->planeMask = AllPlanes;
  request->format = ZPixmap;
  request->offset = 0;

  xShmGetImageReply reply;

  if (_XReply(dpy, (xReply *)&reply, 0, xFalse) && reply.length)
  {
    // I don't believe this section of code gets called because the image data is transferred
	// directly into the image->data field of the XImage shared memory object.
    long nbytes = (long)reply.length << 2;
    _XReadPad(dpy, image->data, nbytes);
  }

  // At this point in the code image->data should contain the image pixel data.

  UnlockDisplay(dpy);
  SyncHandle();

  XCloseDisplay(dpy);
}
