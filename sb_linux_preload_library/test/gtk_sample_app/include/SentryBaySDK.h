#ifndef __SENTRYBAY_SDK_H__
#define __SENTRYBAY_SDK_H__

#include <X11/Xlib.h>

#include "SystemStatus.h"

namespace sb_sdk
{
  // Functions for communicating with service. Return value in each case reflects whether the message
  // was successfully sent to the service.
  bool RegisterProcessForAntiKeylogging( Display* display, Window wid );
  bool DeregisterProcessForAntiKeylogging();

  // Functions for communicating with service. Return value in each case reflects whether the message
  // was successfully sent to the service.
  bool EnableScreenCaptureProtection();
  bool DisableScreenCaptureProtection();

  // Gets the protection status (true means the service responded with enabled).
  bool ScreenCaptureProtectionEnabled();

  // The SDK application needs to continuously poll the service with GetSystemStatus and take
  // appropriate action (e.g. warn the user and exit) if it does not receive a SECURE response.
  // The most obvious reason to not receive a SECURE response is if the service is not running.
  // GetSystemStatus() returns as soon as a non-SECURE status is discovered.
  StatusCode_t GetSystemStatus(void);

  // Gets the system status and extended status information.
  void GetSystemStatusVerbose(SystemStatus &system_status);
}

#endif
