#ifdef __cplusplus
extern "C"
{
#endif

    int wrapper_EnableScreenCaptureProtection();
    int wrapper_ScreenCaptureProtectionEnabled();
    int wrapper_DisableScreenCaptureProtection();
    int wrapper_RegisterWindowForAntiKeylogging(Display*, Window);
    int wrapper_DeregisterWindowForAntiKeylogging(Display*, Window);

#ifdef __cplusplus
}
#endif
