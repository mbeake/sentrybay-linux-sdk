#include "SentryBaySDK.h"
#include "sentryclient.h"

#include <sys/types.h>
#include <unistd.h>

namespace sb_sdk
{
  SystemStatus::SystemStatus()
  {
    m_status = STATUS_UNKNOWN;
    m_reason = REASON_UNKNOWN;
  }
}
