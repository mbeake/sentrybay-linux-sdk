#include "SentryBaySDK.h"
#include "sentryclient.h"

#include <sys/types.h>
#include <unistd.h>
#include"wrapper.h"

static Window get_focus_window()
{
  static Display *display = NULL;
  Window focus_window = 0;
  int revert_to;
  if (!display) display = XOpenDisplay("");
    XGetInputFocus(display, &focus_window, &revert_to);

  return focus_window;
}

namespace sb_sdk
{
  bool RegisterProcessForAntiKeylogging( Display* display, Window wid )
  {
    // 1. Register this process with the service to let it know that it's a protected application
    // 2. Make sure we're receiving focus change events on this window
    // 3. If the supplied window id happens to be the current in-focus window, we'll need to inform the service
    // The actual processing of focus change events is done in the preload library.
    if( display == NULL ) return false;

    SentryClient client;

    client.RegisterProcess( getpid() );

    if (wid)
    {
      XWindowAttributes window_attributes;
      if (XGetWindowAttributes( display, wid, &window_attributes ) && ( window_attributes.your_event_mask & FocusChangeMask ) == 0 )
      {
        // Only invoke XSelectInput() if we really need to ie. FocusChangeMask is *not* already set
        XSelectInput( display, wid, window_attributes.your_event_mask | FocusChangeMask);
      }

      if (wid == get_focus_window())
      {
        client.FocusInWindow( getpid(), wid );
      }
    }

    return true;
  }

  bool DeregisterProcessForAntiKeylogging()
  {
    SentryClient client;

    client.UnRegisterProcess( getpid() );

    return true;
  }

  bool EnableScreenCaptureProtection()
  {
    SentryClient client;

    client.SetILockState(true);

    return client.GetILockState() == true;
  }

  bool DisableScreenCaptureProtection()
  {
    SentryClient client;

    client.SetILockState(false);

    return client.GetILockState() == false;
  }

  bool ScreenCaptureProtectionEnabled()
  {
    SentryClient client;

    return client.GetILockState();
  }

  static void systemStatus(SentryClient::STATUS status, SystemStatus &system_status)
  {
    switch(status)
    {
    case SentryClient::COMPROMISED_NO_LIBRARY:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_SENTRYBAY_LIBRARY_MISSING);
      break;

    case SentryClient::COMPROMISED_NO_PRELOAD:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_LD_SO_PRELOAD_MISSING);
      break;

    case SentryClient::COMPROMISED_NO_ENTRY:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_LD_SO_PRELOAD_NO_SENTRYBAY_ENTRY);
      break;

    case SentryClient::COMPROMISED_PROC_DENIED:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_PROC_OPEN_FAILED);
      break;

    case SentryClient::TENTATIVE_NOT_ENTRY_ZERO:
      system_status.status(STATUS_TENTATIVE);
      system_status.reason(REASON_LD_SO_PRELOAD_SENTRYBAY_NOT_FIRST_ENTRY);
      break;

    case SentryClient::TENTATIVE_PRELOAD_DETECTED:
      system_status.status(STATUS_TENTATIVE);
      system_status.reason(REASON_PROCESS_RUNNING_WITH_LD_PRELOAD);
      break;

    case SentryClient::SECURE:
      system_status.status(STATUS_SECURE);
      system_status.reason(REASON_SYSTEM_SECURE);
      break;

    case SentryClient::SEND_FAILED:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_SEND_MESSAGE_FAILED);
      break;

    default:
      system_status.status(STATUS_UNKNOWN);
      system_status.reason(REASON_UNKNOWN);
      break;
    }
  }

  enum StatusCode_t GetSystemStatus(void)
  {
    SystemStatus system_status;

    SentryClient client;
    SentryClient::StatusData status_data = client.GetSystemStatus(false);
    systemStatus(status_data.status, system_status);

    return system_status.status();
  }

  void GetSystemStatusVerbose(SystemStatus &system_status)
  {
    SentryClient client;
    SentryClient::StatusData status_data = client.GetSystemStatus(true);
    systemStatus(status_data.status, system_status);

    SystemStatus::ProcessInfoList_t processes;

    for (auto it = status_data.entries.begin(); it != status_data.entries.end(); it++)
    {
      std::vector<std::string> entry = *it;

      struct SystemStatus::ProcessInfo_t processInfo;

      processInfo.pid = std::stoi(entry[2]);
      processInfo.fullpathname = entry[1];
      processInfo.ld_preload_env = entry[0];

      processes.push_back(processInfo);
    }

    system_status.suspiciousProcesses(processes);
  }

}

#ifdef __cplusplus
extern "C" {
#endif
int wrapper_EnableScreenCaptureProtection()
{
    int ret;
    ret = sb_sdk::EnableScreenCaptureProtection();
    return ret;
}

int wrapper_ScreenCaptureProtectionEnabled()
{
    int ret;
    ret = sb_sdk::ScreenCaptureProtectionEnabled();
    return ret;
}

int wrapper_DisableScreenCaptureProtection()
{
    int ret;
    ret = sb_sdk::DisableScreenCaptureProtection();
    return ret;
}

int wrapper_RegisterWindowForAntiKeylogging(Display* display, Window wid) 
{
    int ret ;
    ret = sb_sdk::RegisterProcessForAntiKeylogging(display,wid);
    return ret;
}

int wrapper_DeregisterWindowForAntiKeylogging(Display* display, Window wid) 
{
    int ret ;
    ret = sb_sdk::DeregisterProcessForAntiKeylogging();
    return ret;
}

#ifdef __cplusplus
}
#endif

