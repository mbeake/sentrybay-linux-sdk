// SentryBay Image Lock Status Set & Read Header File - The Welder/Skid Row
// 18.01.2019

#ifndef _SENTRY_CLIENT_H_
#define _SENTRY_CLIENT_H_

#include <cstdint>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <string>
#include <sstream>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
//#include "trace.h"

#define SERVER_PATH "/usr/local/bin/sentrybay/.sentry_server"

class SentryClient
{
	public:
		enum MsgType : uint32_t
		{
			ILOCK_STATUS	= 0x80100001,
			ILOCK_RESPONSE	= 0x80100002,
			KEY_SCRAMBLED	= 0x80100010,
			KSCRAM_RESPONSE = 0x80100020,
			REG_PROCESS		= 0x80100040,
			UNREG_PROCESS	= 0x80100080,
			FOCUS_IN		= 0x80100100,
			FOCUS_OUT		= 0x80100200,
			ILOCK_SET		= 0x80100400,
			DUMP_SVC		= 0x80101000,
			DUMP_RESPONSE	= 0x80102000,
			SYSTEM_STATUS	= 0x80104000,
			SYSTEM_RESPONSE = 0x80108000
		};
		enum STATUS : uint32_t
		{
			COMPROMISED_NO_LIBRARY,
			COMPROMISED_NO_PRELOAD,
			COMPROMISED_NO_ENTRY,
			COMPROMISED_PROC_DENIED,
			TENTATIVE_NOT_ENTRY_ZERO,
			TENTATIVE_PRELOAD_DETECTED,
			SECURE,
			SEND_FAILED
		};

		typedef struct _StatusData
		{
			_StatusData() : count(0) {}
			STATUS status;
			uint32_t count;
			std::vector<std::vector<std::string>> entries;
		} StatusData;


		void SetILockState(bool state) { securesend(ILOCK_SET, state); }
		bool GetILockState(void)
		{
			msg.msgtype = ILOCK_STATUS;
			if (sendmsg(true))
				return static_cast<bool>(msg.msgtype == ILOCK_RESPONSE ? msg.status : false);
			return false;
		}
		bool IsKeyScrambled(const uint32_t pid)
		{
			msg.msgtype = KEY_SCRAMBLED;
			msg.status = pid;
			if (sendmsg(true))
				return static_cast<bool>(msg.msgtype == KSCRAM_RESPONSE ? msg.status : false);
			return false;
		}
		void RegisterProcess(const uint32_t pid) { securesend(REG_PROCESS, pid); }
		void UnRegisterProcess(const uint32_t pid) { securesend(UNREG_PROCESS, pid); }
		void FocusInWindow(const uint32_t pid, const uint64_t wid) { securesend(FOCUS_IN, pid, wid); }
		void FocusOutWindow(const uint32_t pid, const uint64_t wid) { securesend(FOCUS_OUT, pid, wid); }
		StatusData GetSystemStatus(bool verbose)
		{
			msg.msgtype = SYSTEM_STATUS;
			msg.status = static_cast<uint32_t>(verbose);
			std::unique_ptr<uint8_t[]> msg_data(new uint8_t[4096]());
			StatusData data;
			if (sendmsg(true, msg_data.get()) == false)
			{
				data.status = SEND_FAILED;
				return data;
			}
			_msg* msg_ptr = reinterpret_cast<_msg*>(msg_data.get());
			data.status = static_cast<STATUS>(msg_ptr->status);
			if (verbose)
			{
				if (data.status == TENTATIVE_NOT_ENTRY_ZERO || data.status == TENTATIVE_PRELOAD_DETECTED)
				{
					data.count = msg_ptr->base64;
					if (data.status == TENTATIVE_PRELOAD_DETECTED)
					{
						uint8_t* str_ptr = msg_data.get() + sizeof(_msg);

						auto get_string = [&]() -> std::string {
							std::stringstream ss;
							for (; *str_ptr; str_ptr++)
								ss << *str_ptr;
							str_ptr++;
							return ss.str();
						};

						for (size_t i = 0; i < msg_ptr->base64; i++)
						{
							std::vector<std::string> entry;
							entry.push_back(get_string());
							entry.push_back(get_string());
							entry.push_back(get_string());
							data.entries.push_back(entry);
						}
					}
				}
			}
			return data;
		}
	private:
		struct _msg
		{
			MsgType msgtype;
			uint32_t status;
			uint64_t base64;
		} msg;


		void securesend(MsgType type, const uint32_t state = 0, const uint64_t base = 0)
		{
			msg.msgtype = type, msg.status = state, msg.base64 = base;
			sendmsg();
		}

		bool sendmsg(bool response = false, void* verbose = nullptr)
		{
			ssize_t rc;
			struct sockaddr_un server_sockaddr;
			memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));
	
			int sock = socket(AF_UNIX, SOCK_STREAM, 0);
			if (sock == -1)
			{
				//tracef(TRACE_ERROR, "SVC", "sendmsg(): socket() returned -1: errno=%d\n", errno);
				return false;
			}

			server_sockaddr.sun_family = AF_UNIX;
			strcpy(server_sockaddr.sun_path, SERVER_PATH);
			rc = connect(sock, (struct sockaddr*)&server_sockaddr, sizeof(struct sockaddr_un));
			if (rc == -1)
			{
				//tracef(TRACE_ERROR, "SVC", "sendmsg(): connect() returned -1: errno=%d\n", errno);
		        close(sock);
				return false;
			}
			rc = send(sock, &msg, sizeof(msg), 0);
			if (rc == -1)
			{
				//tracef(TRACE_ERROR, "SVC", "sendmsg(): send() returned -1: errno=%d\n", errno);
				close(sock);
				return false;
			}
			else if (rc != sizeof(msg))
			{
				//tracef(TRACE_ERROR, "SVC", "sendmsg(): send() returned %d bytes expected %d\n", rc, sizeof(msg));
				close(sock);
				return false;
			}
			if (response)
			{
				void* buffer = verbose ? verbose : &msg;
				size_t msg_size = verbose ? 4096 : sizeof(msg);
				rc = recv(sock, buffer, msg_size, 0);
				if (rc == -1)
				{
					//tracef(TRACE_ERROR, "SVC", "sendmsg(): recv() returned -1: errno=%d\n", errno);
					close(sock);
					return false;
				}
				else if (rc < static_cast<ssize_t>(sizeof(msg)))
				{
					//tracef(TRACE_ERROR, "SVC", "sendmsg(): recv() returned %d bytes expected%d\n", rc, sizeof(msg));
					close(sock);
					return false;
				}
			}
			close(sock);
			return true;
		}
};

#endif
