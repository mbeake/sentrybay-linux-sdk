LD_PRELOAD=/usr/local/lib/sentrybay/libsentrybay.so shutter -e -f -n -o shutterscreenshotfull.png
LD_PRELOAD=/usr/local/lib/sentrybay/libsentrybay.so shutter -e --select=100,200,300,400 -n -o shutterscreenshotarea.png
LD_PRELOAD=/usr/local/lib/sentrybay/libsentrybay.so shutter -e --window=panel -n -o shutterscreenshotwindow.png
LD_PRELOAD=/usr/local/lib/sentrybay/libsentrybay.so shutter -e --web=http://bbc.co.uk -n -o shutterscreenshotweb.png
LD_PRELOAD=/usr/local/lib/sentrybay/libsentrybay.so shutter -e -a -n -o shutterscreenshotactive.png
LD_PRELOAD=/usr/local/lib/sentrybay/libsentrybay.so shutter -e --section -n -o shutterscreenshotsection.png
