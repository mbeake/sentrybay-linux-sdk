#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#define DEFAULT_TIMEOUT_MSECS 10
#define MAXIMUM_TIMEOUT_MSECS 250

int SocketTimeoutMSECS()
{
	try
	{
		boost::property_tree::ptree pt;

		boost::property_tree::ini_parser::read_ini("/usr/local/bin/sentrybay/.config/config.ini", pt);

		int socketTimeoutMSECS = pt.get<int>("Client.SocketTimeout_ms", DEFAULT_TIMEOUT_MSECS);

		if (socketTimeoutMSECS <= 0) socketTimeoutMSECS = DEFAULT_TIMEOUT_MSECS;
		else if (socketTimeoutMSECS > MAXIMUM_TIMEOUT_MSECS) socketTimeoutMSECS = MAXIMUM_TIMEOUT_MSECS;

		return socketTimeoutMSECS;
	}
	catch (...)
	{
		return DEFAULT_TIMEOUT_MSECS;
	}
}

int main()
{
	int socketTimeoutMSECS = SocketTimeoutMSECS();

	printf("Socket Timeout: %d ms\n", socketTimeoutMSECS);

    return 0;
}
