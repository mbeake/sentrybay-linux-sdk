/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <QX11Info>

#include <dlfcn.h>

#include "QtX11ImageConversion.h"
#include <SentryBaySDK.h>

#include "screenshot.h"

//! [0]
Screenshot::Screenshot()
    :  screenshotLabel(new QLabel(this)),
       serviceLabel(new QLabel("Service Status: unknown", this)),
       reasonLabel(new QLabel("Reason: unknown", this)),
       statusHistory(new QTextEdit(this)),
       servicePollTimer(new QTimer(this))
{
    screenshotLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    screenshotLabel->setAlignment(Qt::AlignCenter);

    const QRect screenGeometry = QApplication::desktop()->screenGeometry(this);
    screenshotLabel->setMinimumSize(screenGeometry.width() / 8, screenGeometry.height() / 8);

    serviceLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    reasonLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    statusHistory->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    statusHistory->setMinimumSize(screenGeometry.width() / 8, screenGeometry.height() / 8);
    statusHistory->setReadOnly(true);
    statusHistory->setStyleSheet("font: 9pt Courier");

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(screenshotLabel);
    mainLayout->addWidget(serviceLabel);
    mainLayout->addWidget(reasonLabel);
    mainLayout->addWidget(statusHistory);

    QGroupBox *optionsGroupBox = new QGroupBox(tr("Options"), this);
    delaySpinBox = new QSpinBox(optionsGroupBox);
    delaySpinBox->setSuffix(tr(" s"));
    delaySpinBox->setMaximum(60);

    connect(delaySpinBox, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &Screenshot::updateCheckBox);

    hideThisWindowCheckBox = new QCheckBox(tr("Hide This Window"), optionsGroupBox);

    screenShotDisabled = sb_sdk::ScreenCaptureProtectionEnabled();

    screenShotDisableCheckBox = new QCheckBox(tr("Disable Screenshot"), optionsGroupBox);
    screenShotDisableCheckBox->setChecked(screenShotDisabled);
    applicationProtectionCheckBox = new QCheckBox(tr("Enable Application Protection"), optionsGroupBox);
    applicationProtectionCheckBox->setChecked(false);

    QGridLayout *optionsGroupBoxLayout = new QGridLayout(optionsGroupBox);
    optionsGroupBoxLayout->addWidget(new QLabel(tr("Screenshot Delay:"), this), 0, 0);
    optionsGroupBoxLayout->addWidget(delaySpinBox, 0, 1);
    optionsGroupBoxLayout->addWidget(hideThisWindowCheckBox, 1, 0, 1, 2);
    optionsGroupBoxLayout->addWidget(screenShotDisableCheckBox, 2, 0, 1, 2);
    optionsGroupBoxLayout->addWidget(applicationProtectionCheckBox, 3, 0, 1, 2);

    mainLayout->addWidget(optionsGroupBox);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    newScreenshotButton = new QPushButton(tr("New Screenshot"), this);
    connect(newScreenshotButton, &QPushButton::clicked, this, &Screenshot::newScreenshot);
    buttonsLayout->addWidget(newScreenshotButton);
    QPushButton *saveScreenshotButton = new QPushButton(tr("Save Screenshot"), this);
    connect(saveScreenshotButton, &QPushButton::clicked, this, &Screenshot::saveScreenshot);
    buttonsLayout->addWidget(saveScreenshotButton);
    QPushButton *quitScreenshotButton = new QPushButton(tr("Quit"), this);
    quitScreenshotButton->setShortcut(Qt::CTRL + Qt::Key_Q);
    connect(quitScreenshotButton, &QPushButton::clicked, this, &QWidget::close);
    buttonsLayout->addWidget(quitScreenshotButton);
    buttonsLayout->addStretch();
    mainLayout->addLayout(buttonsLayout);

    connect(screenShotDisableCheckBox, &QCheckBox::stateChanged, this, &Screenshot::onScreenShotDisableCheckBoxStateChanged);
    connect(applicationProtectionCheckBox, &QCheckBox::stateChanged, this, &Screenshot::onApplicationProtectionCheckBoxStateChanged);

    connect(servicePollTimer, &QTimer::timeout, this, &Screenshot::updateServiceStatus);

    mainLayout->addWidget(new QTextEdit(this));
    mainLayout->addWidget(new QTextEdit(this));

    shootScreen();
    delaySpinBox->setValue(5);

    setWindowTitle(tr("Screenshot"));
    resize(300, 200);

    servicePollTimer->start(500);
}
//! [0]

//! [1]
void Screenshot::resizeEvent(QResizeEvent * /* event */)
{
    QSize scaledSize = originalPixmap.size();
    scaledSize.scale(screenshotLabel->size(), Qt::KeepAspectRatio);
    if (!screenshotLabel->pixmap() || scaledSize != screenshotLabel->pixmap()->size())
        updateScreenshotLabel();
}
//! [1]

//! [2]
void Screenshot::newScreenshot()
{
    if (hideThisWindowCheckBox->isChecked())
        hide();
    newScreenshotButton->setDisabled(true);

    QTimer::singleShot(delaySpinBox->value() * 1000, this, &Screenshot::shootScreen);
}
//! [2]

//! [3]
void Screenshot::saveScreenshot()
{
    const QString format = "png";
    QString initialPath = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    if (initialPath.isEmpty())
        initialPath = QDir::currentPath();
    initialPath += tr("/untitled.") + format;

    QFileDialog fileDialog(this, tr("Save As"), initialPath);
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setFileMode(QFileDialog::AnyFile);
    fileDialog.setDirectory(initialPath);
    QStringList mimeTypes;
    foreach (const QByteArray &bf, QImageWriter::supportedMimeTypes())
        mimeTypes.append(QLatin1String(bf));
    fileDialog.setMimeTypeFilters(mimeTypes);
    fileDialog.selectMimeTypeFilter("image/" + format);
    fileDialog.setDefaultSuffix(format);
    if (fileDialog.exec() != QDialog::Accepted)
        return;
    const QString fileName = fileDialog.selectedFiles().first();
    if (!originalPixmap.save(fileName)) {
        QMessageBox::warning(this, tr("Save Error"), tr("The image could not be saved to \"%1\".")
                             .arg(QDir::toNativeSeparators(fileName)));
    }
}
//! [3]

//! [4]
void Screenshot::shootScreen()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    if (const QWindow *window = windowHandle())
        screen = window->screen();
    if (!screen)
        return;

    if (delaySpinBox->value() != 0)
        QApplication::beep();

#if 1
// Native X11 code - start
    XWindowAttributes attributes;

    Display *display = XOpenDisplay(nullptr);
    Window root = DefaultRootWindow(display);

    /* Status status = */ XGetWindowAttributes(display, root, &attributes);

    XImage *ximage = XGetImage(display, root, 0, 0, static_cast<unsigned int>(attributes.width), static_cast<unsigned int>(attributes.height), AllPlanes, ZPixmap);
    QImage qimage = WebCore::qimageFromXImage(ximage);
    originalPixmap = QPixmap::fromImage(qimage);

    XDestroyImage(ximage);
    XCloseDisplay(display);
// Native X11 code - end
#else
    originalPixmap = screen->grabWindow(0);
#endif
    updateScreenshotLabel();

    newScreenshotButton->setDisabled(false);
    if (hideThisWindowCheckBox->isChecked())
        show();
}
//! [4]

//! [6]
void Screenshot::updateCheckBox()
{
    if (delaySpinBox->value() == 0) {
        hideThisWindowCheckBox->setDisabled(true);
        hideThisWindowCheckBox->setChecked(false);
    } else {
        hideThisWindowCheckBox->setDisabled(false);
    }
}
//! [6]


//! [10]
void Screenshot::updateScreenshotLabel()
{
    screenshotLabel->setPixmap(originalPixmap.scaled(screenshotLabel->size(),
                                                     Qt::KeepAspectRatio,
                                                     Qt::SmoothTransformation));
}
//! [10]

void Screenshot::updateServiceStatus()
{
    QString strStatus(tr("Unknown"));
    QString strReason(tr("Unknown"));

    sb_sdk::SystemStatus systemStatus;

    sb_sdk::StatusCode_t statusCode = sb_sdk::GetSystemStatus();

    if (statusCode == sb_sdk::STATUS_SECURE)
    {
        systemStatus.status(statusCode);
        systemStatus.reason(sb_sdk::REASON_SYSTEM_SECURE);
    }
    else
    {
        sb_sdk::GetSystemStatusVerbose(systemStatus);
    }

    switch(systemStatus.status())
    {
    case sb_sdk::STATUS_UNKNOWN:
        strStatus = QString(tr("Unknown"));
        break;

    case sb_sdk::STATUS_COMPROMISED:
        strStatus = QString(tr("COMPROMISED"));
        break;

    case sb_sdk::STATUS_TENTATIVE:
        strStatus = QString(tr("TENTATIVE"));
        break;

    case sb_sdk::STATUS_SECURE:
        strStatus = QString(tr("SECURE"));
        break;
    };

    switch(systemStatus.reason())
    {
    case sb_sdk::REASON_UNKNOWN:
        strReason = QString(tr("Unknown"));
        break;
    case sb_sdk::REASON_SYSTEM_SECURE:
        strReason = QString(tr("Secure"));
        break;
    case sb_sdk::REASON_SEND_MESSAGE_FAILED:
        strReason = QString(tr("Failed to send message to SentryBay service (service stopped?)"));
        break;
    case sb_sdk::REASON_SENTRYBAY_LIBRARY_MISSING:
        strReason = QString(tr("/usr/local/lib/sentrybay/libsentrybay.so file missing"));
        break;
    case sb_sdk::REASON_LD_SO_PRELOAD_MISSING:
        strReason = QString(tr("/etc/ld.so.preload file missing"));
        break;
    case sb_sdk::REASON_LD_SO_PRELOAD_NO_SENTRYBAY_ENTRY:
        strReason = QString(tr("No libsentrybay.so entry in /etc/ld.so.preload"));
        break;
    case sb_sdk::REASON_LD_SO_PRELOAD_SENTRYBAY_NOT_FIRST_ENTRY:
        strReason = QString(tr("libsentrybay.so is not first entry in /etc/ld.so.preload"));
        break;
    case sb_sdk::REASON_PROC_OPEN_FAILED:
        strReason = QString(tr("SentryBay service failed to open system /proc folder"));
        break;
    case sb_sdk::REASON_PROCESS_RUNNING_WITH_LD_PRELOAD:
        strReason = QString(tr("Processes using LD_PRELOAD"));
        break;
    case sb_sdk::REASON_SERVER_SOCKET_PATH:
        strReason = QString(tr("Misconfigured Server Socket Path"));
        break;
    }

    QString serviceStatusText = tr("Service Status: %1").arg(strStatus);
    QString reasonText = tr("Reason: %1").arg(strReason);

    serviceLabel->setText(serviceStatusText);
    reasonLabel->setText(reasonText);

    sb_sdk::SystemStatus::ProcessInfoList_t processes = systemStatus.suspiciousProcesses();

    if (statusHistory->toPlainText().isEmpty() ||
        systemStatus.status() != previousSystemStatus.status() ||
        systemStatus.reason() != previousSystemStatus.reason() ||
        processes.size() != previousSystemStatus.suspiciousProcesses().size())
    {
        QString statusHistoryEntry;

        statusHistoryEntry += serviceStatusText + "\n";
        statusHistoryEntry += reasonText + "\n\n";

        for (auto it = processes.begin(); it != processes.end(); it++)
        {
            sb_sdk::SystemStatus::ProcessInfo_t processInfo = *it;

            QString processInfoText;

            QTextStream(&processInfoText) <<
                "Process Id(pid): " << static_cast<unsigned>(processInfo.pid) << "\n" <<
                processInfo.fullpathname.c_str() << "\n" <<
                processInfo.ld_preload_env.c_str() << "\n\n";

            statusHistoryEntry += processInfoText;
        }

        statusHistory->append(statusHistoryEntry);

        previousSystemStatus = systemStatus;
    }

    screenShotDisabled = sb_sdk::ScreenCaptureProtectionEnabled();

    screenShotDisableCheckBox->setChecked(screenShotDisabled);
}

void Screenshot::onScreenShotDisableCheckBoxStateChanged()
{
    bool disabled = screenShotDisableCheckBox->isChecked();

    if (disabled != screenShotDisabled)
    {
        // CheckBox state must have been changed by the user. Go ahead and action it.

        screenShotDisabled = disabled;

        if (disabled)
        {
            qDebug() << QString("Enabling screen capture protection");
            sb_sdk::EnableScreenCaptureProtection();
        }
        else
        {
            qDebug() << QString("Disabling screen capture protection");
            sb_sdk::DisableScreenCaptureProtection();
        }
    }
}

void Screenshot::onApplicationProtectionCheckBoxStateChanged()
{
    bool applicationProtection = applicationProtectionCheckBox->isChecked();

    if (applicationProtection)
    {
        qDebug() << QString("Register application winId()=%1").arg(this->winId(), 0, 16);
        sb_sdk::RegisterProcessForAntiKeylogging(QX11Info::display(), this->winId());
    }
    else
    {
        qDebug() << QString("DeRegister application winId()=%1").arg(this->winId(), 0, 16);
        sb_sdk::DeregisterProcessForAntiKeylogging();
    }
}
