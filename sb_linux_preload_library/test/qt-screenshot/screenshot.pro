QT += widgets x11extras
requires(qtConfig(filedialog))

ARCH = x86_64-linux-gnu

HEADERS = screenshot.h \
          QtX11ImageConversion.h \

SOURCES = main.cpp \
          screenshot.cpp \
          QtX11ImageConversion.cpp \

SDK_PATH = ../../../sb_linux_sdk
TRACE_PATH = ../../../trace

INCLUDEPATH += "$${SDK_PATH}/include"
INCLUDEPATH += "$${TRACE_PATH}"

LIBS += -lX11 -L"$${SDK_PATH}/$${ARCH}/libs/" -L"$${TRACE_PATH}/$${ARCH}/libs/" -l:libsentrybaysdk.a -ltrace -static-libstdc++

QMAKE_POST_LINK += ./linuxdeployqt-6-x86_64.AppImage screenshot
