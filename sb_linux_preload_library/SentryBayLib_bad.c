#include <signal.h>

#ifdef __cplusplus
extern "C" {
#endif

void _init(void)
{
  raise(SIGSEGV);
}

#ifdef __cplusplus
}
#endif
