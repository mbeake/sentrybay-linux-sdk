cd ~
DOWNLOAD_PATH=http://downloads.raspberrypi.org/raspbian/images

# Earliest know Raspbian Jesie
#FOLDER=raspbian-2015-09-28
#FILENAME=2015-09-24-raspbian-jessie

# Version of Jessie used in azeria-labs.com qemu tutorial
FOLDER=raspbian-2017-04-10
FILENAME=2017-04-10-raspbian-jessie

wget ${DOWNLOAD_PATH}/${FOLDER}/${FILENAME}.zip
wget ${DOWNLOAD_PATH}/${FOLDER}/${FILENAME}.zip.sha1
sha1sum ${FILENAME}.zip
cat ${FILENAME}.zip.sha1
git clone https://github.com/dhruvvyas90/qemu-rpi-kernel
mkdir -p qemu_vms
cp qemu-rpi-kernel/kernel-qemu-4.4.34-jessie qemu_vms/
cd qemu_vms
unzip ../${FILENAME}.zip
fdisk -l ${FILENAME}.img
#sudo mkdir /mnt/raspbian
# offset needs to be the start offset reported by fdisk of the 2nd partition multiplied by 512
# When the raspbian file system is mounted we can get whatever files we need for cross-compilation from
# /mnt/raspbian/usr/arm-linux-gnueabihf/lib
#sudo mount -v -o offset=47185920 -t ext4 ~/qemu_vms/${FILENAME}.img /mnt/raspbian
#sudo umount /mnt/raspbian
sudo apt-get install qemu-system-arm
qemu-system-arm -kernel ~/qemu_vms/kernel-qemu-4.4.34-jessie -cpu arm1176 -m 256 -M versatilepb -serial stdio -append "root=/dev/sda2 rootfstype=ext4 rw" -hda ~/qemu_vms/${FILENAME}.img -no-reboot
