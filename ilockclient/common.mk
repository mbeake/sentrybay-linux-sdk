include ../global_common.mk

INCS := -I../trace -I../include
DEPLIBS := ../trace/$(LIBS)/libtrace.a
CXXFLAGS := -static -pthread -Wall -Werror -O3 -std=c++11 -fno-strict-aliasing -g1 -O3 -fthreadsafe-statics -D "NDEBUG" -frtti -fomit-frame-pointer -fexceptions $(INCS)

all: $(EXES)/ilockclient

$(EXES)/ilockclient: ilockclient/ilockclient.cpp $(DEPLIBS)
	@mkdir -p $(EXES)
	@$(CC) $(CXXFLAGS) -Wl,-z,relro -Wl,--strip-all -Wl,-z,noexecstack -Wl,--no-undefined -Wl,-z,now -o $@ $^

clean: 
	@rm -rf $(EXES)
