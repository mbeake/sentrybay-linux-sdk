//
//  ilockclient.cpp
//
//  Copyright © 2019 SentryBay. All rights reserved.
//
//  ilockclient Main Application Implementation File


#include <cstdint>
#include <cstdlib>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <limits>
#include <termios.h>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include "SentryClient.h"

#define COMMAND_FAILED "Command failed: Service not running ?"
#define INVALID_ARGUMENT "Command failed: Invalid argument"

void waitkey(void)
{
    struct termios t[2];
    tcgetattr(fileno(stdin), &t[0]);
    t[1] = t[0];
    t[1].c_lflag &= (~ICANON & ~ECHO);
    t[1].c_cc[VTIME] = 0;
    t[1].c_cc[VMIN] = 1;
    tcsetattr(fileno(stdin), TCSANOW, &t[1]);
    std::streambuf *buf = std::cin.rdbuf();
    buf->sgetc();
    buf->sbumpc();
    tcsetattr(fileno(stdin), TCSANOW, &t[0]);
}

void convert_str(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), [](uint8_t c) -> uint8_t {
        return static_cast<uint8_t>(std::toupper(c));
    });
}

void print_system_status(const sb_sdk_client::StatusData& data)
{
    std::cout << "System Status is: ";
    switch (data.status)
    {
        case sb_sdk_client::COMPROMISED_SERVER_SOCKET_PATH:
        {
            std::cout << "COMPROMISED_SERVER_SOCKET_PATH";
            break;
        }
        case sb_sdk_client::COMPROMISED_NO_LIBRARY:
        {
            std::cout << "COMPROMISED_NO_LIBRARY";
            break;
        }
        case sb_sdk_client::COMPROMISED_NO_PRELOAD:
        {
            std::cout << "COMPROMISED_NO_PRELOAD";
            break;
        }
        case sb_sdk_client::COMPROMISED_NO_ENTRY:
        {
            std::cout << "COMPROMISED_NO_ENTRY";
            break;
        }
        case sb_sdk_client::COMPROMISED_PROC_DENIED:
        {
            std::cout << "COMPROMISED_PROC_DENIED";
            break;
        }
        case sb_sdk_client::TENTATIVE_NOT_ENTRY_ZERO:
        {
            std::cout << "TENTATIVE_NOT_ENTRY_ZERO";
            break;
        }
        case sb_sdk_client::TENTATIVE_PRELOAD_DETECTED:
        {
            std::cout << "TENTATIVE_PRELOAD_DETECTED";
            break;
        }
        case sb_sdk_client::SECURE:
        {
            std::cout << "SECURE";
            break;
        }
        case sb_sdk_client::SEND_FAILED:
        {
            std::cout << "SEND_FAILED";
            break;
        }
    }
    std::cout << std::endl;
    if (data.status == sb_sdk_client::TENTATIVE_NOT_ENTRY_ZERO)
        std::cout << "Entry offset: " << data.count << " bytes" << std::endl;
    else if (data.status == sb_sdk_client::TENTATIVE_PRELOAD_DETECTED)
    {
        if (!data.entries.empty())
        {
            for (auto& entry : data.entries)
            {
                std::cout << "Preload: " << entry.at(0) << std::endl;
                std::cout << "Process Name: " << entry.at(1) << std::endl;
                std::cout << "Process Id: " << entry.at(2) << std::endl;
            }
        }
    }
}

int main(int argc, char** argv)
{
    sb_sdk_client::SentryClient client;

    while (true)
    {
        if (system("clear") == -1)
            exit(1);
        std::cout << "\e[0;1;4miLock Client Test Application v1.12" << std::endl
            << "\e[0;1;33m1\e[31m. \e[34mImage Lock State\e[31m" << std::endl
            << "\e[1;33m2\e[31m. \e[34mKey Scrambled \e[31m[\e[33mPID\e[31m]" << std::endl
            << "\e[1;33m3\e[31m. \e[34mRegister Process \e[31m[\e[33mPID\e[31m]" << std::endl
            << "\e[1;33m4\e[31m. \e[34mUnregister Process \e[31m[\e[33mPID\e[31m]" << std::endl
            << "\e[1;33m5\e[31m. \e[34mWindow Focus (DEV TESTING ONLY - DO NOT USE) \e[31m[\e[33mIN \e[31m| \e[33mOUT\e[31m] \e[31m[\e[33mPID\e[31m] [\e[33mWID\e[31m]" << std::endl
            << "\e[1;33m6\e[31m. \e[34mGet System State \e[31m<\e[33mVERBOSE\e[31m>" << std::endl
            << "\e[1;33m7\e[31m. \e[34mSet Image Lock \e[31m[\e[33mON \e[31m| \e[33mOFF\e[31m]" << std::endl
            << "\e[1;33m8\e[31m. \e[34mService Dump" << std::endl
            << "\e[1;33m9\e[31m. \e[34mList Registered Processes" << std::endl
            << "\e[1;33mQ\e[31m. \e[34mQuit Menu" << std::endl
            << "\e[1;32mEnter Command: \e[0m";

        std::string command;
        std::stringstream ss;

        if (argc <= 1)
            std::getline(std::cin, command);
        else
        {
            for (size_t i = 0; i < static_cast<size_t>(argc) - 1; i++)                // Turn command line args into a string of space separated arguments
            {
                if (i)
                    ss << ' ';
                ss << argv[i + 1];
            }
            command = ss.str();
            ss.str(std::string());
        }

        // Turn string of space separated arguments into a vector
        std::vector<std::string> args;
        for (uint8_t c : command)
        {
            if (c == ' ')
            {
                std::string arg = ss.str();
                if (!arg.empty())
                    args.push_back(arg);
                ss.str(std::string());
            }
            else
                ss << c;
        }
        command = ss.str();
        if (!command.empty())
            args.push_back(command);

        if (args.empty())
            continue;

        command = args.at(0);
        uint32_t pid;
        uint64_t wid;

        if (!command.empty() && command.size() < 2 && command.at(0) > '0' && command.at(0) <= '9')
        {
            int returnValue = 0;
            bool state = false;

            switch (command.at(0))
            {
                case '1':
                {
                    if (args.size() == 1)
                    {
                        returnValue = client.GetILockState(state);
                        if (returnValue == 0)
                            std::cout << "Image Lock Status is: " << (state ? "ON" : "OFF") << std::endl;
                        else
                            std::cout << COMMAND_FAILED << std::endl;
                        waitkey();
                    }
                    break;
                }
                case '2':
                {
                    if (args.size() == 2)
                    {
                        try
                        {
                            pid = std::stoi(args.at(1).c_str(), nullptr, 16);
                            returnValue = client.IsKeyScrambled(pid, state);
                            if (returnValue == 0)
                                std::cout << "Key is scrambled: " << (state ? "TRUE" : "FALSE") << std::endl;
                            else
                                std::cout << COMMAND_FAILED << std::endl;
                            waitkey();
                        }
                        catch (std::runtime_error& e) {}
                    }
                    break;
                }
                case '3':
                {
                    if (args.size() == 2)
                    {
                        try
                        {
                            pid = std::stoi(args.at(1).c_str(), nullptr, 10);
                            returnValue = client.RegisterProcess(pid);
                            if (returnValue == 1)
                            {
                                std::cout << COMMAND_FAILED << std::endl;
                                waitkey();
                            }
                        }
                        catch (std::runtime_error& e) {}
                    }
                    break;
                }
                case '4':
                {
                    if (args.size() == 2)
                    {
                        try
                        {
                            pid = std::stoi(args.at(1).c_str(), nullptr, 10);
                            returnValue = client.UnRegisterProcess(pid);
                            if (returnValue == 1)
                            {
                                std::cout << COMMAND_FAILED << std::endl;
                                waitkey();
                            }
                        }
                        catch (std::runtime_error& e) {}
                    }
                    break;
                }
                case '5':
                {
                    if (args.size() == 4)
                    {
                        std::string parm = args.at(1);
                        std::transform(parm.begin(), parm.end(), parm.begin(), [](uint8_t c) -> uint8_t {
                            return static_cast<uint8_t>(std::toupper(c));
                        });
                        try
                        {
                            pid = std::stoi(args.at(2).c_str(), nullptr, 16);
                            wid = std::strtoull(args.at(3).c_str(), nullptr, 16);
                            if (parm == "IN" || parm == "OUT")
                            {
                                if (parm == "IN")
                                    returnValue = client.FocusInWindow(pid, wid);
                                else if (parm == "OUT")
                                    returnValue = client.FocusOutWindow(pid, wid);

                                if (returnValue == 1)
                                {
                                    std::cout << COMMAND_FAILED << std::endl;
                                    waitkey();
                                }
                            }
                            else
                            {
                                std::cout << INVALID_ARGUMENT << std::endl;
                                waitkey();
                            }
                        }
                        catch (std::runtime_error& e) {}
                    }
                    break;
                }
                case '6':
                {
                    bool verbose = false;
                    if (args.size() == 2)
                    {
                        std::string str = args.at(1);
                        convert_str(str);
                        if (str == "VERBOSE")
                            verbose = true;
                    }
                    sb_sdk_client::StatusData data;
                    returnValue = client.GetSystemStatus(verbose, data);
                    if (returnValue == 1)
                        std::cout << COMMAND_FAILED << std::endl;
                    else
                          print_system_status(data);
                    waitkey();
                    break;
                }
                case '7':
                {
                    ss.str(std::string());
                    if (args.size() == 2)
                    {
                        for (uint8_t c : args.at(1))
                            ss << static_cast<uint8_t>(std::toupper(c));
                        returnValue = client.SetILockState(ss.str() == "ON" ? true : false);
                        if (returnValue == 1)
                        {
                            std::cout << COMMAND_FAILED << std::endl;
                            waitkey();
                        }
                    }
                    break;
                }
                case '8':
                {
                    sb_sdk_client::DumpData data;
                    returnValue = client.DumpService(data);
                    if (returnValue == 1)
                        std::cout << COMMAND_FAILED << std::endl;
                    else
                    {
                        std::cout << "Image Lock: " << (data.ilock_status ? "ON" : "OFF") << std::endl;
                        print_system_status(data.status_data);
                        std::cout << "Registered Processes: " << std::endl;
                        if (data.pids.empty())
                            std::cout << "\tNone" << std::endl;
                        else
                        {
                            for (auto iter = data.pids.begin(); iter != data.pids.end(); ++iter)
                                std::cout << "\t" << std::dec << *iter << std::endl;
                        }
                        std::cout << "InFocus PID: " << std::dec << data.focus_pid_wid.first << std::endl;
                        std::cout << "InFocus WID: " << std::hex << std::setw(16) << std::setfill('0') << data.focus_pid_wid.second << std::endl;
                    }
                    waitkey();
                    break;
                }
                case '9':
                {
                    std::set<uint32_t> pids;
                    returnValue = client.GetRegisteredProcesses(pids);
                    if (returnValue == 1)
                        std::cout << COMMAND_FAILED << std::endl;
                    else
                    {
                        std::cout << "Registered Processes: " << std::endl;
                        if (pids.empty())
                            std::cout << "\tNone" << std::endl;
                        else
                        {
                            for (auto iter = pids.begin(); iter != pids.end(); ++iter)
                                std::cout << "\t" << std::dec << *iter << std::endl;
                        }
                    }
                    waitkey();
                    break;
                }
            }
        }
        else if (!command.empty() && command.size() < 2 && std::toupper(command.at(0)) == 'Q')
            break;
        if (argc > 1)
            break;
    }
    return 0;
}
