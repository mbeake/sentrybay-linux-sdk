#!/bin/bash

# help
echo -e "\e[1mSentryBay Linux Bundle Builder v1.2\e[0m"
if [ "$1" = "" ]; then
    echo "Incorrect command line arguments, use --help for details"
    exit 1
elif [ "$1" = "--help" ]; then
    echo -e "Options: \n\t\e[1m./build_linux_bundle.sh [\e[0;32mVERSION\e[0m] [include_sdk] [configurable_socket_path]"
    echo "This script should be run from the linux_sdk folder."
    exit 0;
fi

# process command line options
VERSION=$1
shift 1
OPTIONS=$*

# common vars
ARCH=x86_64-linux-gnu
LIBS=${ARCH}/libs
EXES=${ARCH}/executables

# target dirs
linux_sdk_folder="$PWD"
bundle_folder="${linux_sdk_folder}/bundle"
chmod -R 775 "${bundle_folder}/" &> /dev/null
rm -rf "${bundle_folder}/"
mkdir -p "${bundle_folder}"

# Update the client header file CONFIGURABLE_SERVER_PATH value
sentry_client_header_file=${linux_sdk_folder}/include/SentryClient.h

if [[ $OPTIONS =~ "configurable_socket_path" ]]; then
    sed -i 's/^#define CONFIGURABLE_SERVER_PATH .$/#define CONFIGURABLE_SERVER_PATH 1/' ${sentry_client_header_file}
else
    sed -i 's/^#define CONFIGURABLE_SERVER_PATH .$/#define CONFIGURABLE_SERVER_PATH 0/' ${sentry_client_header_file}
fi

# trace lib
echo -e  -n "\e[1mBuilding trace library... \e[0m"
cd "${linux_sdk_folder}/trace"
make clean
make
if [[ $OPTIONS =~ "include_sdk" ]]; then
    mkdir -p "${bundle_folder}/trace"
    cp ${LIBS}/libtrace.a "${bundle_folder}/trace/"
    cp trace.h "${bundle_folder}/trace/"
fi
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"

# sb_ica_launcher
echo -e  -n "\e[1mBuilding sb_ica_launcher... \e[0m"
cd "${linux_sdk_folder}/sb_ica_launcher"
make clean
make
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"

# ilockclient
echo -e -n "\e[1mBuilding ilockclient... \e[0m"
cd "${linux_sdk_folder}/ilockclient"
make clean
make
if [[ $OPTIONS =~ "include_sdk" ]]; then
    mkdir -p "${bundle_folder}/ilockclient"
    cp ${EXES}/ilockclient "${bundle_folder}/ilockclient/"
fi
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"

# sdk
echo -e -n "\e[1mBuilding SDK... \e[0m"
cd "${linux_sdk_folder}/sb_linux_sdk"
make clean
make
if [[ $OPTIONS =~ "include_sdk" ]]; then
    mkdir -p "${bundle_folder}/SDK"
    cp ${LIBS}/libsentrybaysdk.a "${bundle_folder}/SDK/"
    cp ${LIBS}/libsentrybaysdk.so "${bundle_folder}/SDK/"
    cp include/SentryBaySDK.h "${bundle_folder}/SDK/"
    cp include/SystemStatus.h "${bundle_folder}/SDK/"
fi
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"

# qt-screenshot
if [[ $OPTIONS =~ "include_sdk" ]]; then
    echo -e -n "\e[1mAdding Qt screenshot... \e[0m"
    mkdir -p "${bundle_folder}/qt-screenshot"
    cp "${linux_sdk_folder}/sb_linux_preload_library/test/qt-screenshot/screenshot" "${bundle_folder}/qt-screenshot/"
    cp "${linux_sdk_folder}/sb_linux_preload_library/test/qt-screenshot/qt.conf" "${bundle_folder}/qt-screenshot/"
    cp -R "${linux_sdk_folder}/sb_linux_preload_library/test/qt-screenshot/lib/" "${bundle_folder}/qt-screenshot/"
    cp -R "${linux_sdk_folder}/sb_linux_preload_library/test/qt-screenshot/plugins/" "${bundle_folder}/qt-screenshot/"
    echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"
fi

# service
cd "${linux_sdk_folder}/sentryservice"
echo -e -n "\e[1mBuilding sentryservice... \e[0m"
make clean
make
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"


# preload lib
cd "${linux_sdk_folder}/sb_linux_preload_library"
echo -e -n "\e[1mBuilding Preload Library... \e[0m"
make clean
make
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"

# Installer/components into bundle_folder and fill
echo -e -n "\e[1mPopulating the Installer directory... \e[0m"
cd "${linux_sdk_folder}/Installer"
cp build.sh "${bundle_folder}/"
cp -r components "${bundle_folder}"
cd "${bundle_folder}"
cp "${linux_sdk_folder}/sb_linux_preload_library/${LIBS}/libsentrybay.so" components/sentryservice-install/usr/local/lib/sentrybay
cp "${linux_sdk_folder}/sentryservice/${EXES}/sentryservice" components/sentryservice-install/usr/local/bin/sentrybay
cp "${linux_sdk_folder}/sb_ica_launcher/${EXES}/sb_ica_launcher" components/sentryservice-install/usr/local/bin/sentrybay
mkdir -p components/sentryservice-install/usr/local/bin/sentrybay/.config
cp "${linux_sdk_folder}/sentryservice/config.ini" components/sentryservice-install/usr/local/bin/sentrybay/.config
mkdir -p components/sentryservice-install/etc/sentrybay
cp "${linux_sdk_folder}/sb_ica_launcher/sb_ica_launcher.ini" components/sentryservice-install/etc/sentrybay
mkdir -p components/sentryservice-install/usr/local/bin/sentrybay/.trace
cp "${linux_sdk_folder}/trace/trace.ini" components/sentryservice-install/usr/local/bin/sentrybay/.trace
if [[ $OPTIONS =~ "configurable_socket_path" ]]; then
    cp "${linux_sdk_folder}/sentryservice/.client.conf" components/sentryservice-install/usr/local/bin/sentrybay/
fi
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"
echo -e "\e[1mBuilding Installation Package...\e[0m"
./build.sh $VERSION
mv build Installer
rm -rf build.sh
chmod -R 775 components/ &> /dev/null
rm -rf components
echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"

# archiving
cd "${linux_sdk_folder}/"
echo -e -n "\e[1mCompiling linux_bundle_$VERSION.tar.gz bundle... \e[0m"
tar --owner=0 --group=0 -czvf linux_bundle_$VERSION.tar.gz -C "${bundle_folder}/" --exclude linux_bundle_$VERSION.tar.gz . &> /dev/null
rm -rf "${bundle_folder}/"
mv linux_bundle_$VERSION.tar.gz "${linux_sdk_folder}/Installer/build/"

echo -e "\e[1m[\e[32mDone\e[0;1m]\e[0m"
