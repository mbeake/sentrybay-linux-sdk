// SentryBay System Status Implementation - The Welder/Skid Row
// 02.05.2019

#include <cstdint>
#include <fstream>
#include <sstream>
#include <memory>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <dirent.h>
#include <string>
#include <unistd.h>

static constexpr const char* PROC_PATH = "/proc/";
static constexpr const char* PRE_NAME = "/etc/ld.so.preload";
static constexpr const char* LIB_NAME = "/usr/local/lib/sentrybay/libsentrybay.so";
static constexpr const char* LDPR_LOAD = "LD_PRELOAD";

enum STATUS : uint32_t
{
    COMPROMISED,
    TENTATIVE,
    SECURE
};

STATUS checkpreload(void)
{
    STATUS status = COMPROMISED;
    std::fstream fs;
    fs.open(LIB_NAME, std::ios::in | std::ios::binary);
    if (fs.is_open())
    {
        fs.close();
        fs.open(PRE_NAME, std::ios::in | std::ios::binary);
        if (fs.is_open())
        {
            std::stringstream ss;
            ss << fs.rdbuf();
            fs.close();
            size_t pos = ss.str().find(LIB_NAME);
            if (pos != std::string::npos)
            {
                if (pos)
                    std::cout << LIB_NAME << " not first entry in: " << PRE_NAME << std::endl;
                status = pos ? TENTATIVE : SECURE;
            }
            else
                std::cout << "Cannot find: " << LIB_NAME << " in file: " << PRE_NAME << std::endl;
        }
        else
            std::cout << "Unable to open: " << PRE_NAME << std::endl;
    }
    else
        std::cout << "Unable to open: " << LIB_NAME << std::endl;
    return status;
}

STATUS checkenvironments(void)
{
    STATUS status = SECURE;

    auto isproc = [](uint8_t* name) -> bool {
        do
        {
            if (!std::isdigit(*name++))
                return false;
        } while (*name);
        return true;
    };

    DIR* dir;
    if ((dir = opendir(PROC_PATH)))
    {
        dirent* entry;
        size_t lines = 0;
        std::cout << "Environments: [\e[1mPID\e[0m, \e[1mSIZE\e[0m]" << std::endl;
        while ((entry = readdir(dir)) != nullptr)
        {
            if (entry->d_type == DT_DIR && isproc(reinterpret_cast<uint8_t*>(entry->d_name)))
            {
                std::string fname = PROC_PATH + std::string(entry->d_name) + "/environ";
                std::fstream efile(fname.c_str(), std::ios::in | std::ios::binary);

                if (efile.is_open())
                {
                    std::string env;
                    std::getline(efile, env);
                    if (lines && !(lines % 4))
                        std::cout << std::endl;
                    else if (lines)
                        std::cout << "\t";
                    std::cout << "[" << "\e[1m" << std::setw(5) << std::setfill('0') << entry->d_name << "\e[0m, " <<
                        (env.size() > 0 ? "\e[35m" : "\e[34m") << std::setw(4) << std::setfill('0') << env.size() << "\e[0m" << "]";
                    if (env.size())
                    {
                        std::transform(env.begin(), env.end(), env.begin(), [](uint8_t c) -> uint8_t {
                            return static_cast<uint8_t>(std::toupper(c));
                        });

                        if (env.find(LDPR_LOAD) != std::string::npos)
                        {
                            std::cout << std::endl << "LD_PRELOAD found in PID: " << entry->d_name;
                            status = COMPROMISED;
                        }
                    }
                    efile.close();
                    lines++;
                }
            }
            if (status == COMPROMISED)
                break;
        }
        closedir(dir);
        std::cout << std::endl;
    }
    else
    {
        std::cout << "Unable to open directory: " << PROC_PATH << std::endl;
        status = COMPROMISED;
    }
    return status;
}

STATUS processenvproc(void)
{
    STATUS status = checkpreload();
    if (status == SECURE)
        status = checkenvironments();
    return status;
}

int main(int argc, char** argv)
{
    std::cout << "\e[0;1mSentryService SystemState Utility v1.0\e[0m" << std::endl;
    if (!geteuid())
    {
        STATUS status = checkpreload();
        std::cout << "Preload checks complete, system is: ";
        if (status == SECURE)
            std::cout << "\e[32mSECURE";
        else if (status == TENTATIVE)
            std::cout << "\e[36mTENTATIVE";
        else
            std::cout << "\e[31mCOMPROMISED";
        std::cout << "\e[0m" << std::endl;

        if (status == SECURE)
        {
            status = checkenvironments();
            std::cout << "Environment checks complete, system is: " << (status == SECURE ? "\e[32mSECURE" : "\e[31mCOMPROMISED") << "\e[0m" <<std::endl;
        }
    }
    else
        std::cout << "\e[31mThis utility can only be run by root\e[0m" << std::endl;
    return 0;
}
