#!/usr/bin
import os
import time
import platform
import datetime
import subprocess


print("*******************************************************************")
print ("Automated testing of Different methods of screencapture in LINUX :")
currentDT = datetime.datetime.now()
print ("Test Executed at \'%d:%d:%d\' on \'%d-%d-%d\' over the platform:" %(currentDT.hour,currentDT.minute,currentDT.second,currentDT.day,currentDT.month,currentDT.year))
print (platform.linux_distribution())

print("*******************************************************************")
print(" ")
print("Initial stability Tests")
print("========================")
print (" ")

if (os.path.exists('/usr/local/bin/sentrybay/sentryservice')):
 print ("1.Linux SDK installed.. Starting automated test")
else:
 print ("Linux SDK NOT installed.. Quiting Automated Test")
 quit()

SDK_version = subprocess.check_output("ls -l /usr/local/lib/sentrybay/ | grep -w libsentrybay.so. | awk -F/ '{print $NF}'",shell=True)
print (" ")
print "2.SDK version or library version installed :{}".format(SDK_version)

if (os.path.exists('/etc/ld.so.preload')):
 print ("3.ld.so.preload file is present in /etc folder.")
else:
 print ("3.ld.so.preload file is NOT present in /etc folder... Quiting Automated Test")
 quit()

service_status = subprocess.check_output("systemctl list-unit-files --type=service | grep sentryservice-install.service | awk '{print$NF}'",shell=True)
service_running = subprocess.check_output("systemctl list-units --type service -all | grep sentryservice-install.service | awk '{print$3}'",shell=True)

print (" ")

print ("4.SentryService status enabled or Disabled : {}".format(service_status))

print ("5.SentryService Active or Inactive : {}".format(service_running))


if (service_running != "active\n"):
 print ("SentryService is not active in this machine,Quiting Automation testing")
 quit()

owner_etc = subprocess.check_output("ls -l / | grep etc | awk '{print $3,$4}'",shell=True)
owner_var = subprocess.check_output("ls -l / | grep var | awk '{print $3,$4}'",shell=True)
print ("6.Ownership of system folder /etc : {}".format(owner_etc))
print ("7.Ownership of system folder /var : {}".format(owner_var))


print("*******************************************************************")
print("Anti-screencapture tests")
print("*******************************************************************")
rv = os.system("chmod 775 ilockclient")

if(rv != 0):
 print "Permission denied to execute ilockclient"
 print "Please provide permision 775 lto ilockclient in the current directory"
 exit()

def Compare(Im1,Im2,method):
 
 result = os.system("cmp "+Im1+" "+Im2+" > out")
 if(result != 0):
  print "Screen captured After protection through \"%s\" method is protected"%method
  print "Test PASSED"
 else:
  print "Screen captured After protection through \"%s\" method is NOT protected"%method
  print "Test FAILED"
 



def Image_magic():
 os.system("rm -rf IM_protection*.png")
 print"Testcase 1 : Protection Against \"Image_magic\" screen capture method"
 print" "

 os.system("./ilockclient 7 OFF > out")
 
 os.system("import -window root IM_protection_OFF.png")

 if(os.path.isfile('IM_protection_OFF.png')):
  print"Protection DISABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured before protection,aborting test..."
  return
 
 os.system("./ilockclient 7 ON > out")
 
 os.system("import -window root IM_protection_ON.png")

 if(os.path.isfile('IM_protection_ON.png')):
  print"Protection ENABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured After protection,aborting test..."
  return

 Compare("IM_protection_OFF.png","IM_protection_ON.png","Image_magic")
 

def Scrot():
 os.system("rm -rf scrotImage*.png")
 print"Testcase 2 : Protection Against \"Scrot\" screen capture method"
 print" "

 os.system("./ilockclient 7 OFF > out")
 os.system("scrot scrotImage_POFF.png")

 if(os.path.isfile('scrotImage_POFF.png')):
  print"Protection DISABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured before protection,aborting test..."
  return

 os.system("./ilockclient 7 ON > out")
 os.system("scrot scrotImage_PON.png")

 if(os.path.isfile('scrotImage_PON.png')):
  print"Protection ENABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured After protection,aborting test..."
  return

 Compare("scrotImage_POFF.png","scrotImage_PON.png","Scrot")

def GNOME():
 os.system("rm -rf gnomeImage*.png")
 print"Testcase 3 : Protection Against \"GNOME\" screen capture method"
 print" "

 os.system("./ilockclient 7 OFF > out")
 os.system("gnome-screenshot -f gnomeImage_POFF.png >out 2>&1")

 if(os.path.isfile('gnomeImage_POFF.png')):
  print"Protection DISABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured before protection,aborting test..."
  return

 os.system("./ilockclient 7 ON > out")
 os.system("gnome-screenshot -f gnomeImage_PON.png >out 2>&1")

 if(os.path.isfile('gnomeImage_PON.png')):
  print"Protection ENABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured After protection,aborting test..."
  return

 Compare("gnomeImage_POFF.png","gnomeImage_PON.png","gnome")


def shutter():
 os.system("rm -rf ShutterImage*.png")
 print"Testcase 4 : Protection Against \"Shutter\" screen capture method"
 print" "

 os.system("./ilockclient 7 OFF > out")
 os.system("shutter -e -f -n -o ShutterImage_POFF.png >out 2>&1")
 
 if(os.path.isfile('ShutterImage_POFF.png')):
  print"Protection DISABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured before protection,aborting test..."
  return

 os.system("./ilockclient 7 ON > out")
 os.system("shutter -e -f -n -o ShutterImage_PON.png >out 2>&1")
 
 if(os.path.isfile('ShutterImage_PON.png')):
  print"Protection ENABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured After protection,aborting test..."
  return
 
 Compare("ShutterImage_POFF.png","ShutterImage_PON.png","Shutter")

def Graphics_magic():
 os.system("rm -rf graphicsmagic*.png")
 print"Testcase 5 : Protection Against \"Graphics_magic\" screen capture method"
 print" "

 os.system("./ilockclient 7 OFF > out")
 os.system("gm import -window root graphicsmagic_POFF.png")
 
 if(os.path.isfile('graphicsmagic_POFF.png')):
  print"Protection DISABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured before protection,aborting test..."
  return

 os.system("./ilockclient 7 ON > out")
 os.system("gm import -window root graphicsmagic_PON.png")
 
 if(os.path.isfile('graphicsmagic_PON.png')):
  print"Protection ENABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured After protection,aborting test..."
  return
 
 Compare("graphicsmagic_POFF.png","graphicsmagic_PON.png","Graphics_magic")

def flameshot():
 cwd = os.getcwd()
 os.system("rm -rf screen*.png")

 print "Test case 6 : Protection Against \"Flameshot\" screen capture method"
 print" "

 os.system("./ilockclient 7 OFF > out")
 os.system("flameshot full -p "+cwd)
 time.sleep(1)

 if(os.path.isfile('screenshot.png')):
  print"Protection DISABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured before protection,aborting test..."
  return

 os.system("./ilockclient 7 ON > out")
 os.system("flameshot full -p "+cwd)
 time.sleep(1)

 if(os.path.isfile('screenshot_1.png')):
  print"Protection ENABLED : Screen capture attempted"
  print" "
 else:
  print"Screen NOT captured After protection,aborting test..."
  return

 Compare("screenshot.png","screenshot_1.png","Flameshot")
 
Image_magic()
print"======================================================================= "
Scrot()
print"======================================================================= "
GNOME()
print"======================================================================= "
shutter()
print"======================================================================= "
Graphics_magic()
print"======================================================================= "
flameshot()
os.system("./ilockclient 7 ON > out")





