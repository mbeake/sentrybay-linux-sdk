include ../global_common.mk

INCS := -I../trace -I../include
DEPLIBS := ../trace/$(LIBS)/libtrace.a
CXXFLAGS := -static -pthread -Wall -fPIC -DPIC -Werror -O3 -std=c++11 -D "NDEBUG" -frtti -fomit-frame-pointer -fexceptions $(INCS)

all: $(EXES)/sentryservice

$(EXES)/sentryservice: sentryservice/sentryservice.cpp $(DEPLIBS)
	@mkdir -p $(EXES)
	@$(CC) $(CXXFLAGS) -o $@ $^ 

clean: 
	@rm -rf $(OBJS) $(EXES)
