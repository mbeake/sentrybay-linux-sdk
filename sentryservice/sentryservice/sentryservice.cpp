//
//  sentryservice.cpp
//
//  Copyright © 2019 SentryBay. All rights reserved.
//
//  SentryServer Main Application Implementation File

#include <cstdint>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <sstream>
#include <memory>
#include <set>
#include <fstream>
#include <random>
#include <stdlib.h>
#include <iomanip>
#include <unistd.h>
#include <errno.h>
#include <string.h> // for strerror()
#include <dirent.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <signal.h> // for kill()
#include <sys/time.h>
#include <time.h>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <chrono>
#include <thread>
#include <mutex>
#include <functional>
#include <boost/algorithm/string.hpp>
#include "trace.h"
#include "SentryClient.h"
#include "utils.h"

#define BIN_FOLDER "/usr/local/bin/sentrybay/"

static constexpr const char *SERVICE_CONFIG_FILE_PATH = BIN_FOLDER ".sentryservice.conf";
static constexpr const char *CONFIG_INI_PATH = BIN_FOLDER ".config/config.ini";

static std::string server_socket_path = sb_sdk_client::SERVER_PATH;

static constexpr const char *PROC_PATH = "/proc/";
static constexpr const char *PRE_NAME = "/etc/ld.so.preload";
static constexpr const char *LIB_NAME = sb_sdk_client::LIB_NAME;
static constexpr const char *LDPR_LOAD = "LD_PRELOAD";

static constexpr const char *GNOME_1 = "/etc/dbus-1/session.d/org.gnome.Shell.Screencast.conf";
static constexpr const char *GNOME_2 = "/etc/dbus-1/session.d/org.gnome.Shell.Screenshot.conf";
static constexpr const char *GNOME_SCREENCAST = "<!DOCTYPE busconfig PUBLIC\n"
    " \"-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN\"\n"
    " \"http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd\">\n"
    "<busconfig>\n"
    "  <policy context=\"default\">\n"
    "    <deny send_destination=\"org.gnome.Shell.Screencast\" send_interface=\"org.gnome.Shell.Screencast\"/>\n"
    "  </policy>\n"
    "</busconfig>\n";
static constexpr const char *GNOME_SCREENSHOT = "<!DOCTYPE busconfig PUBLIC\n"
    " \"-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN\"\n"
    " \"http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd\">\n"
    "<busconfig>\n"
    "  <policy context=\"default\">\n"
    "    <deny send_destination=\"org.gnome.Shell.Screenshot\" send_interface=\"org.gnome.Shell.Screenshot\"/>\n"
    "  </policy>\n"
    "</busconfig>\n";

static constexpr const char *KDE_2 = "/etc/dbus-1/session.d/org.kde.kwin.Screenshot.conf";
static constexpr const char *KDE_SCREENSHOT = "<!DOCTYPE busconfig PUBLIC\n"
    " \"-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN\"\n"
    " \"http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd\">\n"
    "<busconfig>\n"
    "  <policy context=\"default\">\n"
    "    <deny send_destination=\"org.kde.KWin\" send_interface=\"org.kde.kwin.Screenshot\"/>\n"
    "  </policy>\n"
    "</busconfig>\n";

struct SYSTEM_STATUS_DATA
{
    sb_sdk_client::STATUS status = sb_sdk_client::SECURE;
    unsigned entry_offset = 0; // Offset of the SentryBay preload library within /etc/ld.so.preload
    unsigned entry_count = 0; // Number of suspicious processes packed into the tentative_data field.
    std::vector<uint8_t> tentative_data;
};

SYSTEM_STATUS_DATA cached_system_status_data;

pthread_mutex_t cached_system_status_data_mutex = PTHREAD_MUTEX_INITIALIZER;

SYSTEM_STATUS_DATA get_cached_system_status_data()
{
    SYSTEM_STATUS_DATA system_status_data;

    pthread_mutex_lock(&cached_system_status_data_mutex);

    system_status_data = cached_system_status_data;

    pthread_mutex_unlock(&cached_system_status_data_mutex);

    return system_status_data;
}

void set_cached_system_status_data(const SYSTEM_STATUS_DATA &system_status_data)
{
    pthread_mutex_lock(&cached_system_status_data_mutex);

    cached_system_status_data = system_status_data;

    pthread_mutex_unlock(&cached_system_status_data_mutex);
}

sb_sdk_client::STATUS processenvproc(bool verbose, SYSTEM_STATUS_DATA &system_status_data);

static const int CACHED_STATUS_REFRESH_MICROSECONDS = 1000000; // 1 second

void *system_status_worker(void *)
{
    while (true)
    {
        SYSTEM_STATUS_DATA system_status_data;

        tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "system_status_worker(): calling processenvproc()...");
        processenvproc(true, system_status_data);
        tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "system_status_worker(): processenvproc() done.");

        set_cached_system_status_data(system_status_data);

        usleep(CACHED_STATUS_REFRESH_MICROSECONDS);
    }

    return NULL;
}

static bool ilock_state = false;
std::mutex process_tracking_mutex;
static std::set<uint32_t> protected_processes;
static std::pair<uint32_t, uint64_t> focus_pid_wid;
// Map of pids, with a boolean to track whether we think they're in-focus.
// Essentially if the last focus message we saw was a focus-in we assume they're in-focus.
// This only exists as a sanity check when switching between a protected process and
// an unprotected process, so we don't worry about the corner cases when switching between windows
// in the same process for focus_pid_wid.
static std::unordered_map<uint32_t, bool> process_map;

// Window to pid map
static std::unordered_map<uint64_t, uint32_t> window_map;

static std::vector<uint8_t> system_status_response;

void dbuscontrol(const uint32_t ctrl);
pthread_mutex_t ilock_process_tracking_mutex = PTHREAD_MUTEX_INITIALIZER;
static std::set<pid_t> ilock_protected_processes;
static const int ILOCK_PROTECTED_PROCESS_REFRESH_MICROSECONDS = 1000000; // 1 second

void *ilock_process_tracking_removal_worker(void *)
{
    while (true)
    {
        pthread_mutex_lock(&ilock_process_tracking_mutex);
        for (std::set<pid_t>::iterator iter = ilock_protected_processes.begin(); iter != ilock_protected_processes.end(); ) {
            if (kill(*iter, 0) == -1 && errno == ESRCH) {
                // process doesnt exist
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "ilock_process_tracking_removal_worker(): removing %d.",*iter);
                iter = ilock_protected_processes.erase(iter);
            } else {
                ++iter;
            }
        }
        if (ilock_state && ilock_protected_processes.empty()) {
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "ilock_process_tracking_removal_worker() switches ilock_state off");
            ilock_state = false;
            dbuscontrol(false);
        }
        pthread_mutex_unlock(&ilock_process_tracking_mutex);

        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "ilock_process_tracking_removal_worker(): ongoing.");

        usleep(ILOCK_PROTECTED_PROCESS_REFRESH_MICROSECONDS);
    }

    return NULL;
}

static const int PROTECTED_PROCESS_REFRESH_HOURS = 12; // 12 hour

void *process_tracking_removal_worker(void *) {
    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "process_tracking_removal_worker start");
    while (true) {
        process_tracking_mutex.lock();
        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "process_tracking_removal_worker(): checking.");
        for (auto iter = protected_processes.begin(); iter != protected_processes.end(); ) {
            if (kill(*iter, 0) == -1 && errno == ESRCH) {
                // process doesnt exist
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "key - process_tracking_removal_worker(): removing %d.",*iter);
                if (focus_pid_wid.first == *iter) {
                    focus_pid_wid.first = 0;
                    focus_pid_wid.second = 0;
                }
                iter = protected_processes.erase(iter);
            } else {
                ++iter;
            }
        }
        process_tracking_mutex.unlock();
        auto x = std::chrono::steady_clock::now() + std::chrono::hours(PROTECTED_PROCESS_REFRESH_HOURS);
        std::this_thread::sleep_until(x);
        tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "process_tracking_removal_worker(): ongoing.");
    }
    return NULL;
}

// Metrics for investigating system performance/latency issues

typedef struct
{
    struct timeval timestamp;
    unsigned long intervalToLastMessage_usecs;
} MessageMetric;

typedef std::map<sb_sdk_client::MsgType, MessageMetric> MessageMetrics;
typedef std::map<pid_t, MessageMetrics> ProcMessageMetrics;

static ProcMessageMetrics processMessageMetricsMap;

const unsigned DEFAULT_THRESHOLD_INTERVAL_MS = 100;

static bool metricsEnabled = false;

// Threshold Interval - We are interested in messages that are separated by an interval that might suggest
// there is a system latency developing. Typically a long interval is due simply because there is a genuine
// separation of messages - for example gaps in user keyboard input. But if we were to see larger than expected
// gaps between messages we would want to know. Threshold Interval is simply the threshold interval value for
// reporting potential latency.
static unsigned thresholdInterval_usecs = DEFAULT_THRESHOLD_INTERVAL_MS * 1000;

const static unsigned MICROSECONDS_PER_SECOND = 1000000;

static unsigned long interval_usecs(struct timeval &prev, struct timeval &next)
{
    struct timeval interval;

    interval.tv_sec = next.tv_sec - prev.tv_sec;
    if (next.tv_usec < prev.tv_usec)
    {
        interval.tv_sec--;
        interval.tv_usec = MICROSECONDS_PER_SECOND + next.tv_usec - prev.tv_usec;
    }
    else
    {
        interval.tv_usec = next.tv_usec - prev.tv_usec;
    }

    return interval.tv_sec * MICROSECONDS_PER_SECOND + interval.tv_usec;
}

static void metrics_log(sb_sdk_client::MsgType msgtype, pid_t pid, struct timeval timestamp)
{
    if (!metricsEnabled) return;

//    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_METRICS, "metrics_log(): msgtype=%s pid=%d timestamp.tv_sec=%lu timestamp.tv_usec=%lu",
//        sb_sdk_client::SentryClient::MessageType(msgtype), pid, timestamp.tv_sec, timestamp.tv_usec);

    // The following operation creates an entry for pid if one doesn't already exist
    MessageMetrics &MessageMetrics = processMessageMetricsMap[pid];

    bool found_a_previous_timestamp = (MessageMetrics.find(msgtype) != MessageMetrics.end());

    // The following operation creates an entry for msgtype if one doesn't already exist
    MessageMetric &message_metric = MessageMetrics[msgtype];

    if (found_a_previous_timestamp)
    {
        unsigned long prev_message_interval_usecs = message_metric.intervalToLastMessage_usecs;

        message_metric.intervalToLastMessage_usecs = interval_usecs(message_metric.timestamp, timestamp);

        if (prev_message_interval_usecs > thresholdInterval_usecs && message_metric.intervalToLastMessage_usecs > thresholdInterval_usecs)
        {
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_METRICS, "metrics_log(): msgtype=%s pid=%d interval(usecs)=%lu",
                sb_sdk_client::SentryClient::MessageType(msgtype), pid, message_metric.intervalToLastMessage_usecs);
        }
    }
    else
    {
        message_metric.intervalToLastMessage_usecs = 0;
    }

    message_metric.timestamp = timestamp;
}

static void ConfigureMetrics()
{
    try
    {
        boost::property_tree::ptree pt;

        boost::property_tree::ini_parser::read_ini(CONFIG_INI_PATH, pt);

        metricsEnabled = pt.get<bool>("ServiceMetrics.Enabled", false);

        thresholdInterval_usecs = pt.get<int>("ServiceMetrics.ThresholdInterval_ms", DEFAULT_THRESHOLD_INTERVAL_MS) * 1000;
        if (thresholdInterval_usecs <= 0) thresholdInterval_usecs = DEFAULT_THRESHOLD_INTERVAL_MS * 1000;
    }
    catch (...)
    {
        // Nothing to do
    }
}

class Serializer
{
    public:
        explicit Serializer(bool alignment = false) {}
        template<typename T>
        void Serialize(const T& type)
        {
            std::copy(reinterpret_cast<uint8_t*>(const_cast<T*>(&type)),
                reinterpret_cast<uint8_t*>(const_cast<T*>(&type)) + sizeof(T), std::back_inserter(sdata));
        }
        const uint64_t GetU64Value()
        {
            uint64_t* p = static_cast<uint64_t*>(const_cast<void*>(ptr));
            uint64_t v = *p++;
            ptr = p;
            return v;
        }
        const uint32_t GetU32Value()
        {
            uint32_t* p = static_cast<uint32_t*>(const_cast<void*>(ptr));
            uint32_t v = *p++;
            ptr = p;
            return v;
        }
        const uint16_t GetU16Value()
        {
            uint16_t* p = static_cast<uint16_t*>(const_cast<void*>(ptr));
            uint16_t v = *p++;
            ptr = p;
            return v;
        }
        const uint8_t GetU8Value()
        {
            uint8_t* p = static_cast<uint8_t*>(const_cast<void*>(ptr));
            uint8_t v = *p++;
            ptr = p;
            return v;
        }
        template<typename T>
        size_t GetPlaceholder(void)
        {
            T t{};
            size_t pos = sdata.size();
            Serialize(t);
            return pos;
        }
        template<typename T>
        void InsertSerialize(size_t pos, const T& type)
        {
            std::copy(reinterpret_cast<uint8_t*>(const_cast<T*>(&type)),
                reinterpret_cast<uint8_t*>(const_cast<T*>(&type)) + sizeof(T), const_cast<uint8_t*>(sdata.data() + pos));
        }
        const void* GetSerializer() const { return ptr; }
        void SetSerializer(void* data) { ptr = data; }
        const uint8_t* GetSerializedData(void) const { return sdata.data(); }
        const size_t GetSerializedDataSize(void) const { return sdata.size(); }
    private:
        std::vector<uint8_t> sdata;
        void* ptr;
};

sb_sdk_client::STATUS checkpreload(bool verbose, SYSTEM_STATUS_DATA &system_status_data)
{
    sb_sdk_client::STATUS &status = system_status_data.status;
    std::fstream fs;
    fs.open(LIB_NAME, std::ios::in | std::ios::binary);
    if (fs.is_open())
    {
        fs.close();
        fs.open(PRE_NAME, std::ios::in | std::ios::binary);
        if (fs.is_open())
        {
            ////
            // Cycle through the entries in PRE_NAME file and compare the inode of each entry with the inode of LIB_NAME
            ////
            std::stringstream ss;
            ss << fs.rdbuf();
            fs.close();
            std::string lib_name_in_preload = ss.str(); 
            boost::algorithm::trim(lib_name_in_preload);
            status = sb_sdk_client::COMPROMISED_NO_ENTRY;
            if (!lib_name_in_preload.empty()) // quick get out if there are no entries in PRE_NAME file
            {
                struct stat lib_stat = {0};
                stat(LIB_NAME, &lib_stat);
                // Get the entries from PRE_NAME file into a vector
                std::vector<std::string> preload_libs (std::istream_iterator<std::string>{ss}, std::istream_iterator<std::string>());
                bool first_entry = true;
                // Cycle through the vector to find the entry with the same inode as LIB_NAME and break out of loop if found
                for(const std::string &preload_lib : preload_libs)
                {
                    struct stat lib_in_preload_stat = {0}; 
                    int lib_name_in_preload_rc = stat(preload_lib.c_str(), &lib_in_preload_stat);
#ifdef DEBUG
                    tracef(TRACE_ALWAYS, SUBSYSTEM_NAMES::SVC, 
                           "LIB_NAME == %s, lib_name_in_preload == %s, first_entry == %s, preload_lib == %s ", 
                           LIB_NAME, lib_name_in_preload.c_str(), first_entry ? "true" : "false", preload_lib.c_str());
                    tracef(TRACE_ALWAYS, SUBSYSTEM_NAMES::SVC, 
                           "lib_name_in_preload_rc == %i, lib_stat.st_ino == %d, lib_in_preload_stat.st_ino == %d, lib_stat.st_ino == lib_in_preload_stat.st_ino == %s",
                           lib_name_in_preload_rc, lib_stat.st_ino, lib_in_preload_stat.st_ino, lib_stat.st_ino == lib_in_preload_stat.st_ino ? "true" : "false");
#endif
                    if ((lib_name_in_preload_rc == 0) && (lib_stat.st_ino == lib_in_preload_stat.st_ino))
                    {
                        status = first_entry ? sb_sdk_client::SECURE : sb_sdk_client::TENTATIVE_NOT_ENTRY_ZERO;
                        break;
                    }
                    first_entry = false;
                }
            }
        }
        else
            status = sb_sdk_client::COMPROMISED_NO_PRELOAD;
    }
    else
        status = sb_sdk_client::COMPROMISED_NO_LIBRARY;
    return status;
}

sb_sdk_client::STATUS checkenvironments(bool verbose, SYSTEM_STATUS_DATA &system_status_data)
{
    sb_sdk_client::STATUS &status = system_status_data.status;

    status = sb_sdk_client::SECURE;

    auto isproc = [](uint8_t* name) -> bool {
        do
        {
            if (!std::isdigit(*name++))
                return false;
        } while (*name);
        return true;
    };

    DIR* dir;
    if ((dir = opendir(PROC_PATH)))
    {
        if (verbose)
            system_status_data.entry_count = 0;

        dirent* entry;
        while ((entry = readdir(dir)) != nullptr)
        {
            if (entry->d_type == DT_DIR && isproc(reinterpret_cast<uint8_t*>(entry->d_name)))
            {
                std::string fname = PROC_PATH + std::string(entry->d_name) + "/environ";
                std::string pname = PROC_PATH + std::string(entry->d_name) + "/cmdline";
                std::fstream efile(fname.c_str(), std::ios::in | std::ios::binary);

                if (efile.is_open())
                {
                    std::string env;
                    std::getline(efile, env);

                    std::vector<uint8_t> process_tentative_data; // Discarded if insufficient room in message response

                    auto get_proc_name = [&]() {
                        std::fstream cmdline(pname, std::ios::in | std::ios::binary);
                        if (cmdline.is_open())
                        {
                            int c;
                            while ((c = cmdline.get()) != '\0' && c != EOF)
                                process_tentative_data.push_back(static_cast<uint8_t>(c));
                            process_tentative_data.push_back('\0');
                            cmdline.close();
                        }
                    };

                    if (env.size())
                    {
                        size_t pos = env.find(LDPR_LOAD);
                        if (pos != std::string::npos)
                        {
                            if (verbose)
                            {
                                for (; pos < env.size() && env.at(pos); pos++)
                                    process_tentative_data.push_back(env.at(pos));
                                process_tentative_data.push_back('\0');
                                get_proc_name();
                                for (size_t i = 0; entry->d_name[i]; i++)
                                    process_tentative_data.push_back(entry->d_name[i]);
                                process_tentative_data.push_back('\0');

                                // Insert this entry
                                system_status_data.tentative_data.insert(
                                    system_status_data.tentative_data.end(), process_tentative_data.begin(), process_tentative_data.end()
                                );
                                system_status_data.entry_count++;
                            }
                            status = sb_sdk_client::TENTATIVE_PRELOAD_DETECTED;
                        }
                    }
                    efile.close();
                }
            }
            if (status == sb_sdk_client::TENTATIVE_PRELOAD_DETECTED && !verbose)
                break;
        }
        closedir(dir);
    }
    else
        status = sb_sdk_client::COMPROMISED_PROC_DENIED;
    return status;
}

sb_sdk_client::STATUS processenvproc(bool verbose, SYSTEM_STATUS_DATA &system_status_data)
{
    sb_sdk_client::STATUS status = checkpreload(verbose, system_status_data);
    if (status == sb_sdk_client::SECURE)
    {
        // Perform server_socket checks
#if CONFIGURABLE_SERVER_PATH
        std::string server_socket_path_client = sb_sdk_client::SentryClient::GetServerSocketPathFromClientConfigFile();
#else
        std::string server_socket_path_client = sb_sdk_client::SERVER_PATH;
#endif
        if (server_socket_path_client != server_socket_path)
            status = sb_sdk_client::COMPROMISED_SERVER_SOCKET_PATH;
    }
    if (status == sb_sdk_client::SECURE)
        status = checkenvironments(verbose, system_status_data);
    return status;
}

void dbusdata(const char* fname, const char* data)
{
    std::fstream fs;
    fs.open(fname, std::ios::binary | std::ios::in);
    if (!fs.is_open())
    {
        fs.open(fname, std::ios::binary | std::ios::out);
        if (fs.is_open())
            fs << data;
        else
            return;
    }
    fs.close();
}

void dbuscontrol(const uint32_t ctrl)
{
    if (ctrl)
    {
        dbusdata(GNOME_1, GNOME_SCREENCAST);
        dbusdata(GNOME_2, GNOME_SCREENSHOT);
        dbusdata(KDE_2, KDE_SCREENSHOT);
    }
    else
    {
        unlink(GNOME_1);
        unlink(GNOME_2);
        unlink(KDE_2);
    }
}

struct ProtectionModeRAII
{
    ProtectionModeRAII()
    {
        if (utils::GetProtectionMode() == utils::PROTECTION_MODE_STATIC)
        {
            ilock_state = true;
            dbuscontrol(1);
        }
    }
    ~ProtectionModeRAII()
    {
        if (utils::GetProtectionMode() == utils::PROTECTION_MODE_STATIC)
        {
            ilock_state = false;
            dbuscontrol(0);
        }
    }
};

bool processmsg(char* buf, Serializer& serializer)
{
    struct timeval timeval;
    gettimeofday(&timeval, NULL); // ie. seconds and microseconds since epoch

    sb_sdk_client::Msg* msg = reinterpret_cast<sb_sdk_client::Msg*>(buf);
    switch (msg->msgtype)
    {
        case sb_sdk_client::ILOCK_STATUS:
        {
            msg->msgtype = sb_sdk_client::ILOCK_RESPONSE;
            msg->msgsize = sizeof(sb_sdk_client::Msg);
            msg->data_1 = static_cast<uint32_t>(ilock_state);
            msg->data_2 = 0;
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): ILOCKSTATUS: ilock_state=%d", ilock_state);
            return true;
        }
        case sb_sdk_client::KEY_SCRAMBLED:
        {
            uint32_t pid = msg->data_1;

            metrics_log((sb_sdk_client::MsgType)msg->msgtype, pid, timeval);

            int kill_retval = 0;
            // Clear focus_pid_wid if the process is not running.
            // We don't worry about not running processes in the process_map, because if theyre not running
            // we obviously won't get key event messages from them.
            if (focus_pid_wid.first && (kill_retval = kill(focus_pid_wid.first, 0)) == -1 && errno == ESRCH)
            {
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): KEY_SCRAMBLED in-focus process pid=%d is no longer running",
                       focus_pid_wid.first);
                process_tracking_mutex.lock();
                protected_processes.erase(focus_pid_wid.first);
                process_tracking_mutex.unlock();
                focus_pid_wid.first = 0;
                focus_pid_wid.second = 0;
            }

            msg->msgtype = sb_sdk_client::KSCRAM_RESPONSE;
            msg->msgsize = sizeof(sb_sdk_client::Msg);
            msg->data_1 = static_cast<uint32_t>(false);
            msg->data_2 = 0;
            if( focus_pid_wid.first && pid != focus_pid_wid.first )
            {
                tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "KEY_SCRAMBLED(): check pid=%u", pid );
                auto iter = process_map.find( pid );
                if( iter != process_map.end() && iter->second == true )
                {
                    // If this process is processing key events AND the service thinks it's in focus
                    // then if the service also thinks a different, protected process is in-focus
                    // then something has probably gone wrong with the focus events. In 99% of
                    // circumstances the process which is handling key events should get them
                    // unscrambled, so if there is mismatch we "fail safe" and assume it's the
                    // focus_pid_wid which is wrong.
                    // Note we do not erase from protected_processes; it's still protected, it's just assumed to be not
                    // currently in-focus (so protection will take effect again on the next in-focus event).

                    // This logic was causing a problem on KDE, the keylogger was getting unscrambled keys because out of sequence FOCUS
                    // events were causing the focus_pid_wid to be cleared. Having an additional sanity check in the preload so it only sends
                    // FOCUS_IN messages if the XGetInputFocus agrees this window is in focus seems to solve the problem however.
                    // Merlin 27th November 2019.
                     tracef( TRACE_WARN, SUBSYSTEM_NAMES::SVC, "resetting focus_pid_wid (problem with focus order?) pid=%u", pid );
                     focus_pid_wid.first = 0;
                     focus_pid_wid.second = 0;
                     msg->data_1 = static_cast<uint32_t>(false);
                }
                else
                {
                    tracef( TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "KEY_SCRAMBLED(): pid:%u is not in focus", pid );
                    msg->data_1 = static_cast<uint32_t>(true);
                }
            }

            tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): KEY_SCRAMBLED: pid=%u scrambled=%d", pid, msg->data_1);
            return true;
        }
        case sb_sdk_client::REG_PROCESS:
        {
            if (utils::GetProtectionMode() == utils::PROTECTION_MODE_DYNAMIC)
            {
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): REG_PROCESS pid=%d", msg->data_1);
                process_tracking_mutex.lock();
                protected_processes.insert(msg->data_1);
                process_tracking_mutex.unlock();
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): REG_PROCESS focus_pid_wid: %d/0x%x", focus_pid_wid.first, focus_pid_wid.second);
            }
            else
            {
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): REG_PROCESS pid=%d ignored", msg->data_1);
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): REG_PROCESS focus_pid_wid: %d/0x%x ignored", focus_pid_wid.first, focus_pid_wid.second);
            }
            break;
        }
        case sb_sdk_client::UNREG_PROCESS:
        {
            if (utils::GetProtectionMode() == utils::PROTECTION_MODE_DYNAMIC)
            {
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): UNREG_PROCESS pid=%d", msg->data_1);
                process_tracking_mutex.lock();
                if (protected_processes.count(msg->data_1))
                {
                    if (msg->data_1 == focus_pid_wid.first)
                    {
                        focus_pid_wid.first = 0;
                        focus_pid_wid.second = 0;
                    }
                    protected_processes.erase(msg->data_1);
                }
                process_tracking_mutex.unlock();
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): UNREG_PROCESS focus_pid_wid: %d/0x%x", focus_pid_wid.first, focus_pid_wid.second);
            }
            else
            {
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): UNREG_PROCESS pid=%d ignored", msg->data_1);
                tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): UNREG_PROCESS focus_pid_wid: %d/0x%x ignored", focus_pid_wid.first, focus_pid_wid.second);
            }
            break;
        }
        case sb_sdk_client::FOCUS_IN:
        {
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): FOCUS_IN pid=%d wid=0x%x", msg->data_1, msg->data_2);
            auto pid_iter = process_map.find( msg->data_1 );
            if( pid_iter != process_map.end() )
            {
                pid_iter->second = true;
            }
            else
            {
                process_map.insert( std::make_pair(msg->data_1, true) );
            }
            auto wid_iter = window_map.find( msg->data_2 );
            if( wid_iter != window_map.end() )
            {
                wid_iter->second = msg->data_1;
            }
            else
            {
                window_map.insert( std::make_pair(msg->data_2, msg->data_1) );
            }

            process_tracking_mutex.lock();
            if (utils::GetProtectionMode() == utils::PROTECTION_MODE_STATIC || 
                (utils::GetProtectionMode() == utils::PROTECTION_MODE_DYNAMIC && protected_processes.count(msg->data_1)))
            {
                focus_pid_wid.first = msg->data_1;
                focus_pid_wid.second = msg->data_2;
            }
            process_tracking_mutex.unlock();
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): FOCUS_IN: focus_pid_wid: %d/0x%x", focus_pid_wid.first, focus_pid_wid.second);
            break;
        }
        case sb_sdk_client::FOCUS_OUT:
        {
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): FOCUS_OUT pid=%d wid=0x%x", msg->data_1, msg->data_2);
            if( msg->data_1 == 0 )
            {
                // Destroy window notification. Set focus to false for relevant pid, erase Window.
                auto win_iter = window_map.find( msg->data_2 );
                if( win_iter != window_map.end() )
                {
                    auto proc_iter = process_map.find( win_iter->second );
                    if( proc_iter != process_map.end() )
                    {
                        proc_iter->second = false;
                    }
                    window_map.erase( win_iter );
                }
            }
            else
            {
                auto iter = process_map.find( msg->data_1 );
                if( iter != process_map.end() )
                {
                    iter->second = false;
                }
                else
                {
                    process_map.insert( std::make_pair(msg->data_1, false) );
                }
            }

            if ( (msg->data_1 == 0 || msg->data_1 == focus_pid_wid.first) &&
                  msg->data_2 == focus_pid_wid.second)
            {
                focus_pid_wid.first = 0;
                focus_pid_wid.second = 0;
            }
            tracef(TRACE_INFO, SUBSYSTEM_NAMES::SVC, "processmsg(): FOCUS_OUT: focus_pid_wid: %d/0x%x", focus_pid_wid.first, focus_pid_wid.second);
            break;
        }
        case sb_sdk_client::ILOCK_SET:
        {
            if (utils::GetProtectionMode() != utils::PROTECTION_MODE_DYNAMIC) break;

            pthread_mutex_lock(&ilock_process_tracking_mutex);
            if (msg->data_1) {
                ilock_protected_processes.insert(msg->data_2);
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): ILOCK_SET: add PID ilock_state=%d pid:%d", msg->data_1, msg->data_2);
            } else {
                if (ilock_protected_processes.erase(msg->data_2)) {
                    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): ILOCK_SET: remove PID ilock_state=%d pid:%d", msg->data_1, msg->data_2);
                } else {
                    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): ILOCK_SET: PID not found ilock_state=%d pid:%d", msg->data_1, msg->data_2);
                }
                if (!ilock_protected_processes.empty()) {
                    tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): ILOCK_SET: still has protected PIDs ilock_state=%d pid:%d", msg->data_1, msg->data_2);
                    pthread_mutex_unlock(&ilock_process_tracking_mutex);
                    break;
                }
            }

            if (ilock_state != msg->data_1) {
                ilock_state = msg->data_1;
                dbuscontrol(msg->data_1);
            } else {
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "ilock state havent changed");
            }
            pthread_mutex_unlock(&ilock_process_tracking_mutex);
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): ILOCK_SET: ilock_state=%d", ilock_state);
            break;
        }
        case sb_sdk_client::SYSTEM_STATUS:
        {
            SYSTEM_STATUS_DATA system_status_data = get_cached_system_status_data();

            sb_sdk_client::STATUS &status = system_status_data.status;

            bool verbose = static_cast<bool>(msg->data_1);

            msg->msgtype = sb_sdk_client::SYSTEM_RESPONSE;
            msg->msgsize = sizeof(sb_sdk_client::Msg);
            if (verbose) msg->msgsize += system_status_data.tentative_data.size();
            msg->data_1 = status;
            msg->data_2 = 0;
            if (verbose)
            {
                if (status == sb_sdk_client::TENTATIVE_NOT_ENTRY_ZERO)
                    msg->data_2 = system_status_data.entry_offset;
                else if (status == sb_sdk_client::TENTATIVE_PRELOAD_DETECTED)
                    msg->data_2 = system_status_data.entry_count;
            }
            system_status_response = std::vector<uint8_t>(sizeof(sb_sdk_client::Msg));
            for (size_t i = 0; i < sizeof(sb_sdk_client::Msg); i++)
                system_status_response[i] = buf[i];
            if (verbose)
            {
                system_status_response.insert(system_status_response.end(),
                    system_status_data.tentative_data.begin(),
                    system_status_data.tentative_data.end());
            }
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "processmsg(): SYSTEM_STATUS: system_status=%d", msg->data_1);
            return true;
        }
        case sb_sdk_client::DUMP_SVC:
        {
            SYSTEM_STATUS_DATA system_status_data = get_cached_system_status_data();

            sb_sdk_client::STATUS &status = system_status_data.status;

            msg->msgtype = sb_sdk_client::DUMP_RESPONSE;
            msg->msgsize = sizeof(sb_sdk_client::Msg);
            msg->data_1 = 0;
            msg->data_2 = 0;

            serializer.Serialize(ilock_state);
            serializer.Serialize(focus_pid_wid.first);
            serializer.Serialize(focus_pid_wid.second);
            size_t pos = serializer.GetPlaceholder<uint32_t>();
            process_tracking_mutex.lock();
            for (auto iter = protected_processes.begin(); iter != protected_processes.end(); ++iter)
                serializer.Serialize(*iter);
            serializer.InsertSerialize<uint32_t>(pos, static_cast<uint32_t>(protected_processes.size()));
            process_tracking_mutex.unlock();
            serializer.Serialize(status);
            pos = serializer.GetPlaceholder<uint32_t>();
            for (std::vector<uint8_t>::iterator iter = system_status_data.tentative_data.begin(); iter != system_status_data.tentative_data.end(); ++iter)
                serializer.Serialize(*iter);
            serializer.InsertSerialize<uint32_t>(pos, static_cast<uint32_t>(system_status_data.entry_count));

            msg->msgsize += serializer.GetSerializedDataSize();

            return true;
        }
        case sb_sdk_client::REG_PROCESSES_STATUS:
        {
            msg->msgtype = sb_sdk_client::REG_PROCESSES_RESPONSE;
            msg->msgsize = sizeof(sb_sdk_client::Msg);
            msg->data_1 = 0;
            msg->data_2 = 0;

            size_t pos = serializer.GetPlaceholder<uint32_t>();
            process_tracking_mutex.lock();
            for (auto iter = protected_processes.begin(); iter != protected_processes.end(); ++iter)
                serializer.Serialize(*iter);
            serializer.InsertSerialize<uint32_t>(pos, static_cast<uint32_t>(protected_processes.size()));
            process_tracking_mutex.unlock();

            msg->msgsize += serializer.GetSerializedDataSize();

            return true;
        }
        default: {}
    }
    return false;
}

int main(void)
{
    try
    {
        int server_sock;
        ssize_t rc, bytes_rec = 0;
        struct sockaddr_un server_sockaddr;
        char buf[256];
        int backlog = 128;
        memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));
        memset((void*)buf, 0, 256);

        server_sock = socket(AF_UNIX, SOCK_STREAM, 0);
        if (server_sock == -1)
        {
            std::cout << "Unable to create domain socket" << std::endl;
            exit(1);
        }
#if CONFIGURABLE_SERVER_PATH
        // Retrieve the name of the server socket from the "server_socket" entry in the service config file
        try
        {
            boost::property_tree::ptree pt;
            boost::property_tree::ini_parser::read_ini(SERVICE_CONFIG_FILE_PATH, pt);
            server_socket_path = pt.get<std::string>("server_socket");
            tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC, "server_socket_path=%s", server_socket_path.c_str());
        }
        catch (...)
        {
            tracef(TRACE_FATAL, SUBSYSTEM_NAMES::SVC, "Failed to read server_socket from %s", SERVICE_CONFIG_FILE_PATH);
            close(server_sock);
            exit(1);
        }
#endif
        server_sockaddr.sun_family = AF_UNIX;
        strcpy(server_sockaddr.sun_path, server_socket_path.c_str());
        socklen_t len = sizeof(server_sockaddr);

        unlink(server_socket_path.c_str());
        rc = bind(server_sock, (struct sockaddr*)&server_sockaddr, len);
        if (rc == -1)
        {
            int errnum = errno;
            tracef(TRACE_FATAL, SUBSYSTEM_NAMES::SVC, "Unable to bind to domain socket %s error=%s", server_socket_path.c_str(), strerror(errnum));
            std::cout << "Unable to bind to domain socket" << std::endl;
            close(server_sock);
            exit(1);
        }

        chmod(server_socket_path.c_str(), (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));

#if CONFIGURABLE_SERVER_PATH
        // Create a client configuration file containing the location of the server socket
        try
        {
            boost::property_tree::ptree pt;
            pt.put("server_socket", server_socket_path);
            boost::property_tree::write_ini(sb_sdk_client::CLIENT_CONFIG_FILE_PATH, pt);
            chmod(sb_sdk_client::CLIENT_CONFIG_FILE_PATH, (S_IRUSR | S_IRGRP | S_IROTH));
        }
        catch (...)
        {
            tracef(TRACE_FATAL, SUBSYSTEM_NAMES::SVC, "Failed to create client config file %s", sb_sdk_client::CLIENT_CONFIG_FILE_PATH);
            close(server_sock);
            exit(1);
        }
#endif

        rc = listen(server_sock, backlog);
        if (rc == -1)
        {
            std::cout << "Error whilst listening to domain socket" << std::endl;
            close(server_sock);
            exit(1);
        }

        ConfigureMetrics();

        tracef(TRACE_ALWAYS, SUBSYSTEM_NAMES::SVC, utils::GetProtectionModeTxt());
        ProtectionModeRAII protection_mode_RAII;

        pthread_t system_status_worker_thread;
        rc = pthread_create(&system_status_worker_thread, NULL, system_status_worker, NULL);
        if (rc != 0) tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC, "main(): failed to create worker thread, error %d", rc);

        pthread_t ilock_process_tracking_removal_thread;
        if (utils::GetProtectionMode() == utils::PROTECTION_MODE_DYNAMIC)
        {
            rc = pthread_create(&ilock_process_tracking_removal_thread, NULL, ilock_process_tracking_removal_worker, NULL);
            if (rc != 0) tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC, "main(): failed to create ilock worker thread, error %d", rc);
        }

        pthread_t process_tracking_removal_thread;
        rc = pthread_create(&process_tracking_removal_thread, NULL, process_tracking_removal_worker, NULL);
        if (rc != 0) tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC, "main(): failed to create worker thread, error %d", rc);

        while (true)
        {
            struct sockaddr_un client_sockaddr;
            memset(&client_sockaddr, 0, sizeof(struct sockaddr_un));
            int client_sock = accept(server_sock, (struct sockaddr*)&client_sockaddr, &len);
            if (client_sock == -1)
            {
                std::cout << "Error whilst accepting socket connection" << std::endl;
                continue;
            }

            len = sizeof(client_sockaddr);
            rc = getpeername(client_sock, (struct sockaddr *)&client_sockaddr, &len);
            if (rc == -1)
            {
                std::cout << "Error whilst getting peername from connection" << std::endl;
                close(client_sock);
                continue;
            }

            bytes_rec = recv(client_sock, (void*)buf, sizeof(sb_sdk_client::Msg), 0);
            if (bytes_rec == -1)
            {
                std::cout << "Error whilst receiving data from domain socket" << std::endl;
                close(client_sock);
                continue;
            }

            Serializer serializer;

            if (processmsg(buf, serializer))
            {
                sb_sdk_client::Msg* msg = reinterpret_cast<sb_sdk_client::Msg*>(buf);
                if (msg->msgtype == sb_sdk_client::DUMP_RESPONSE || msg->msgtype == sb_sdk_client::REG_PROCESSES_RESPONSE)
                {
                    std::vector<uint8_t> message_response;

                    for (unsigned i = 0; i < sizeof(sb_sdk_client::Msg); i++)
                        message_response.push_back(buf[i]);

                    const uint8_t *serialized_response_data = serializer.GetSerializedData();

                    for (unsigned i = 0; i < serializer.GetSerializedDataSize(); i++)
                        message_response.push_back(serialized_response_data[i]);

                    rc = send(client_sock, message_response.data(), message_response.size(), MSG_NOSIGNAL);
                }
                else if (msg->msgtype == sb_sdk_client::SYSTEM_RESPONSE)
                {
                    rc = send(client_sock, system_status_response.data(), system_status_response.size(), MSG_NOSIGNAL);
                }
                else
                    rc = send(client_sock, (void*)buf, sizeof(sb_sdk_client::Msg), MSG_NOSIGNAL);
                if (rc == -1)
                {
                    std::cout << "Error whilst sending response to client" << std::endl;
                    close(client_sock);
                    continue;
                }
            }
            close(client_sock);
        }
        close(server_sock);
    }
    catch (const std::exception& ex)
    {
        std::cout << "std::exception occurred, what=\"" << ex.what() << "\""<< std::endl;
        return 1;
    }
    catch (...)
    {
        std::exception_ptr exception = std::current_exception();

        std::cout << "non std::exception occurred, type=" << std::flush;
        std::cout << (exception ? exception.__cxa_exception_type()->name() : "unknown") << std::endl;
        return 1;
    }
    return 0;
}
