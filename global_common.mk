ifndef ARCH
$(error ARCH is not set)
endif

OBJS := $(ARCH)/objs
LIBS := $(ARCH)/libs
EXES := $(ARCH)/executables
