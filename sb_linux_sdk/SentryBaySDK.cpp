#include "SentryBaySDK.h"
#include "SentryClient.h"

#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <string>

static Window get_focus_window()
{
  static Display *display = NULL;
  Window focus_window = 0;
  int revert_to;
  if (!display) display = XOpenDisplay("");
    XGetInputFocus(display, &focus_window, &revert_to);

  return focus_window;
}

namespace sb_sdk
{
  bool RegisterProcessForAntiKeylogging( Display* display, Window wid )
  {
    // 1. Register this process with the service to let it know that it's a protected application
    // 2. Make sure we're receiving focus change events on this window
    // 3. If the supplied window id happens to be the current in-focus window, we'll need to inform the service
    // The actual processing of focus change events is done in the preload library.
    if( display == NULL ) return false;

    sb_sdk_client::SentryClient client;

    if (client.RegisterProcess( getpid() ) != 0) return false;

    if (wid)
    {
      XWindowAttributes window_attributes;
      if (XGetWindowAttributes( display, wid, &window_attributes ) && ( window_attributes.your_event_mask & FocusChangeMask ) == 0 )
      {
        // Only invoke XSelectInput() if we really need to ie. FocusChangeMask is *not* already set
        XSelectInput( display, wid, window_attributes.your_event_mask | FocusChangeMask);
      }

      if (wid == get_focus_window())
      {
        client.FocusInWindow( getpid(), wid );
      }
    }

    return true;
  }

  bool DeregisterProcessForAntiKeylogging()
  {
    sb_sdk_client::SentryClient client;

    return client.UnRegisterProcess( getpid() ) == 0;
  }

  bool EnableScreenCaptureProtection()
  {
    bool ilockstate = false;

    sb_sdk_client::SentryClient client;

    if (client.SetILockState(true) != 0) return false;

    if (client.GetILockState(ilockstate) != 0) return false;

    return ilockstate == true;
  }

  bool DisableScreenCaptureProtection()
  {
    bool ilockstate = true;

    sb_sdk_client::SentryClient client;

    if (client.SetILockState(false) != 0) return false;

    if (client.GetILockState(ilockstate) != 0) return false;

    return ilockstate == false;
  }

  bool ScreenCaptureProtectionEnabled()
  {
    bool ilockstate = false;

    sb_sdk_client::SentryClient client;

    if (client.GetILockState(ilockstate) != 0) return false;

    return ilockstate;
  }

  static void systemStatus(sb_sdk_client::STATUS status, SystemStatus &system_status)
  {
    switch(status)
    {
    case sb_sdk_client::COMPROMISED_SERVER_SOCKET_PATH:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_SERVER_SOCKET_PATH);
      break;

    case sb_sdk_client::COMPROMISED_NO_LIBRARY:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_SENTRYBAY_LIBRARY_MISSING);
      break;

    case sb_sdk_client::COMPROMISED_NO_PRELOAD:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_LD_SO_PRELOAD_MISSING);
      break;

    case sb_sdk_client::COMPROMISED_NO_ENTRY:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_LD_SO_PRELOAD_NO_SENTRYBAY_ENTRY);
      break;

    case sb_sdk_client::COMPROMISED_PROC_DENIED:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_PROC_OPEN_FAILED);
      break;

    case sb_sdk_client::TENTATIVE_NOT_ENTRY_ZERO:
      system_status.status(STATUS_TENTATIVE);
      system_status.reason(REASON_LD_SO_PRELOAD_SENTRYBAY_NOT_FIRST_ENTRY);
      break;

    case sb_sdk_client::TENTATIVE_PRELOAD_DETECTED:
      system_status.status(STATUS_TENTATIVE);
      system_status.reason(REASON_PROCESS_RUNNING_WITH_LD_PRELOAD);
      break;

    case sb_sdk_client::SECURE:
      system_status.status(STATUS_SECURE);
      system_status.reason(REASON_SYSTEM_SECURE);
      break;

    case sb_sdk_client::SEND_FAILED:
      system_status.status(STATUS_COMPROMISED);
      system_status.reason(REASON_SEND_MESSAGE_FAILED);
      break;

    default:
      system_status.status(STATUS_UNKNOWN);
      system_status.reason(REASON_UNKNOWN);
      break;
    }
  }

  enum StatusCode_t GetSystemStatus(void)
  {
    SystemStatus system_status;

    sb_sdk_client::SentryClient client;
    sb_sdk_client::StatusData status_data;
    client.GetSystemStatus(false, status_data);
    systemStatus(status_data.status, system_status);

    return system_status.status();
  }

  void GetSystemStatusVerbose(SystemStatus &system_status)
  {
    sb_sdk_client::SentryClient client;
    sb_sdk_client::StatusData status_data;
    client.GetSystemStatus(true, status_data);
    systemStatus(status_data.status, system_status);

    SystemStatus::ProcessInfoList_t processes;

    for (auto it = status_data.entries.begin(); it != status_data.entries.end(); it++)
    {
      std::vector<std::string> entry = *it;

      struct SystemStatus::ProcessInfo_t processInfo;

      processInfo.pid = std::stoi(entry[2]);
      processInfo.fullpathname = entry[1];
      processInfo.ld_preload_env = entry[0];

      processes.push_back(processInfo);
    }

    system_status.suspiciousProcesses(processes);
  }

  std::set<pid_t> GetRegisteredProcesses()
  {
    std::set<pid_t> retval;

    sb_sdk_client::SentryClient client;

    std::set<uint32_t> pids;
    client.GetRegisteredProcesses(pids);

    // Throughout the Sentry Service and header file, pids are stored as uint32_t
    // Convert them to pid_t
    for (auto it = pids.begin(); it != pids.end(); it++)
      retval.insert(*it);

    return retval;
  }
}


void updateServiceStatus()
{
  char strStatus[252] = "Unknown";
  char strReason[252] = "Unknown"; //later make it dynamic

  sb_sdk::SystemStatus systemStatus;
  sb_sdk::StatusCode_t statusCode = sb_sdk::GetSystemStatus();
  if (statusCode == sb_sdk::STATUS_SECURE)
  {
      systemStatus.status(statusCode);
      systemStatus.reason(sb_sdk::REASON_SYSTEM_SECURE);
  }
  else
  {
      sb_sdk::GetSystemStatusVerbose(systemStatus);
  }

  switch(systemStatus.status())
  {
  case sb_sdk::STATUS_UNKNOWN:
      strcpy(strStatus,"Unknown");
      break;
  case sb_sdk::STATUS_COMPROMISED:
      strcpy(strStatus,"COMPROMISED");
      break;
  case sb_sdk::STATUS_TENTATIVE:
      strcpy(strStatus,"TENTATIVE");
      break;
  case sb_sdk::STATUS_SECURE:
      strcpy(strStatus,"SECURE");
      break;
  };

  switch(systemStatus.reason())
  {
    case sb_sdk::REASON_UNKNOWN:
        strcpy(strReason,"Unknown");
        break;
    case sb_sdk::REASON_SYSTEM_SECURE:
        strcpy(strReason,"Secure");
        break;
    case sb_sdk::REASON_SEND_MESSAGE_FAILED:
        strcpy(strReason,"Failed to send message to SentryBay service (service stopped?)");
        break;
    case sb_sdk::REASON_SENTRYBAY_LIBRARY_MISSING:
        strcpy(strReason,"/usr/local/lib/sentrybay/libsentrybay.so file missing");
        break;
    case sb_sdk::REASON_LD_SO_PRELOAD_MISSING:
        strcpy(strReason,"/etc/ld.so.preload file missing");
        break;
    case sb_sdk::REASON_LD_SO_PRELOAD_NO_SENTRYBAY_ENTRY:
        strcpy(strReason,"No libsentrybay.so entry in /etc/ld.so.preload");
        break;
    case sb_sdk::REASON_LD_SO_PRELOAD_SENTRYBAY_NOT_FIRST_ENTRY:
        strcpy(strReason,"libsentrybay.so is not first entry in /etc/ld.so.preload");
        break;
    case sb_sdk::REASON_PROC_OPEN_FAILED:
        strcpy(strReason,"SentryBay service failed to open system /proc folder");
        break;
    case sb_sdk::REASON_PROCESS_RUNNING_WITH_LD_PRELOAD:
        strcpy(strReason,"Processes using LD_PRELOAD");
        break;
    case sb_sdk::REASON_SERVER_SOCKET_PATH:
        strcpy(strReason,"Misconfigured Server Socket Path");
        break;
  }

  tracef(TRACE_WARN, SUBSYSTEM_NAMES::SVC, "Service Status: %s", strStatus);
  tracef(TRACE_WARN, SUBSYSTEM_NAMES::SVC, "Reason: %s", strReason);

  sb_sdk::SystemStatus::ProcessInfoList_t processes = systemStatus.suspiciousProcesses();

  for (auto it = processes.begin(); it != processes.end(); it++)
  {
    sb_sdk::SystemStatus::ProcessInfo_t processInfo = *it;
    tracef(TRACE_WARN, SUBSYSTEM_NAMES::SVC, "Suspicious Process Id(pid)=%d info full path: %s", static_cast<unsigned>(processInfo.pid), processInfo.fullpathname.c_str());
    tracef(TRACE_WARN, SUBSYSTEM_NAMES::SVC, "Suspicious Process Id(pid)=%d info preload env: %s", static_cast<unsigned>(processInfo.pid), processInfo.ld_preload_env.c_str());
  }

}

#ifdef __cplusplus
extern "C" {
#endif
bool wrapper_EnableScreenCaptureProtection()
{
  return sb_sdk::EnableScreenCaptureProtection();
}

bool wrapper_ScreenCaptureProtectionEnabled()
{
  return sb_sdk::ScreenCaptureProtectionEnabled();
}

bool wrapper_DisableScreenCaptureProtection()
{
  return sb_sdk::DisableScreenCaptureProtection();
}

bool wrapper_RegisterProcessForAntiKeylogging(Display* display, Window wid)
{
  return sb_sdk::RegisterProcessForAntiKeylogging(display,wid);
}

bool wrapper_DeregisterProcessForAntiKeylogging()
{
  return sb_sdk::DeregisterProcessForAntiKeylogging();
}

void wrapper_updateServiceStatus()
{
  updateServiceStatus();
}

bool SystemStatusIsSecure()
{
  sb_sdk::StatusCode_t statusCode = sb_sdk::GetSystemStatus();

  if (statusCode == sb_sdk::STATUS_SECURE) {
    return true;
  } else {
    return false;
  }
}

#ifdef __cplusplus
}
#endif
