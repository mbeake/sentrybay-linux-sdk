#ifndef __SYSTEM_STATUS_H__
#define __SYSTEM_STATUS_H__

#include <sys/types.h>
#include <string>
#include <vector>

namespace sb_sdk
{
  enum StatusCode_t
  {
    STATUS_UNKNOWN,
    STATUS_COMPROMISED,
    STATUS_TENTATIVE,
    STATUS_SECURE
  };

  enum ReasonCode_t
  {
    REASON_UNKNOWN,
    REASON_SYSTEM_SECURE,
    REASON_SEND_MESSAGE_FAILED,
    REASON_SENTRYBAY_LIBRARY_MISSING,
    REASON_LD_SO_PRELOAD_MISSING,
    REASON_LD_SO_PRELOAD_NO_SENTRYBAY_ENTRY,
    REASON_LD_SO_PRELOAD_SENTRYBAY_NOT_FIRST_ENTRY,
    REASON_PROC_OPEN_FAILED,
    REASON_PROCESS_RUNNING_WITH_LD_PRELOAD,
    REASON_SERVER_SOCKET_PATH
  };

  class SystemStatus
  {
  public:

    /*
     * Structure for containing process information, in particular for those
     * processes that are suspected of being a security thread.
     */
    struct ProcessInfo_t
    {
      pid_t pid; // process id
      std::string fullpathname; // full path name *and* process name
      std::string ld_preload_env; // the value of the LD_PRELOAD environment parameter
    };

    typedef std::vector<ProcessInfo_t> ProcessInfoList_t;

    /* Default constructor
     *
     * Defaults to system status SECURE (reason code SYSTEM_SECURE).
     */
    SystemStatus();

    /* The system status to be returned to the SDK application.
     *
     * The SDK requests the system status from the SentryBay service.
     * Note that if the SentryBay service is not running or there is a problem
     * communicating with the SentryBay service the SDK will return a system status
     * of COMPROMISED.
     */
    StatusCode_t status() const { return m_status; }
    void status(StatusCode_t status) { m_status = status; }

    /* The reason for the reported system status.
     *
     * If system status is SECURE, the reason code will be SYSTEM_SECURE.
     * Otherwise the reason code will indicate the reason why the system is not SECURE.
     */
    ReasonCode_t reason() const { return m_reason; }
    void reason(ReasonCode_t reason) { m_reason = reason; }

    /* Returns a list of suspicious processes.
     *
     * If the systen is SECURE, the list will be empty.
     * If the system status is PROCESS_RUNNING_WITH_LD_PRELOAD there will be at least one entry.
     */
    ProcessInfoList_t suspiciousProcesses() const { return m_suspiciousProcesses; }
    void suspiciousProcesses(ProcessInfoList_t processes) { m_suspiciousProcesses = processes; }

  private:
    StatusCode_t m_status;
    ReasonCode_t m_reason;
    ProcessInfoList_t m_suspiciousProcesses;
  };
}

#endif  
