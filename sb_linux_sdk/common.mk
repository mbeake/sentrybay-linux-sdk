include ../global_common.mk

INCS := -I../trace -I../include -Iinclude
DEPLIBS := ../trace/$(LIBS)/libtrace.a
CXXFLAGS := -Wall -Werror -O3 -std=c++11 $(INCS)

all: $(LIBS)/libsentrybaysdk.a $(LIBS)/libsentrybaysdk.so

$(LIBS)/libsentrybaysdk.a: $(OBJS)/SentryBaySDK.o $(OBJS)/SystemStatus.o $(DEPLIBS)
	@mkdir -p $(LIBS)
	@ar rcs $@ $^

$(LIBS)/libsentrybaysdk.so: $(OBJS)/SentryBaySDK.o $(OBJS)/SystemStatus.o $(DEPLIBS)
	@$(CC) -shared -o $(LIBS)/libsentrybaysdk.so $(OBJS)/SentryBaySDK.o $(OBJS)/SystemStatus.o $(DEPLIBS)

$(OBJS)/SentryBaySDK.o: SentryBaySDK.cpp
	@mkdir -p $(OBJS)
	@$(CC) $(CXXFLAGS) -fPIC -c -o $@ $^

$(OBJS)/SystemStatus.o: SystemStatus.cpp
	@mkdir -p $(OBJS)
	@$(CC) $(CXXFLAGS) -c -o $@ $^

clean: 
	@rm -rf $(OBJS) $(LIBS)
