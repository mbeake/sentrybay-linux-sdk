#ifndef TRACE_H_
#define TRACE_H_

#ifdef __cplusplus
extern "C" {
#endif

enum TraceCategory
{
    TRACE_ALWAYS = 0,
    TRACE_DEBUG = 1,
    TRACE_INFO = 2,
    TRACE_WARN = 4,
    TRACE_ERROR = 8,
    TRACE_FATAL = 16
};

struct SUBSYSTEM_NAMES
{
    static constexpr const char* DLFCN = "DLFCN";
    static constexpr const char* ILOCK = "ILOCK";
    static constexpr const char* LIB_INIT = "LIB_INIT";
    static constexpr const char* SBKEYS = "SBKEYS";
    static constexpr const char* SVC = "SVC";
    static constexpr const char* SVC_CLIENT = "SVC_CLIENT";
    static constexpr const char* SVC_METRICS = "SVC_METRICS";
    static constexpr const char* X11 = "X11";
    static constexpr const char* X11KEYS = "X11KEYS";
    static constexpr const char* XCB = "XCB";
    static constexpr const char* XCBKEYS = "XCBKEYS";
    static constexpr const char* GETIDS = "GETIDS";
    static constexpr const char* ICA_LAUNCHER = "ICA_LAUNCHER";
};

void trace(enum TraceCategory category, const char * subsystem, const char * message);

void tracef(enum TraceCategory category, const char * subsystem, const char * format, ...);

#ifdef __cplusplus
}
#endif

#endif /* TRACE_H_ */
