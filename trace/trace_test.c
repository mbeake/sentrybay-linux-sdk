#include "trace.h"

int main()
{
    trace(TRACE_DEBUG, "TEST", "trace(): TRACE_DEBUG TEST");
    trace(TRACE_INFO,  "TEST", "trace(): TRACE_INFO TEST");
    trace(TRACE_WARN,  "TEST", "trace(): TRACE_WARN TEST");
    trace(TRACE_ERROR, "TEST", "trace(): TRACE_ERROR TEST");
    trace(TRACE_FATAL, "TEST", "trace(): TRACE_FATAL TEST");

    tracef(TRACE_DEBUG, "TEST", "trace(): TRACE_%s TEST", "DEBUG");
    tracef(TRACE_INFO,  "TEST", "trace(): TRACE_%s TEST", "INFO");
    tracef(TRACE_WARN,  "TEST", "trace(): TRACE_%s TEST", "WARN");
    tracef(TRACE_ERROR, "TEST", "trace(): TRACE_%s TEST", "ERROR");
    tracef(TRACE_FATAL, "TEST", "trace(): TRACE_%s TEST", "FATAL");

    return 0;
}
