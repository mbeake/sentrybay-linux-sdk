#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>
#include <string>
#include <syslog.h>
#include <sys/stat.h>
#include <unistd.h>
#include <map>

#include <boost/optional/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "trace.h"
////
//
// SEC-21 - Careful not to call tracing logic in functions in ../include/utils.h
//          Will end up in an infinite loop
//          ../include/utils.h functions used in traceing logic have warning comments in their body
//
////
#include "../include/utils.h"

#ifdef __cplusplus
extern "C" {
#endif

namespace
{
std::string categoryToString(enum TraceCategory category)
{
    switch(category) {
    case TRACE_ALWAYS: return "ALWAYS";
    case TRACE_DEBUG: return "DEBUG";
    case TRACE_INFO: return "INFO";
    case TRACE_WARN: return "WARN";
    case TRACE_ERROR: return "ERROR";
    case TRACE_FATAL: return "FATAL";
    }
    return "UNKNOWN";
}

void writeToLog(enum TraceCategory category, const std::string &subsystem, const std::string &message)
{
    struct timeval timeval;
    gettimeofday(&timeval, NULL); // ie. seconds and microseconds since epoch

    struct tm tm;
    localtime_r(&timeval.tv_sec, &tm);

    char buffer[256];
    buffer[sizeof(buffer) - 1] = '\0';
    size_t size = strftime(buffer, sizeof(buffer) - 1, "%Y/%m/%d %H:%M:%S", &tm);
    snprintf(&buffer[size], sizeof(buffer) - size - 1, ".%06ld, %s, %s, 0x%lx, %s",
             (long)timeval.tv_usec, categoryToString(category).c_str(), subsystem.c_str(), (long)pthread_self(), message.c_str());

    openlog(NULL, LOG_PID|LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "%s", buffer);
    closelog();
}
#define writeToLog_VARARG_MACRO va_list args; va_start(args, format); char buffer[256]; buffer[sizeof(buffer) - 1] = '\0'; vsnprintf(buffer, sizeof(buffer) - 1, format, args); va_end(args); writeToLog(category, subsystem, buffer);
void writeToLogf(enum TraceCategory category, const std::string &subsystem, const char *format, ...)
{
    writeToLog_VARARG_MACRO
}

boost::property_tree::ptree & getCategoriesCache()
{
    thread_local static boost::property_tree::ptree categories_cache;
    return categories_cache;
}

// Read the trace ini file to decide if the entry for this category+subsystem should be writen to the file
bool logThisMessage(enum TraceCategory category, const std::string &subsystem)
{
#ifndef DEBUG_WHITELISTING
    if(utils::isWhitelisted(subsystem.c_str())) return false;
#endif

    if (category == TRACE_ALWAYS)
    {
        return true;
    }

  ////
  //
  // SEC-9, SEC-21
  //  Multiple process and threads can attempt to process the ini file at the same time.
  //  This creates a performance issue on most machines and can crash processes on some machines.
  //  To reduces the chances of these issues occurring, the reading of the ini file is to be staggered
  //  in each thread by 'interval' (with each thread hopefully having a different value for 'interval'
  //  in most intances) provided the file has changes since the last read.
  //  A consequence of this is that the ini file has to be cached which has to be updated on
  //  every read and cleared if the file is not present.

  //
  // SEC-24 - Do not have any writeToLog() and writeToLogf() outside of DEBUG guards.
  //          Otherwise, Chromium on CIGNA does not start properly.
  //

    static const std::string trace_ini_abs_path = "/usr/local/bin/sentrybay/.trace/trace.ini";

    // If the ini file does not exist then it might have been deleted so reset categories map
    struct stat stats;
    if (stat(trace_ini_abs_path.c_str(), &stats) != 0)
    {
#ifdef DEBUG
        writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - stat = %d", stat(trace_ini_abs_path.c_str(), &stats));
#endif //DEBUG
        getCategoriesCache().clear();
        return false;
    }

    // The ini file does exist so only re/load it if it has changed since the last time it was read, provided 'interval' time has passed
    struct timeval &last_time_ini_loaded_timeval = utils::getLastCheckedTimeval();
    struct timeval current_time_timeval;
    gettimeofday(&current_time_timeval, NULL); // ie. seconds and microseconds since epoch
    int interval = utils::getCheckInterval(current_time_timeval);
    // If this is the first time the ini file will be read pause for 'interval' usecs to
    // reduce the chances of multiple processes/threads reading the file at once
    if (last_time_ini_loaded_timeval.tv_sec == 0 && last_time_ini_loaded_timeval.tv_usec == 0)
    {
#ifdef DEBUG
        writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - init interval = %d", interval);
#endif //DEBUG
        // (this part of the code only runs on the first pass through this code)
        // need to initialize last_time_ini_loaded_timeval in this run,
        // but this needs to done in a way 'randomized' way to prevent performance issues
        // due all threads initialize the boost::property_tree::ptree all at once
        // - simplest solution is to usleep
        // (interval is a just a value here - its time unit is irrelavent)
        usleep(interval);
    }
    // If the sufficient time has passed and the ini file has been modified since last read
    // then read the ini file and update the cache
    int time_diff = (current_time_timeval.tv_sec - last_time_ini_loaded_timeval.tv_sec);
    bool has_trace_ini_been_modified = utils::hasFileBeenModifiedSinceLastTimeval(stats, last_time_ini_loaded_timeval);
    bool read_ini_file = (time_diff > interval) && has_trace_ini_been_modified;
#ifdef DEBUG
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - last_time_ini_loaded_timeval->tv_sec = %d", last_time_ini_loaded_timeval.tv_sec);
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - last_time_ini_loaded_timeval->tv_usec = %d", last_time_ini_loaded_timeval.tv_usec);
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - current_time_timeval.tv_sec = %d", current_time_timeval.tv_sec);
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - current_time_timeval.tv_usec = %d", current_time_timeval.tv_usec);
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - timediff = %d", time_diff);
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - interval = %d", interval);
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - hasFileBeenModifiedSinceLastRead() = %s", has_trace_ini_been_modified ? "true" : "false");
    writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - read_ini_file = %s", read_ini_file ? "true" : "false");
#endif //DEBUG
    if (read_ini_file)
    {
        // record the time that file is being read for the next iteration
        last_time_ini_loaded_timeval = current_time_timeval;
#ifdef DEBUG
        writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - new last_time_ini_loaded_timeval->tv_sec = %d", last_time_ini_loaded_timeval.tv_sec);
        writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - new last_time_ini_loaded_timeval->tv_usec = %d", last_time_ini_loaded_timeval.tv_usec);
        writeToLog(TRACE_ALWAYS, subsystem, "logThisMessage() - reading trace.ini");
#endif //DEBUG
        try
        {
            boost::property_tree::ini_parser::read_ini(trace_ini_abs_path, getCategoriesCache());
        }
        catch (const std::exception &e)
        {
            writeToLog(TRACE_FATAL, subsystem, e.what());
        }
        catch (...)
        {
            writeToLog(TRACE_FATAL, subsystem, "Could not read trace ini");
        }
    }
    std::string categories = getCategoriesCache().get("Default.Categories", "");
    boost::optional<boost::property_tree::ptree&> treeSection = getCategoriesCache().get_child_optional(subsystem);
    if (treeSection)
    {
        categories = treeSection.get().get("Categories", categories);
    }
#ifdef DEBUG
    if (!categories.empty())
    {
        writeToLogf(TRACE_ALWAYS, subsystem, "logThisMessage() - [%s]", categories.c_str());
    }
#endif //DEBUG
    // Get the info from the cache for this category+subsystem
    return (categories.find("ALL") != std::string::npos ||
            categories.find(categoryToString(category)) != std::string::npos);
  //
  /////
}

void trace(enum TraceCategory category, const char * subsystem, const char * message)
{
    if (logThisMessage(category, subsystem) == false) return;
    writeToLog(category, subsystem, message);
}

void tracef(enum TraceCategory category, const char * subsystem, const char * format, ...)
{
    if (logThisMessage(category, subsystem) == false) return;
    writeToLog_VARARG_MACRO
}
}
#ifdef __cplusplus
}
#endif
