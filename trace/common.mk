include ../global_common.mk

INCS := 
CXXFLAGS := -fvisibility=hidden -Wall -fPIC -DPIC -Werror -O3 -std=c++11 $(INCS)

all: $(LIBS)/libtrace.a

$(LIBS)/libtrace.a: $(OBJS)/trace.o
	@mkdir -p $(LIBS)
	@ar rcs $@ $^

$(OBJS)/trace.o: trace.cpp
	@mkdir -p $(OBJS)
	@$(CC) $(CXXFLAGS) -c -o $@ $^

clean: 
	@rm -rf $(OBJS) $(LIBS)
