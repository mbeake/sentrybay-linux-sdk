#ifndef _SENTRY_CLIENT_H_
#define _SENTRY_CLIENT_H_

#include <cstdint>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <set>
#include <mutex>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <string>
#include <sstream>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include "utils.h"
#include "trace.h"

// Indicates whether the server socket path is static (SERVER_PATH) or dynamic (read from CLIENT_CONFIG_FILE_PATH).
// Note: The CONFIGURABLE_SERVER_PATH value is updated by the build scripts.
#define CONFIGURABLE_SERVER_PATH 0

namespace sb_sdk_client
{
static constexpr const char *SERVER_PATH = "/usr/local/bin/sentrybay/.sentry_server";
static constexpr const char *CLIENT_CONFIG_FILE_PATH = "/usr/local/bin/sentrybay/.client.conf";
static constexpr const char *LIB_NAME = "/usr/local/lib/sentrybay/libsentrybay.so";

enum MsgType : uint32_t
{
    ILOCK_STATUS            = 0x30F11DC2,
    ILOCK_RESPONSE          = 0x56C8C66C,
    REG_PROCESS             = 0x1BF327A8,
    UNREG_PROCESS           = 0x761DFFE9,
    KEY_SCRAMBLED           = 0x3DBEEFD4,
    KSCRAM_RESPONSE         = 0x4DA1E7C3,
    FOCUS_IN                = 0x494EE88A,
    FOCUS_OUT               = 0x3C204ED4,
    ILOCK_SET               = 0x3C0823FC,
    DUMP_SVC                = 0x162DDC01,
    DUMP_RESPONSE           = 0x5A96A95E,
    SYSTEM_STATUS           = 0x5420601C,
    SYSTEM_RESPONSE         = 0x528EEDC6,
    REG_PROCESSES_STATUS    = 0x57C4E496,
    REG_PROCESSES_RESPONSE  = 0x02B7CC2F,
};

typedef struct
{
    MsgType msgtype;
    uint32_t msgsize; // Total message size including the size of the msg struct itself
    uint32_t data_1;
    uint64_t data_2;
} Msg;

enum STATUS : uint32_t
{
    COMPROMISED_SERVER_SOCKET_PATH,
    COMPROMISED_NO_LIBRARY,
    COMPROMISED_NO_PRELOAD,
    COMPROMISED_NO_ENTRY,
    COMPROMISED_PROC_DENIED,
    TENTATIVE_NOT_ENTRY_ZERO,
    TENTATIVE_PRELOAD_DETECTED,
    SECURE,
    SEND_FAILED
};

typedef struct _StatusData
{
    _StatusData() : count(0) {}
    STATUS status;
    uint32_t count;
    std::vector<std::vector<std::string>> entries;
} StatusData;

typedef struct _DumpData
{
    _DumpData() : ilock_status(false), status_data() {}
    bool ilock_status;
    std::pair<uint32_t, uint64_t> focus_pid_wid;
    std::set<uint32_t> pids;
    StatusData status_data;
} DumpData;

class DeSerializer
{
    public:
        DeSerializer(void* data) : ptr(data) {}
        const uint32_t GetU32Value()
        {
            uint32_t* p = static_cast<uint32_t*>(ptr);
            uint32_t v = *p++;
            ptr = p;
            return v;
        }
        const uint64_t GetU64Value()
        {
            uint64_t* p = static_cast<uint64_t*>(ptr);
            uint64_t v = *p++;
            ptr = p;
            return v;
        }
        const uint16_t GetU16Value()
        {
            uint16_t* p = static_cast<uint16_t*>(ptr);
            uint16_t v = *p++;
            ptr = p;
            return v;
        }
        const uint8_t GetU8Value()
        {
            uint8_t* p = static_cast<uint8_t*>(ptr);
            uint8_t v = *p++;
            ptr = p;
            return v;
        }
        void* GetPtr() const { return ptr; }
    private:
        void* ptr;
};

static std::vector<std::vector<std::string>> GetStringEntries(uint8_t* ptr, uint32_t count)
{
    std::vector<std::vector<std::string>> entries;

    auto get_string = [&]() -> std::string {
        std::stringstream ss;
        for (; *ptr; ptr++)
            ss << *ptr;
        ptr++;
        return ss.str();
    };

    for (uint32_t i = 0; i < count; i++)
    {
        std::vector<std::string> entry;
        entry.push_back(get_string());
        entry.push_back(get_string());
        entry.push_back(get_string());
        entries.push_back(entry);
    }
    return entries;
}

class SentryClient
{
    public:
        static std::string GetServerSocketPathFromClientConfigFile()
        {
            std::string server_socket_path;
            // Retrieve the name of the server socket from the "server_socket" entry in the client config file
            try
            {
                boost::property_tree::ptree pt;
                boost::property_tree::ini_parser::read_ini(CLIENT_CONFIG_FILE_PATH, pt);
                server_socket_path = pt.get<std::string>("server_socket");
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_CLIENT, "server_socket_path=%s", server_socket_path.c_str());
            }
            catch (...)
            {
                tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "Failed to read server_socket from %s", CLIENT_CONFIG_FILE_PATH);
            }
            return server_socket_path;
        }
        static std::string ServerSocketPath()
        {
#if CONFIGURABLE_SERVER_PATH
            static bool server_socket_path_initialised = false;
            static std::string server_socket_path;
            if (!server_socket_path_initialised)
            {
                static std::mutex mutex;
                const std::lock_guard<std::mutex> lock(mutex); // mutex released when lock_guard goes out of scope

                if (!server_socket_path_initialised)
                {
                    server_socket_path = GetServerSocketPathFromClientConfigFile();
                    server_socket_path_initialised = (server_socket_path.empty() == false);
                }
            }
            return server_socket_path;
#else
            return SERVER_PATH;
#endif
        }
        int SetILockState(bool state) {
            pid_t ilockpid = getpid();
            if (ilockpid == 0 || ilockpid == -1) {
                tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "Not a valid pid %d ", ilockpid);
                return 1;
            }
            return securesend(ILOCK_SET, state, ilockpid);
        }
        int GetILockState(bool &state)
        {
            msg.msgtype = ILOCK_STATUS;
            msg.msgsize = sizeof(Msg);
            int returnValue = sendmsg(true);
            if (returnValue == 0 && msg.msgtype == ILOCK_RESPONSE)
                state = static_cast<bool>(msg.data_1);
            return returnValue;
        }
        int IsKeyScrambled(const uint32_t pid, bool &state)
        {
            if (utils::GetProtectionMode() & utils::PROTECTION_MODE_STATIC)
            {
                bool have_focus = (gfocuswid() != 0);
                state = !have_focus;
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_CLIENT, "IsKeyScrambled(): %s short circuit, wid=%d, have_focus=%s, state=%s, return=0, %p",
                       utils::GetProtectionModeTxt(), gfocuswid(), have_focus ? "true" : "false", state ? "true" : "false", &(gfocuswid()));
                return 0;
            }
            msg.msgtype = KEY_SCRAMBLED;
            msg.msgsize = sizeof(Msg);
            msg.data_1 = pid;
            int returnValue = sendmsg(true);
            if (returnValue == 0 && msg.msgtype == KSCRAM_RESPONSE)
                state = static_cast<bool>(msg.data_1);
            return returnValue;
        }
        int RegisterProcess(const uint32_t pid) { return securesend(REG_PROCESS, pid); }
        int UnRegisterProcess(const uint32_t pid) { return securesend(UNREG_PROCESS, pid); }
        int FocusInWindow(const uint32_t pid, const uint64_t wid)
        {
            gfocuswid() = wid;
            if (utils::GetProtectionMode() & utils::PROTECTION_MODE_STATIC)
            {
                bool have_focus = (gfocuswid() != 0);
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_CLIENT, "FocusInWindow(): %s short circuit, wid=%d, have_focus=%s, return=0, %p",
                       utils::GetProtectionModeTxt(), gfocuswid(), have_focus ? "true" : "false", &(gfocuswid()));
                return 0;
            }
            return securesend(FOCUS_IN, pid, wid); 
        }
        int FocusOutWindow(const uint32_t pid, const uint64_t wid)
        {
            if (wid == gfocuswid()) gfocuswid() = 0;
            if (utils::GetProtectionMode() & utils::PROTECTION_MODE_STATIC)
            {
                bool have_focus = (gfocuswid() != 0);
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_CLIENT, "FocusOutWindow(): %s short circuit, wid=%d, have_focus=%s, return=0, %p",
                       utils::GetProtectionModeTxt(), gfocuswid(), have_focus ? "true" : "false", &(gfocuswid()));
                return 0;
            }
            return securesend(FOCUS_OUT, pid, wid);
        }

        int DumpService(DumpData &data)
        {
            msg.msgtype = DUMP_SVC;
            msg.msgsize = sizeof(Msg);
            msg.data_1 = 0;
            msg.data_2 = 0;

            int returnValue = sendmsg(true);
            if (returnValue == 0 && msg.msgtype == DUMP_RESPONSE)
            {
                DeSerializer deserializer(msg_specific_response_data.get());
                data.ilock_status = deserializer.GetU8Value();
                uint32_t pid = deserializer.GetU32Value();
                data.focus_pid_wid = std::make_pair(pid, deserializer.GetU64Value());
                for (uint32_t i = 0, j = deserializer.GetU32Value(); i < j; i++)
                    data.pids.insert(deserializer.GetU32Value());
                data.status_data.status = static_cast<STATUS>(deserializer.GetU32Value());
                uint32_t count = deserializer.GetU32Value();
                data.status_data.entries = GetStringEntries(static_cast<uint8_t*>(deserializer.GetPtr()), count);
            }
            return returnValue;
        }

        int GetSystemStatus(bool verbose, StatusData &data)
        {
            msg.msgtype = SYSTEM_STATUS;
            msg.msgsize = sizeof(Msg);
            msg.data_1 = static_cast<uint32_t>(verbose);
            msg.data_2 = 0;

            int returnValue = sendmsg(true);

            if (returnValue != 0)
            {
                data.status = SEND_FAILED;
            }
            else if (returnValue == 0 && msg.msgtype == SYSTEM_RESPONSE)
            {
                data.status = static_cast<STATUS>(msg.data_1);
                if (verbose)
                {
                    if (data.status == TENTATIVE_NOT_ENTRY_ZERO || data.status == TENTATIVE_PRELOAD_DETECTED)
                    {
                        data.count = msg.data_2;
                        if (data.status == TENTATIVE_PRELOAD_DETECTED)
                        {
                            uint8_t* str_ptr = msg_specific_response_data.get();

                            auto get_string = [&]() -> std::string {
                                std::stringstream ss;
                                for (; *str_ptr; str_ptr++)
                                    ss << *str_ptr;
                                str_ptr++;
                                return ss.str();
                            };

                            for (size_t i = 0; i < data.count; i++)
                            {
                                std::vector<std::string> entry;
                                entry.push_back(get_string());
                                entry.push_back(get_string());
                                entry.push_back(get_string());
                                data.entries.push_back(entry);
                            }
                        }
                    }
                }
            }
            return returnValue;
        }

        int GetRegisteredProcesses(std::set<uint32_t> &pids)
        {
            msg.msgtype = REG_PROCESSES_STATUS;
            msg.msgsize = sizeof(Msg);
            msg.data_1 = 0;
            msg.data_2 = 0;

            int returnValue = sendmsg(true);
            if (returnValue == 0 && msg.msgtype == REG_PROCESSES_RESPONSE)
            {
                DeSerializer deserializer(msg_specific_response_data.get());
                uint32_t count = deserializer.GetU32Value();
                for (uint32_t i = 0; i < count; i++)
                    pids.insert(deserializer.GetU32Value());
            }
            return returnValue;
        }

        static const char *MessageType(MsgType msgType)
        {
            switch(msgType)
            {
            case ILOCK_STATUS: return "ILOCK_STATUS";
            case ILOCK_RESPONSE: return "ILOCK_RESPONSE";
            case KEY_SCRAMBLED: return "KEY_SCRAMBLED";
            case KSCRAM_RESPONSE: return "KSCRAM_RESPONSE";
            case REG_PROCESS: return "REG_PROCESS";
            case UNREG_PROCESS: return "UNREG_PROCESS";
            case FOCUS_IN: return "FOCUS_IN";
            case FOCUS_OUT: return "FOCUS_OUT";
            case ILOCK_SET: return "ILOCK_SET";
            case DUMP_SVC: return "DUMP_SVC";
            case DUMP_RESPONSE: return "DUMP_RESPONSE";
            case SYSTEM_STATUS: return "SYSTEM_STATUS";
            case SYSTEM_RESPONSE: return "SYSTEM_RESPONSE";
            case REG_PROCESSES_STATUS: return "REG_PROCESSES_STATUS";
            case REG_PROCESSES_RESPONSE: return "REG_PROCESSES_RESPONSE";
            default: return "UNKNOWN";
            }
        }

    private:
        Msg msg;

        std::unique_ptr<uint8_t[]> msg_specific_response_data;

        static const unsigned DEFAULT_TIMEOUT_MSECS = 150;

        uint64_t& gfocuswid()
        {
            // Store the in-focus window id when a window controlled by this process is in-focus.
            // Zero means no window controlled by this process is in-focus.
            static uint64_t g_focus_wid = 0;
            return g_focus_wid;
        }

        int securesend(MsgType type, const uint32_t state = 0, const uint64_t base = 0)
        {
            msg.msgtype = type, msg.msgsize = sizeof(Msg), msg.data_1 = state, msg.data_2 = base;
            return sendmsg();
        }

        int sendmsg(bool response = false)
        {
            ssize_t rc;
            struct sockaddr_un server_sockaddr;
            memset(&server_sockaddr, 0, sizeof(struct sockaddr_un));

            int sock = socket(AF_UNIX, SOCK_STREAM, 0);
            if (sock == -1)
            {
                int errnum = errno;
                tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): socket() returned -1: errno=%d %s", errnum, strerror(errnum));
                return 1;
            }
            else
            {
                tracef(TRACE_DEBUG, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): msg=%s",  MessageType(msg.msgtype));
            }

            struct timeval timeout;

            timeout.tv_sec = 0;
            timeout.tv_usec = DEFAULT_TIMEOUT_MSECS * 1000;

            rc = setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout));
            if (rc == -1)
            {
                int errnum = errno;
                tracef(TRACE_WARN, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): setsocketopt(SO_SNDTIMEO) returned -1: errno=%d %s", errnum, strerror(errnum));
            }

            rc = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
            if (rc == -1)
            {
                int errnum = errno;
                tracef(TRACE_WARN, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): setsocketopt(SO_RCVTIMEO) returned -1: errno=%d %s", errnum, strerror(errnum));
            }

            server_sockaddr.sun_family = AF_UNIX;
            strcpy(server_sockaddr.sun_path, ServerSocketPath().c_str());
            rc = connect(sock, (struct sockaddr*)&server_sockaddr, sizeof(struct sockaddr_un));
            if (rc == -1)
            {
                int errnum = errno;
                tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): connect() returned -1: errno=%d %s msg=%s", errnum, strerror(errnum), MessageType(msg.msgtype));
                close(sock);
                return 1;
            }
            rc = send(sock, &msg, sizeof(Msg), 0);
            if (rc == -1)
            {
                int errnum = errno;
                tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): send() returned -1: errno=%d %s timeout(ms)=%d", errnum, strerror(errnum), MessageType(msg.msgtype));
                close(sock);
                return 1;
            }
            else if (rc != sizeof(Msg))
            {
                tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): send() returned %d bytes expected %d msg=%s", rc, sizeof(Msg), MessageType(msg.msgtype));
                close(sock);
                return 1;
            }
            if (response)
            {
                ssize_t rc = recv(sock, (void*)&msg, sizeof(Msg), 0);

                if (rc == -1)
                {
                    int errnum = errno;
                    tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): recv() returned -1: errno=%d %s msg=%s", errnum, strerror(errnum), MessageType(msg.msgtype));
                    close(sock);
                    return 1;
                }
                else if (rc < static_cast<ssize_t>(sizeof(Msg)))
                {
                    tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): recv() returned %d bytes expected %d msg=%s", rc, sizeof(Msg), MessageType(msg.msgtype));
                    close(sock);
                    return 1;
                }
                else
                {
                    ssize_t byte_count = msg.msgsize - sizeof(Msg);

                    if (byte_count > 0)
                    {
                        msg_specific_response_data.reset(new uint8_t[byte_count]);

                        ssize_t rc = recv(sock, msg_specific_response_data.get(), byte_count, 0);

                        if (rc == -1)
                        {
                            int errnum = errno;
                            tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): message specific response data: recv() returned -1: errno=%d %s", errnum, strerror(errnum));
                            close(sock);
                            return 1;
                        }
                        else if (rc < byte_count)
                        {
                            tracef(TRACE_ERROR, SUBSYSTEM_NAMES::SVC_CLIENT, "sendmsg(): message specific response data: recv() returned %d bytes expected %d", rc, byte_count);
                            close(sock);
                            return 1;
                        }
                    }
                }
            }

            close(sock);
            return 0;
        }
};
}
#endif

