#ifndef _UTILS_H_
#define _UTILS_H_

#include <cstdint>
#include <stdlib.h>
#include <vector>
#include <climits>
#include <memory>
#include <set>
#include <string>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <errno.h>

////
//
// SEC-21 - Careful calling tracing logic in this file
//          Will end up in an infinite loop if the tracing logic also calls these function.
//          Functions used in traceing logic have warning comments in their body
//
////
#include "../trace/trace.h"

namespace utils
{

enum PROTECTION_MODE : uint32_t
{
    PROTECTION_MODE_DYNAMIC = 1 << 0,
    PROTECTION_MODE_STATIC = 1 << 1,
    PROTECTION_MODE_UNSET = 0
};

__attribute__((unused)) static struct timeval & getLastCheckedTimeval()
{
    ////
    //
    // SEC-21 - This function is use by trace logic - do not call any trace functions here
    //
    ////
    static struct timeval last_time_checked_timeval = {0,0};
    return last_time_checked_timeval;
}

__attribute__((unused)) static int getCheckInterval(const struct timeval &current_time_timeval)
{
    ////
    //
    // SEC-21 - This function is use by trace logic - do not call any trace functions here
    //
    ////
    static const int MIN_INTERVAL = 1, MAX_INTERVAL = 120;
    int rc = (current_time_timeval.tv_usec % MAX_INTERVAL) + MIN_INTERVAL;
    return rc;
}

__attribute__((unused)) static bool hasFileBeenModifiedSinceLastTimeval(const struct stat &stats, const struct timeval &last_time_read_timeval)
{
    ////
    //
    // SEC-21 - This function is use by trace logic - do not call any trace functions here
    //
    ////

    // if the file has never been read then effectively it has been 'modified' from this process point of view
    if (last_time_read_timeval.tv_sec == 0) return true;

    static const double epsilon = 0.5;
    return difftime(stats.st_mtime, last_time_read_timeval.tv_sec) > epsilon;
}

static PROTECTION_MODE GetProtectionMode(bool recheck_file = false)
{
    static const std::string path_to_config_file = "/usr/local/bin/sentrybay/.config/.protect";
    static PROTECTION_MODE protection_mode = PROTECTION_MODE_UNSET;
    if (protection_mode == PROTECTION_MODE_UNSET)
    {
        struct stat stats;
        if (stat(path_to_config_file.c_str(), &stats) != 0)
        {
            protection_mode = PROTECTION_MODE_DYNAMIC;
        }
        else
        {
            protection_mode = PROTECTION_MODE_STATIC;
        }
         trace(TRACE_DEBUG, "GetProtectionMode() - stat", std::to_string(stat(path_to_config_file.c_str(), &stats)).c_str());
    }
    else if (recheck_file) // This option prevents the need to kill the process to read the file.
                           // Although not used yet, in the future might add some signal handling to recheck the file
    {
        struct timeval &last_time_checked_timeval = getLastCheckedTimeval();
        struct timeval current_time_timeval;
        gettimeofday(&current_time_timeval, NULL); // ie. seconds and microseconds since epoch
        int interval = getCheckInterval(current_time_timeval);
        int time_diff = (current_time_timeval.tv_sec - last_time_checked_timeval.tv_sec);
        if (time_diff > interval)
        {
            last_time_checked_timeval = current_time_timeval;
            struct stat stats;
            if (stat(path_to_config_file.c_str(), &stats) != 0)
            {
                protection_mode = PROTECTION_MODE_DYNAMIC;
            }
            else
            {
                protection_mode = PROTECTION_MODE_STATIC;
            }
            trace(TRACE_DEBUG, "GetProtectionMode() - recheck stat", std::to_string(stat(path_to_config_file.c_str(), &stats)).c_str());
        }
    }
    return protection_mode;
}

__attribute__((unused)) static const char* GetProtectionModeTxt()  
{
    switch(GetProtectionMode())
    {
        case PROTECTION_MODE_DYNAMIC: return "PROTECTION_MODE_DYNAMIC"; break;
        case PROTECTION_MODE_STATIC: return "PROTECTION_MODE_STATIC"; break;
        default : return "<NOT SET>"; break;
    }
}

__attribute__((unused)) static const char * GetProtectionModePrettyTxt()
{
    std::string protection_mode_pretty_txt = "Protection mode : ";
    protection_mode_pretty_txt += GetProtectionModeTxt();
    return protection_mode_pretty_txt.c_str();
}

struct CWrapperForCppStdString
{
    std::string contents;
};

__attribute__((unused)) static const CWrapperForCppStdString * const getExeName()
{
    static CWrapperForCppStdString exe_name;
    if (exe_name.contents.empty())
    {
       char c_exe_name[PATH_MAX] = {0};
       if (readlink( "/proc/self/exe", c_exe_name, sizeof(c_exe_name)-1 ) != -1)
       {
           exe_name.contents = c_exe_name;
       }
    }
    return &exe_name;
}

struct FileTxt
{
    std::vector<std::string> contents;
};

__attribute__((unused)) static bool readFileStreamTxt(FILE *file_stream, FileTxt *file_txt)
{
    if (!file_stream || !file_txt) return false;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    bool rc = false;
    while ((read = getline(&line, &len, file_stream)) != -1) 
    {
        file_txt->contents.push_back(line);
#ifdef DEBUG
        trace(TRACE_ALWAYS, "readFileStreamTxt()", file_txt->contents.back().c_str());
#endif
        rc = true;
    }
    ////
    // Caller must handle disposing of the 'file_stream'
    // eg : fclose(file_stream);
    ////
    if (line) free(line);
    return rc;
}

template <typename local_instance_type, typename local_instance_deleter_function_type>
struct SimpleLocallyScopedRAII
{
        SimpleLocallyScopedRAII(local_instance_type &local_instance, local_instance_deleter_function_type deleter_function):
          _local_instance(local_instance),
          _deleter_function(deleter_function)
        {
        }
        ~SimpleLocallyScopedRAII()
        {
            _deleter_function(_local_instance);
        }
        SimpleLocallyScopedRAII(const SimpleLocallyScopedRAII&) = delete;
        SimpleLocallyScopedRAII(SimpleLocallyScopedRAII&&) = delete;
        SimpleLocallyScopedRAII& operator=(const SimpleLocallyScopedRAII&) = delete;
        SimpleLocallyScopedRAII& operator=(SimpleLocallyScopedRAII&&) = delete;
        local_instance_type &_local_instance;
        local_instance_deleter_function_type *_deleter_function;
};

__attribute__((unused)) static bool isWhitelisted(const char *subsystem_name)
{
    ////
    //
    // SEC-21 - This function is use by trace logic - do not call any trace functions here
    //
    ////
    bool rv = false;
    const std::string &exe_name = utils::getExeName()->contents;
    if (!exe_name.empty())
    {
        if( // Stratodesk exceptions
            (exe_name == "/bin/iceauth") ||
            (exe_name == "/bin/icewmbg") ||
            (exe_name == "/bin/icewmtray") ||
            (exe_name == "/bin/icewm") ||
            (exe_name == "/bin/idesk") ||
            (exe_name == "/bin/x11vnc") ||
            // icewm
            (exe_name == "/usr/bin/iceauth") ||
            (exe_name == "/usr/bin/icewmbg") ||
            (exe_name == "/usr/bin/icewmtray") ||
            (exe_name == "/usr/bin/icewm") ||
            // idesk - icon manager for window managers
            (exe_name == "/usr/bin/idesk") ||
            // gnome
            (exe_name == "/usr/bin/gnome-shell") ||
            // kde
            (exe_name == "/usr/bin/kwin") ||
            (exe_name == "/usr/bin/kwin_wayland") ||
            (exe_name == "/usr/bin/kwin_x11") ||
            // marco is the MATE window manager
            (exe_name == "/usr/bin/marco") ||
            (exe_name == "/usr/lib/mate-screensaver/mate-screensaver-dialog"))
        {
            rv = true;
        }
    }
    return rv;
}
}
#endif

