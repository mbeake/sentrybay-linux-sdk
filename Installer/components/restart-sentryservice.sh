#!/bin/bash

is_installed()
{
    [ -f /usr/lib/systemd/system/sentryservice-install.service ]
}

is_running()
{
    ps aux | pgrep -a sentryservice &> /dev/null
}

pending_reboot()
{
    if [ -f /usr/lib/systemd/system/sentryservice-remove.service ]; then
        echo -e "\e[31mA reboot is required as an uninstall is already pending\e[0m"
        exit 1
    fi
}

restart()
{
    if ! is_installed; then
        echo -e "\e[31mSentryService is not installed\e[0m"
        exit 1
    fi
    pending_reboot
    if is_running; then
        echo "SentryService is already running, restarting..."
        systemctl restart sentryservice-install
    else
        echo "SentryService is not running, starting..."
        systemctl start sentryservice-install
    fi
    echo "Done."
    exit 0
}

echo -e "\e[1mSentryService Maintenance Script v1.0\e[0m"
if [[ $EUID -ne 0 ]]; then
   echo -e "\e[31mThis maintenance script must be run as root\e[0m" 
   exit 1
fi

if [ "$1" = "--restart" ]; then
    restart
elif [ "$1" = "--help" ]; then
    echo -e "Options:\n\t\e[1m--restart \e[0m(starts or restarts the SentryServer)"
    exit 0
else
    echo "Incorrect command line arguments, use --help for details"
    exit 1
fi
