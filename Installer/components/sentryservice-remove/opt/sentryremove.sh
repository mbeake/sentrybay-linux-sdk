#!/bin/bash
rm -rf /usr/local/bin/sentrybay
rm -rf /usr/local/lib/sentrybay
systemctl disable sentryservice-remove
rm -rf /usr/lib/systemd/system/sentryservice-remove.service
rm -rf /etc/dbus-1/session.d/org.gnome.Shell.Screencast.conf
rm -rf /etc/dbus-1/session.d/org.gnome.Shell.Screenshot.conf
rm -rf /opt/sentryremove.sh
