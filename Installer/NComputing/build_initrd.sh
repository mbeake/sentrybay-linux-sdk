#!/bin/bash

# Extract the initrd image

rm -rf initrd initrd_dir
xz -dc initrd.img > initrd
mkdir initrd_dir && cd initrd_dir && cpio -i < ../initrd

# Deploy the Sentry Bay SDK into the initrd image
# !ToDo

# Construct a modified initrd image

find ./ | cpio -o | xz > ../initrd.img.new


