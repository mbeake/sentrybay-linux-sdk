#!/bin/bash
# Usage: ./build.sh [VERSION]
# eg. ./build.sh 0.99.12


echo -e "\e[1mSentryService Build Script v1.0\e[0m"

if [ "$1" = "" ]; then
    echo "Incorrect command line arguments, use --help for details"
    exit 1
elif [ "$1" = "--help" ]; then
    echo -e "Options:\n\t\e[1m./build [\e[0;32mVERSION\e[0m]"
    exit 0
fi

chmod 755 components/sentryservice-install
chmod 755 components/sentryservice-remove
chmod 744 components/deploy-sentryservice.sh
chmod 755 components/sentryservice-install/*
chmod 644 components/sentryservice-install/etc/preload_lib
chmod 755 components/sentryservice-install/usr/local
chmod 555 components/sentryservice-install/usr/lib
chmod 755 components/sentryservice-install/usr/lib/systemd
chmod 755 components/sentryservice-install/usr/lib/systemd/system
chmod 644 components/sentryservice-install/usr/lib/systemd/system/sentryservice-install.service
chmod 755 components/sentryservice-install/usr/local/*
chmod 755 components/sentryservice-install/usr/local/bin/sentrybay
chmod 700 components/sentryservice-install/usr/local/bin/sentrybay/sentryservice
chmod 400 components/sentryservice-install/usr/local/bin/sentrybay/.sentryservice.conf
chmod 755 components/sentryservice-install/usr/local/bin/sentrybay/.config/
chmod 644 components/sentryservice-install/usr/local/bin/sentrybay/.config/config.ini
chmod 755 components/sentryservice-install/usr/local/bin/sentrybay/.trace/
chmod 644 components/sentryservice-install/usr/local/bin/sentrybay/.trace/trace.ini
chmod 755 components/sentryservice-install/usr/local/lib/sentrybay
chmod 755 components/sentryservice-install/usr/local/lib/sentrybay/libsentrybay.so
chmod 755 components/sentryservice-remove/*
chmod 700 components/sentryservice-remove/opt/sentryremove.sh
chmod 555 components/sentryservice-remove/usr/lib
chmod 755 components/sentryservice-remove/usr/lib/systemd
chmod 755 components/sentryservice-remove/usr/lib/systemd/system
chmod 644 components/sentryservice-remove/usr/lib/systemd/system/sentryservice-remove.service
chmod 744 components/restart-sentryservice.sh
mv components/sentryservice-install/usr/local/lib/sentrybay/libsentrybay.so components/sentryservice-install/usr/local/lib/sentrybay/libsentrybay.so.$1
echo $1 > components/sentryservice-install/usr/local/bin/sentrybay/.version
chmod 644 components/sentryservice-install/usr/local/bin/sentrybay/.version
mkdir -p build
tar --owner=0 --group=0 -czvf build/sentry-service-$1.tar.gz -C components/ sentryservice-install sentryservice-remove &> /dev/null
mv components/sentryservice-install/usr/local/lib/sentrybay/libsentrybay.so.$1 components/sentryservice-install/usr/local/lib/sentrybay/libsentrybay.so
echo -e "Deployment \e[1mbuild/sentry-service-$1.tar.gz \e[0marchive created successfully."
cp components/restart-sentryservice.sh build/
echo -e "Service restart script \e[1mrestart-sentryservice.sh \e[0mcreated successfully."
cp components/deploy-sentryservice.sh build/
sed -i "/SENTRY_VERSION=/c\SENTRY_VERSION=$1" build/deploy-sentryservice.sh
echo -e "Deployment bash script \e[1mbuild/deploy-sentryservice.sh \e[0mcreated successfully."
