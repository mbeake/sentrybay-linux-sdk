#!/bin/bash
SENTRY_VERSION=0.99.150

is_installed()
{
    [ -f /usr/lib/systemd/system/sentryservice-install.service ]
}

pending_reboot()
{
    if [ -f /usr/lib/systemd/system/sentryservice-remove.service ]; then
        echo -e "\e[31mA reboot is required as an uninstall is already pending\e[0m"
        exit 1
    fi
}

install()
{
    if is_installed; then
        echo -e "\e[31mSentryService is already installed\e[0m"
        exit 1
    fi
    pending_reboot
    echo "Installing SentryService and dependencies..."
    tar -C / -zxf sentry-service-$SENTRY_VERSION.tar.gz --strip-components=1 sentryservice-install
    ln -sf /usr/local/lib/sentrybay/libsentrybay.so.$SENTRY_VERSION /usr/local/lib/sentrybay/libsentrybay.so
    echo "Starting SentryService... please wait."
    systemctl daemon-reload
    systemctl enable sentryservice-install &> /dev/null
    systemctl start sentryservice-install
    mv /etc/preload_lib /etc/ld.so.preload
    echo "Installation complete."
    exit 0
}

uninstall()
{
    pending_reboot
    if ! is_installed; then
        echo -e "\e[31mSentryService is not installed\e[0m"
        exit 1
    else
        rm -rf /etc/ld.so.preload
        echo "Stopping SentryService..."
        systemctl disable sentryservice-install &> /dev/null
        systemctl stop sentryservice-install
        echo "Removing installation files..."
        rm -rf /usr/lib/systemd/system/sentryservice-install.service
        tar -C / -zxf sentry-service-$SENTRY_VERSION.tar.gz --strip-components=1 sentryservice-remove
        systemctl daemon-reload
        systemctl enable sentryservice-remove &> /dev/null
        echo "Uninstallation complete. Please reboot the system in order to complete the removal."
        exit 0
    fi
}

check_argument()
{
    if [ "$1" = "" ]; then
        echo -e "\e[31mNo version specifed for requested operation\e[0m"
        exit 1
    elif ! [ -f sentry-service-$1.tar.gz ]; then
        echo -e "\e[31mRequested version is not available for the operation\e[0m"
        exit 1
    fi
    mkdir -p /opt/sentrybay
    tar -C /opt/sentrybay -zxf sentry-service-$SENTRY_VERSION.tar.gz --strip-components=5 sentryservice-install/usr/local/bin/sentrybay/.version
    diff /opt/sentrybay/.version /usr/local/bin/sentrybay/.version &> /dev/null
}

upgrade()
{
    pending_reboot
    if ! is_installed; then
        echo -e "\e[31mSentryService is not installed\e[0m"
        exit 1
    elif check_argument $1; then
        rm -rf /opt/sentrybay
        echo -e "\e[31mRequested version is already installed\e[0m"
        exit 1
    else
        echo "Upgading SentryService to version: $1"
        systemctl stop sentryservice-install
        tar -C /usr/local -zxf sentry-service-$1.tar.gz --strip-components=3 sentryservice-install
        ln -sf /usr/local/lib/sentrybay/libsentrybay.so.$1 /usr/local/lib/sentrybay/libsentrybay.so
        echo "Restarting SentryService... please wait."
        systemctl start sentryservice-install
        rm -rf /opt/sentrybay
        echo "Upgrade complete."
    fi
}

echo -e "\e[1mSentryService Installation Script v1.0\e[0m"
if [[ $EUID -ne 0 ]]; then
   echo -e "\e[31mThis installation script must be run as root\e[0m" 
   exit 1
fi

if [ "$1" = "--install" ]; then
    install
elif [ "$1" = "--uninstall" ]; then
    uninstall
elif [ "$1" = "--help" ]; then
    echo -e "Options:\n\t\e[1m--install \e[0m(Perform a clean installation of the service)\n\t\e[1m--uninstall \e[0m(Removes service and installation files, reboot required)\n\t\e[1m--upgrade [\e[0;32mVERSION\e[0m] (Upgrades an already installed and running service)"
    exit 0
elif [ "$1" = "--upgrade" ]; then
    upgrade $2
else
    echo "Incorrect command line arguments, use --help for details"
    exit 1
fi
